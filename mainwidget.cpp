#include "mainwidget.h"
#include "ui_mainwidget.h"

MainWidget::MainWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MainWidget)
{
    ui->setupUi(this);

    // заданная траектория есть всегда
    ui->widgetGraphic->addData("заданная", true);

    // настройка графика результатов
    ui->widgetResult->SetFileName("result");
    ui->widgetResult->addData("result", true);
    ui->widgetResult->addData("sourseP",true);

    // название полей координат
    ui->labelX->setText("\u03BE");
    ui->labelZ->setText("\u03B6");
    ui->labelVx->setText("V\u03BE");
    ui->labelVz->setText("V\u03B6");
    ui->labeldX->setText("\u0394\u03BE");
    ui->labeldZ->setText("\u0394\u03B6");
    ui->labeldVx->setText("\u0394V\u03BE");
    ui->labeldVz->setText("\u0394V\u03B6");

    // графики для отображения датчиков
    graphicPsiBINS = new GraphicWindow("курс БИНС", "Время, с","",0);
    graphicPsiMagn = new GraphicWindow("курс магнитометра", "Время, с","",0);
    graphicVxBINS = new GraphicWindow("Vx БИНС", "Время, с","",0);
    graphicVzBINS = new GraphicWindow("Vz БИНС", "Время, с","",0);
    graphicVxLag = new GraphicWindow("Vx ГДЛ", "Время, с","",0);
    graphicVzLag = new GraphicWindow("Vz ГДЛ", "Время, с","",0);
    graphicXSNS = new GraphicWindow("X СНС", "Время, с","",0);
    graphicZSNS = new GraphicWindow("Z СНС", "Время, с","",0);
    graphicXGANS = new GraphicWindow("X ГАНС", "Время, с","",0);
    graphicZGANS = new GraphicWindow("Z ГАНС", "Время, с","",0);

    // графики для отображения ССП
    graphicXZ = new GraphicWindow("КНС","\u03BE, м", "\u03B6, м",0);
    graphicX = new GraphicWindow("X", "Время, с","",0);
    graphicZ = new GraphicWindow("Z", "Время, с","",0);
    graphicdX = new GraphicWindow("dX", "Время, с","",0);
    graphicdZ = new GraphicWindow("dZ", "Время, с","",0);
    graphicVx = new GraphicWindow("Vx", "Время, с","",0);
    graphicVz = new GraphicWindow("Vz", "Время, с","",0);
    graphicdVx = new GraphicWindow("dVx", "Время, с","",0);
    graphicdVz = new GraphicWindow("dVz", "Время, с","",0);

    // названия файлов графиков
    ui->widgetGraphic->SetFileName("Traektoria");
    graphicPsiBINS->SetFileName("psiBINS");
    graphicPsiMagn->SetFileName("psiMagn");
    graphicVxBINS->SetFileName("VxBINS");
    graphicVzBINS->SetFileName("VzBINS");
    graphicVxLag->SetFileName("VxLag");
    graphicVzLag->SetFileName("VzLag");
    graphicXSNS->SetFileName("XSNS");
    graphicZSNS->SetFileName("ZSNS");
    graphicXGANS->SetFileName("XGANS");
    graphicZGANS->SetFileName("ZGANS");

    graphicXZ->SetFileName("XZ");
    graphicX->SetFileName("X");
    graphicZ->SetFileName("Z");
    graphicVx->SetFileName("Vx");
    graphicVz->SetFileName("Vz");
    graphicdX->SetFileName("dX");
    graphicdZ->SetFileName("dZ");
    graphicdVx->SetFileName("dVx");
    graphicdVz->SetFileName("dVz");

    //ввод данных на график
    graphicPsiBINS->addData("psiBINS", true);
    graphicPsiMagn->addData("psiMagn", true);
    graphicVxBINS->addData("VxBINS", true);
    graphicVzBINS->addData("VzBINS", true);
    graphicVxLag->addData("VxLag", true);
    graphicVzLag->addData("VzLag", true);
    graphicXSNS->addData("XSNS", true);
    graphicXSNS->addData("Xideal", true);
    graphicZSNS->addData("ZSNS", true);
    graphicXGANS->addData("XGANS", true);
    graphicZGANS->addData("ZGANS", true);

    graphicXZ->addData("SNS",true);
    graphicXZ->addData("BINS_Lag",true);
    graphicXZ->addData("Magn_Lag",true);
    graphicXZ->addData("BINS",true);
    graphicXZ->addData("GANS",true);
    graphicXZ->addData("SNS_BINS",true);
    graphicXZ->addData("GANS_BINS_Lag",true);
    graphicXZ->addData("GANS_Magn_Lag",true);
    graphicXZ->addData("GANS_BINS",true);

    graphicX->addData("SNS",true);
    graphicX->addData("BINS_Lag",true);
    graphicX->addData("Magn_Lag",true);
    graphicX->addData("BINS",true);
    graphicX->addData("GANS",true);
    graphicX->addData("SNS_BINS",true);
    graphicX->addData("GANS_BINS_Lag",true);
    graphicX->addData("GANS_Magn_Lag",true);
    graphicX->addData("GANS_BINS",true);

    graphicZ->addData("SNS",true);
    graphicZ->addData("BINS_Lag",true);
    graphicZ->addData("Magn_Lag",true);
    graphicZ->addData("BINS",true);
    graphicZ->addData("GANS",true);
    graphicZ->addData("SNS_BINS",true);
    graphicZ->addData("GANS_BINS_Lag",true);
    graphicZ->addData("GANS_Magn_Lag",true);
    graphicZ->addData("GANS_BINS",true);

    graphicdX->addData("SNS",true);
    graphicdX->addData("BINS_Lag",true);
    graphicdX->addData("Magn_Lag",true);
    graphicdX->addData("BINS",true);
    graphicdX->addData("GANS",true);
    graphicdX->addData("SNS_BINS",true);
    graphicdX->addData("GANS_BINS_Lag",true);
    graphicdX->addData("GANS_Magn_Lag",true);
    graphicdX->addData("GANS_BINS",true);

    graphicdZ->addData("SNS",true);
    graphicdZ->addData("BINS_Lag",true);
    graphicdZ->addData("Magn_Lag",true);
    graphicdZ->addData("BINS",true);
    graphicdZ->addData("GANS",true);
    graphicdZ->addData("SNS_BINS",true);
    graphicdZ->addData("GANS_BINS_Lag",true);
    graphicdZ->addData("GANS_Magn_Lag",true);
    graphicdZ->addData("GANS_BINS",true);

    graphicVx->addData("BINS_Lag",true);
    graphicVx->addData("Magn_Lag",true);
    graphicVx->addData("BINS",true);
    graphicVx->addData("SNS_BINS",true);
    graphicVx->addData("GANS_BINS_Lag",true);
    graphicVx->addData("GANS_Magn_Lag",true);
    graphicVx->addData("GANS_BINS",true);

    graphicVz->addData("BINS_Lag",true);
    graphicVz->addData("Magn_Lag",true);
    graphicVz->addData("BINS",true);
    graphicVz->addData("SNS_BINS",true);
    graphicVz->addData("GANS_BINS_Lag",true);
    graphicVz->addData("GANS_Magn_Lag",true);
    graphicVz->addData("GANS_BINS",true);

    graphicdVx->addData("BINS_Lag",true);
    graphicdVx->addData("Magn_Lag",true);
    graphicdVx->addData("BINS",true);
    graphicdVx->addData("SNS_BINS",true);
    graphicdVx->addData("GANS_BINS_Lag",true);
    graphicdVx->addData("GANS_Magn_Lag",true);
    graphicdVx->addData("GANS_BINS",true);

    graphicdVz->addData("BINS_Lag",true);
    graphicdVz->addData("Magn_Lag",true);
    graphicdVz->addData("BINS",true);
    graphicdVz->addData("SNS_BINS",true);
    graphicdVz->addData("GANS_BINS_Lag",true);
    graphicdVz->addData("GANS_Magn_Lag",true);
    graphicdVz->addData("GANS_BINS",true);

    // таймеры для расчетов траектории
    systTimer = new QTimer(this);
    systTimer->setTimerType(Qt::PreciseTimer);

    connect(systTimer,&QTimer::timeout,this,&MainWidget::calcTraek);

    // наблюдатели
    nablXSNSBINS = new NabludatelC3(this);
    nablZSNSBINS = new NabludatelC3(this);
    nablXGANSBINSLag = new NabludatelC3(this);
    nablZGANSBINSLag = new NabludatelC3(this);
    nablXGANSMagnLag = new NabludatelC3(this);
    nablZGANSMagnLag = new NabludatelC3(this);
    nablXGANSBINS = new NabludatelC3(this);
    nablZGANSBINS = new NabludatelC3(this);

    // настройка kx-пульт
    K_Protocol = new Qkx_coeffs("./Configs/kx_pult.conf", "ki");
    X_Protocol = new x_protocol ("./Configs/kx_pult.conf","xi",X);

    K.clear();
    for (int i=0;i<2000;i++)
    {
        K.push_back(0);
    }

    connect(K_Protocol, SIGNAL(receiveSucceed()), this, SLOT(K_receive()));

    //simpleContir = new SimpleContur(this);
    //perebor = new Perebor(this);

    //qDebug()<<"cos 90 "<<cos(90/Kc)<<" cos -90 "<<cos(-90/Kc)<<" sin 181 "<<sin(181/Kc)<<" sin -1 "<<sin(-1/Kc)<<" cos -175 "<<cos(-175/Kc);
}

MainWidget::~MainWidget()
{
    delete ui;
}

void MainWidget::K_receive()
{

}

void MainWidget::on_pushButtonAddPoint_clicked()
{
    // нажата кнопка добавить точку на заданную траекторию
    double x = ui->lineEditX->text().toDouble();
    double y = ui->lineEditZ->text().toDouble();

    ui->widgetGraphic->addPoint("заданная",y,x);

    PointStruct p;
    p.x = x;
    p.y  =y;
    pointsVec.append(p);
}

void MainWidget::on_pushButtonAddTraek_clicked()
{
    // нажата кнопка добавить новую траекторию

    // читаем желаемые параметры траектории
    TraekStruct traek;
    traek.Vx = ui->lineEditVx->text().toDouble();
    traek.Vz = ui->lineEditVz->text().toDouble();
    traek.dX = ui->lineEditdX->text().toDouble();
    traek.dZ = ui->lineEditdZ->text().toDouble();
    traek.dVx = ui->lineEditdVx->text().toDouble();
    traek.dVz = ui->lineEditdVz->text().toDouble();
    traek.Ax = ui->lineEditdAx->text().toDouble();
    traek.Az = ui->lineEditdAz->text().toDouble();
    traek.wx = ui->lineEditwx->text().toDouble();
    traek.wz = ui->lineEditwz->text().toDouble();
    traek.xzP = ui->checkBoxUseDelay->isChecked();
    traek.R = ui->lineEditR->text().toDouble();

    // записываем траекторию в список и добавляем в графики
    actualName = ui->lineEditID->text();
    traekList[actualName] = traek;
    ui->widgetGraphic->addData(actualName,true);

    // устанавливаем начальную целевую точку
    calcData = new CalcStruct();
    calcData->x_dist = pointsVec.first().x;
    calcData->z_dist = pointsVec.first().y;
    //calcData->Ktpsi = ui->lineEditTpsi->text().toDouble();
    //calcData->Ktxz = ui->lineEditTdzita->text().toDouble();
    actual_num = 1;

    // очищаем данные датчиков и ССП
    ClearSensorsData();
    sspData = new SSPStruct();
    nablData = new NablStruct();

    // скрываем кнопки запуска до конца расчета
    ui->pushButtonAddTraek->setEnabled(false);

    // выясняем, замыкаем ли СУ по КНС
    isUseKNS = ui->checkBoxUseKNS->isChecked();
    if (isUseKNS)
    {
        // используем КНС
        actualKNS = ui->comboBoxSSP->currentIndex();
    }

    // запускаем расчет
    calcTraek();

    // возвращаем кнопки запуска до конца расчета
    ui->pushButtonAddTraek->setEnabled(true);

    // удаляем более не нужную структуру
    delete calcData;
    delete sspData;
    delete nablData;
}

void MainWidget::calcTraek()
{
    // шаг расчета траектории
    double T=0; // время моделирования
    bool not_end = true; // флаг окончания моделирования

    int graphPeriod = 1; // период вывода графиков
    ReadSensorsParams(); // чтение параметров датчиков для текущего моделирования

    // очистка графиков
    graphicPsiBINS->clearGraphic();
    graphicPsiMagn->clearGraphic();
    graphicVxBINS->clearGraphic();
    graphicVzBINS->clearGraphic();
    graphicVxLag->clearGraphic();
    graphicVzLag->clearGraphic();
    graphicXSNS->clearGraphic();
    graphicZSNS->clearGraphic();
    graphicXGANS->clearGraphic();
    graphicZGANS->clearGraphic();

    graphicXZ->clearGraphic();
    graphicX->clearGraphic();
    graphicZ->clearGraphic();
    graphicVx->clearGraphic();
    graphicVz->clearGraphic();
    graphicdX->clearGraphic();
    graphicdZ->clearGraphic();
    graphicdVx->clearGraphic();
    graphicdVz->clearGraphic();

    SetGraphicSSPVisible();

    // обнуление переменных для результатов интегрированя растущих со временем ошибок
    traekList[actualName].Vx_t = 0; // для получения растущей со временем ошибки координаты X
    traekList[actualName].Vz_t = 0; // для получения растущей со временем ошибки координаты Z
    sensorsData.DwyBINS_t = 0; // угол курса БИНС

    // определение счетчиков для периодов выдачи данных
    // СУ
    if (kontrData.systPeriod<Ttimer)
        kontrData.systPeriod = 1;
    else
    {
        //qDebug()<<" "<<kontrData.systPeriod/Ttimer;
        kontrData.systPeriod = kontrData.systPeriod/Ttimer;
    }
    // датчик координаты X
    if (kontrData.xPeriod<Ttimer)
        kontrData.xPeriod = 1;
    else
        kontrData.xPeriod = kontrData.xPeriod/Ttimer;
    // датчик координаты Y
    if (kontrData.zPeriod<Ttimer)
        kontrData.zPeriod = 1;
    else
        kontrData.zPeriod = kontrData.zPeriod/Ttimer;
    // отображение на графиках
    if (graphPeriod<Ttimer)
        graphPeriod = 1;
    else
        graphPeriod = graphPeriod/Ttimer;

    // угол курса БИНС
    if (sensorsData.psiBINSPeriod<Ttimer)
        sensorsData.psiBINSPeriod = 1;
    else
        sensorsData.psiBINSPeriod = sensorsData.psiBINSPeriod/Ttimer;
    // угол курса Магнитометра
    if (sensorsData.psiMagnPeriod<Ttimer)
        sensorsData.psiMagnPeriod = 1;
    else
        sensorsData.psiMagnPeriod = sensorsData.psiMagnPeriod/Ttimer;
    // северная скорость БИНС
    if (sensorsData.VxBINSPeriod<Ttimer)
        sensorsData.VxBINSPeriod = 1;
    else
        sensorsData.VxBINSPeriod = sensorsData.VxBINSPeriod/Ttimer;
    // восточная скорость БИНС
    if (sensorsData.VzBINSPeriod<Ttimer)
        sensorsData.VzBINSPeriod = 1;
    else
        sensorsData.VzBINSPeriod = sensorsData.VzBINSPeriod/Ttimer;
    // продольная скорость ГДЛ
    if (sensorsData.VxLagPeriod<Ttimer)
        sensorsData.VxLagPeriod = 1;
    else
        sensorsData.VxLagPeriod = sensorsData.VxLagPeriod/Ttimer;
    // поперечная скорость ГДЛ
    if (sensorsData.VzLagPeriod<Ttimer)
        sensorsData.VzLagPeriod = 1;
    else
        sensorsData.VzLagPeriod = sensorsData.VzLagPeriod/Ttimer;
    // координата X СНС
    if (sensorsData.xSNSPeriod<Ttimer)
        sensorsData.xSNSPeriod = 1;
    else
        sensorsData.xSNSPeriod = sensorsData.xSNSPeriod/Ttimer;
    // координата Z СНС
    if (sensorsData.zSNSPeriod<Ttimer)
        sensorsData.zSNSPeriod = 1;
    else
        sensorsData.zSNSPeriod = sensorsData.zSNSPeriod/Ttimer;
    // координата X ГАНС
    if (sensorsData.xGANSPeriod<Ttimer)
        sensorsData.xGANSPeriod = 1;
    else
        sensorsData.xGANSPeriod = sensorsData.xGANSPeriod/Ttimer;
    // координата Z ГАНС
    if (sensorsData.zGANSPeriod<Ttimer)
        sensorsData.zGANSPeriod = 1;
    else
        sensorsData.zGANSPeriod = sensorsData.zGANSPeriod/Ttimer;

    // ССП
    if (sspData->periodSNS<Ttimer)
        sspData->periodSNS = 1;
    else
        sspData->periodSNS = sspData->periodSNS/Ttimer;
    if (sspData->periodBINSLag<Ttimer)
        sspData->periodBINSLag = 1;
    else
        sspData->periodBINSLag = sspData->periodBINSLag/Ttimer;
    if (sspData->periodMagnLag<Ttimer)
        sspData->periodMagnLag = 1;
    else
        sspData->periodMagnLag = sspData->periodMagnLag/Ttimer;
    if (sspData->periodBINS<Ttimer)
        sspData->periodBINS = 1;
    else
        sspData->periodBINS = sspData->periodBINS/Ttimer;
    if (sspData->periodGANS<Ttimer)
        sspData->periodGANS = 1;
    else
        sspData->periodGANS = sspData->periodGANS/Ttimer;

    // наблюдатели
    if (nablData->periodSNSBINS<Ttimer)
        nablData->periodSNSBINS = 1;
    else
        nablData->periodSNSBINS = nablData->periodSNSBINS/Ttimer;
    if (nablData->periodGANSBINSLag<Ttimer)
        nablData->periodGANSBINSLag = 1;
    else
        nablData->periodGANSBINSLag = nablData->periodGANSBINSLag/Ttimer;
    if (nablData->periodGANSMagnLag<Ttimer)
        nablData->periodGANSMagnLag = 1;
    else
        nablData->periodGANSMagnLag = nablData->periodGANSMagnLag/Ttimer;
    if (nablData->periodGANSBINS<Ttimer)
        nablData->periodGANSBINS = 1;
    else
        nablData->periodGANSBINS = nablData->periodGANSBINS/Ttimer;

    // коррекция
    if (nablData->period2SNSBINS<Ttimer)
        nablData->period2SNSBINS = 1;
    else
        nablData->period2SNSBINS = nablData->period2SNSBINS/Ttimer;
    if (nablData->period2GANSBINSLag<Ttimer)
        nablData->period2GANSBINSLag = 1;
    else
        nablData->period2GANSBINSLag = nablData->period2GANSBINSLag/Ttimer;
    if (nablData->period2GANSMagnLag<Ttimer)
        nablData->period2GANSMagnLag = 1;
    else
        nablData->period2GANSMagnLag = nablData->period2GANSMagnLag/Ttimer;
    if (nablData->period2GANSBINS<Ttimer)
        nablData->period2GANSBINS = 1;
    else
        nablData->period2GANSBINS = nablData->period2GANSBINS/Ttimer;

    // счетчики для обновления данных
    int systCount = 0; // СУ
    int xCount = 0; // датчик координаты X
    int zCount = 0; // датчик коордиаты Z
    int graphCount = 0; // отображение графиков
    int psiBINSCount = 0; // угол курса БИНС
    int psiMagnCount = 0; // угол курса магнитометра
    int VxBINSCount = 0; // северная скорсть БИНС
    int VzBINSCount = 0; // восточная скорость БИНС
    int VxLagCount = 0; // продольная скорость ГДЛ
    int VzLagCount = 0; // поперечная скорость ГДЛ
    int xSNSCount = 0; // координата X СНС
    int zSNSCount = 0; // координата Z СНС
    int xGANSCount = 0; // координата X ГАНС
    int zGANSCount = 0; // координата Z ГАНС
    int SNSCount = 0; // ССП по СНС
    int BINSLagCount = 0; // ССП по БИНС и ГДЛ
    int MagnLagCount = 0; // ССП по Магнитометру и ГДЛ
    int BINSCount = 0; // ССП по БИНС
    int GANSCount = 0; // ССП по ГАНС
    int SNSBINSCount = 0; // наблюдатель по СНС и БИНС
    int GANSBINSLagCount = 0; // наблюдатель по ГАНС и БИНС с ГДЛ
    int GANSMagnLagCount = 0; // наблюдатель по ГАНС и Магнитометру с ГДЛ
    int GANSBINSCount = 0; // наблюдатель по ГАНС и БИНС

    // цикл расчетов
    while (not_end)
    {
        if (systCount == kontrData.systPeriod) // период дискретизации системы
        {
            Control_system();
            systCount = 0;
            //qDebug()<<"control system";
        }
        if (xCount == kontrData.xPeriod) // период датчика координаты X
        {
            if (traekList[actualName].xzP)
            {
                calcData->x_sens=traekList[actualName].xPrev;
                traekList[actualName].xPrev=calcData->x_now+traekList[actualName].dX+traekList[actualName].Vx_t+traekList[actualName].Ax*sin(traekList[actualName].wx*T);
            }
            else
            {
                calcData->x_sens = calcData->x_now+traekList[actualName].dX+traekList[actualName].Vx_t+traekList[actualName].Ax*sin(traekList[actualName].wx*T);
                //qDebug()<<"sin noise "<<traekList[actualName].Ax*sin(traekList[actualName].wx*T)<<"rand "<<rand()%10;
            }
            if (isUseKNS)
            {
                // используем КНС для замыкания СУ
                switch(actualKNS)
                {
                case 0: // ССП по СНС
                {
                    calcData->x_kns = sspData->xSNS;
                    //qDebug()<<"calcX_kns "<<sspData->xSNS;
                    break;
                }
                case 1: // ССП по БИНС и ГДЛ
                {
                    calcData->x_kns = sspData->xBINSLag;
                    break;
                }
                case 2: // ССП по Магнитометру и ГДЛ
                {
                    calcData->x_kns = sspData->xMagnLag;
                    break;
                }
                case 3: // ССП по БИНС
                {
                    calcData->x_kns = sspData->xBINS;
                    break;
                }
                case 4: // ССП по ГАНС
                {
                    calcData->x_kns = sspData->xGANS;
                    break;
                }
                case 5: // наблюдатель по СНС и БИНС
                {
                    calcData->x_kns = nablXSNSBINS->GetP();
                    //qDebug()<<"x_kns "<<calcData->x_kns<<" x_now "<<calcData->x_now<<" x_sens "<<calcData->x_sens;
                    break;
                }
                case 6: // наблюдатель по ГАНС и БИНС с ГДЛ
                {
                    calcData->x_kns = nablXGANSBINSLag->GetP();
                    //qDebug()<<"x_kns "<<calcData->x_kns<<" x_now "<<calcData->x_now<<" x_sens "<<calcData->x_sens;
                    break;
                }
                case 7: // наблюдатель по ГАНС и Магнитометру с ГДЛ
                {
                    calcData->x_kns = nablXGANSMagnLag->GetP();
                    break;
                }
                case 8: // наблюдатель по ГАНС и БИНС
                {
                    calcData->x_kns = nablXGANSBINS->GetP();
                    break;
                }
                }
            }
            xCount = 0;
        }
        if (zCount == kontrData.zPeriod) // период датчиа координаты Z
        {
            if (traekList[actualName].xzP)
            {
                calcData->z_sens=traekList[actualName].zPrev;
                traekList[actualName].zPrev=calcData->z_now+traekList[actualName].dZ+traekList[actualName].Vz_t+traekList[actualName].Az*sin(traekList[actualName].wz*T);
            }
            else
            {
                calcData->z_sens = calcData->z_now+traekList[actualName].dZ+traekList[actualName].Vz_t+traekList[actualName].Az*sin(traekList[actualName].wz*T);
            }
            if (isUseKNS)
            {
                // используем КНС для замыкания СУ
                switch(actualKNS)
                {
                case 0: // ССП по СНС
                {
                    calcData->z_kns = sspData->zSNS;
                    break;
                }
                case 1: // ССП по БИНС и ГДЛ
                {
                    calcData->z_kns = sspData->zBINSLag;
                    break;
                }
                case 2: // ССП по Магнитометру и ГДЛ
                {
                    calcData->z_kns = sspData->zMagnLag;
                    break;
                }
                case 3: // ССП по БИНС
                {
                    calcData->z_kns = sspData->zBINS;
                    //qDebug()<<"z_kns "<<calcData->z_kns<<" z_now "<<calcData->z_now<<" z_sens "<<calcData->z_sens;
                    break;
                }
                case 4: // ССП по ГАНС
                {
                    calcData->z_kns = sspData->zGANS;
                    break;
                }
                case 5: // наблюдатель по СНС и БИНС
                {
                    calcData->z_kns = nablZSNSBINS->GetP();
                    //qDebug()<<"z_kns "<<calcData->z_kns<<" z_now "<<calcData->z_now<<" z_sens "<<calcData->z_sens;
                    break;
                }
                case 6: // наблюдатель по ГАНС и БИНС с ГДЛ
                {
                    calcData->z_kns = nablZGANSBINSLag->GetP();
                    //qDebug()<<"z_kns "<<calcData->z_kns<<" z_now "<<calcData->z_now<<" z_sens "<<calcData->z_sens;
                    break;
                }
                case 7: // наблюдатель по ГАНС и Магнитометру с ГДЛ
                {
                    calcData->z_kns = nablZGANSMagnLag->GetP();
                    break;
                }
                case 8: // наблюдатель по ГАНС и БИНС
                {
                    calcData->z_kns = nablZGANSBINS->GetP();
                    break;
                }
                }
            }
            zCount = 0;
        }
        if (psiBINSCount == sensorsData.psiBINSPeriod) // период угла курса БИНС
        {
            if (sensorsData.psiPBINS)
            {
                sensorsData.psiBINS = sensorsData.psiPrevBINS;
                sensorsData.psiPrevBINS = calcData->psi_now*Kc + sensorsData.psi0BINS + sensorsData.DwyBINS_t;
            }
            else
            {
                sensorsData.psiBINS = calcData->psi_now*Kc + sensorsData.psi0BINS + sensorsData.DwyBINS_t;
            }
            psiBINSCount = 0;
            //kalman->calcKalman(sspData.Ksi - sensorsData.xSNS, sspData.Dzita - sensorsData.zSNS, sensorsData.psiBINS/Kc);
        }
        if (psiMagnCount == sensorsData.psiMagnPeriod) // период угла курса магнитометра
        {
            if (sensorsData.psiPMagn)
            {
                sensorsData.psiMagn = sensorsData.psiPrevMagn;
                sensorsData.psiPrevMagn = calcData->psi_now*Kc + sensorsData.psi0Magn + sensorsData.psiAMagn*sin(sensorsData.psiWMagn*T);
            }
            else
            {
                sensorsData.psiMagn = calcData->psi_now*Kc + sensorsData.psi0Magn + sensorsData.psiAMagn*sin(sensorsData.psiWMagn*T);
            }
            psiMagnCount = 0;
        }
        if (VxBINSCount == sensorsData.VxBINSPeriod) // период северной скорости БИНС
        {
            if (sensorsData.VxPBINS)
            {
                sensorsData.VxBINS = sensorsData.VxPrevBINS;
                sensorsData.VxPrevBINS = calcData->dx+sensorsData.Vx0BINS+sensorsData.AxBINS_t+sensorsData.AAxBINS_t;
            }
            else
            {
                sensorsData.VxBINS = calcData->dx+sensorsData.Vx0BINS+sensorsData.AxBINS_t+sensorsData.AAxBINS_t;
            }
            VxBINSCount = 0;
        }
        if (VzBINSCount == sensorsData.VzBINSPeriod) // период восточной скорости БИНС
        {
            if (sensorsData.VzPBINS)
            {
                sensorsData.VzBINS = sensorsData.VzPrevBINS;
                sensorsData.VzPrevBINS = calcData->dz+sensorsData.Vz0BINS+sensorsData.AzBINS_t+sensorsData.AAzBINS_t;
            }
            else
            {
                sensorsData.VzBINS = calcData->dz+sensorsData.Vz0BINS+sensorsData.AzBINS_t+sensorsData.AAzBINS_t;
            }
            VzBINSCount = 0;
        }
        if (VxLagCount == sensorsData.VxLagPeriod) // период продольной скорости ГДЛ
        {
            if (sensorsData.VxPLag)
            {
                sensorsData.VxLag = sensorsData.VxPrevLag;
                sensorsData.VxPrevLag = (1+sensorsData.mxLag)*calcData->Vx+sensorsData.Vx0Lag+sensorsData.VxALag*sin(sensorsData.VxWLag*T);
            }
            else
            {
                sensorsData.VxLag = (1+sensorsData.mxLag)*calcData->Vx+sensorsData.Vx0Lag+sensorsData.VxALag*sin(sensorsData.VxWLag*T);
            }
            //sensorsData.VxLag = (1+sensorsData.mxLag)*(calcData->dx*cos(calcData->psi_now)+calcData->dz*sin(calcData->psi_now));
            VxLagCount = 0;
        }
        if (VzLagCount == sensorsData.VzLagPeriod) // период поперечной скорости ГДЛ
        {
            if (sensorsData.VzPLag)
            {
                sensorsData.VzLag = sensorsData.VzPrevLag;
                sensorsData.VzPrevLag = sensorsData.Vz0Lag+sensorsData.VzALag*sin(sensorsData.VzWLag*T);
            }
            else
            {
                sensorsData.VzLag = sensorsData.Vz0Lag+sensorsData.VzALag*sin(sensorsData.VzWLag*T);
            }
            //sensorsData.VzLag = (1+sensorsData.mzLag)*((-1)*calcData->dx*sin(calcData->psi_now)+calcData->dz*cos(calcData->psi_now));
            VzLagCount = 0;
        }
        if (xSNSCount == sensorsData.xSNSPeriod) // период координаты x СНС
        {
            if (sensorsData.xPSNS)
            {
                sensorsData.xSNS = sensorsData.xPrevSNS;
                sensorsData.xPrevSNS = calcData->x_sens+sensorsData.x0SNS+sensorsData.xASNS*sin(sensorsData.xWSNS*T);
            }
            else
            {
                sensorsData.xSNS = calcData->x_sens+sensorsData.x0SNS+sensorsData.xASNS*sin(sensorsData.xWSNS*T);
            }
            xSNSCount = 0;
        }
        if (zSNSCount == sensorsData.zSNSPeriod) // период координаты z СНС
        {
            if (sensorsData.zPSNS)
            {
                sensorsData.zSNS = sensorsData.zPrevSNS;
                sensorsData.zPrevSNS = calcData->z_sens+sensorsData.z0SNS+sensorsData.zASNS*sin(sensorsData.zWSNS*T);
            }
            else
            {
                sensorsData.zSNS = calcData->z_sens+sensorsData.z0SNS+sensorsData.zASNS*sin(sensorsData.zWSNS*T);
            }
            zSNSCount = 0;
        }
        if (xGANSCount == sensorsData.xGANSPeriod) // период координаты x ГАНС
        {
            if (sensorsData.xPGANS)
            {
                sensorsData.xGANS = sensorsData.xPrevGANS;
                sensorsData.xPrevGANS = calcData->x_sens+sensorsData.x0GANS+sensorsData.xAGANS*sin(sensorsData.xWGANS*T);
            }
            else
            {
                sensorsData.xGANS = calcData->x_sens+sensorsData.x0GANS+sensorsData.xAGANS*sin(sensorsData.xWGANS*T);
            }
            xGANSCount = 0;
        }
        if (zGANSCount == sensorsData.zGANSPeriod) // период координаты z ГАНС
        {
            if (sensorsData.zPGANS)
            {
                sensorsData.zGANS = sensorsData.zPrevGANS;
                sensorsData.zPrevGANS = calcData->z_sens+sensorsData.z0GANS+sensorsData.zAGANS*sin(sensorsData.zWGANS*T);
            }
            else
            {
                sensorsData.zGANS = calcData->z_sens+sensorsData.z0GANS+sensorsData.zAGANS*sin(sensorsData.zWGANS*T);
            }
            zGANSCount = 0;
        }
        if (SNSCount == sspData->periodSNS) // период координат ССП по СНС
        {
            SNSCalc();
            SNSCount = 0;
        }
        if (BINSLagCount == sspData->periodBINSLag) // период координат ССП по БИНС и ГДЛ
        {
            BINSLagCalc();
            BINSLagCount = 0;
        }
        if (MagnLagCount == sspData->periodMagnLag) // период координат ССП по Магнитометру и ГДЛ
        {
            MagnLagCalc();
            MagnLagCount = 0;
        }
        if (BINSCount == sspData->periodBINS) // период координат ССП по БИНС
        {
            BINSCalc();
            BINSCount = 0;
        }
        if (GANSCount == sspData->periodGANS) // период координат ССП по ГАНС
        {
            GANSCalc();
            GANSCount = 0;
        }
        if (SNSBINSCount == nablData->periodSNSBINS) // период координат наблюдателя по СНС и БИНС
        {
            SNSBINSCalc(T);
            SNSBINSCount = 0;
        }
        if (GANSBINSLagCount == nablData->periodGANSBINSLag) // период координат наблюдателя по ГАНС и БИНС с ГДЛ
        {
            GANSBINSLagCalc(T);
            GANSBINSLagCount = 0;
        }
        if (GANSMagnLagCount == nablData->periodGANSMagnLag) // период координат наблюдателя по ГАНС и Магнитометру с ГДЛ
        {
            GANSMagnLagCalc(T);
            GANSMagnLagCount = 0;
        }
        if (GANSBINSCount == nablData->periodGANSBINS) // период координат наблюдателя по ГАНС и БИНС
        {
            GANSBINSCalc(T);
            GANSBINSCount = 0;
        }
        if (fabs(T-nablData->timeSNSBINS)<Ttimer*0.9) // переходим к коррекции наблюдателя по СНС и БИНС
        {
            nablXSNSBINS->SetCorrect();
            nablZSNSBINS->SetCorrect();
            //qDebug()<<"start correct";
        }
        if (fabs(T-nablData->timeGANSBINSLag)<Ttimer*0.9) // переходим к коррекции наблюдателя по ГАНС и БИНС с ГДЛ
        {
            nablXGANSBINSLag->SetCorrect();
            nablZGANSBINSLag->SetCorrect();
        }
        if (fabs(T-nablData->timeGANSMagnLag)<Ttimer*0.9) // переходим к коррекции наблюдателя по ГАНС и Магнитометру с ГДЛ
        {
            nablXGANSMagnLag->SetCorrect();
            nablZGANSMagnLag->SetCorrect();
        }
        if (fabs(T-nablData->timeGANSBINS)<Ttimer*0.9) // переходим к коррекции наблюдателя по ГАНС и БИНС
        {
            nablXGANSBINS->SetCorrect();
            nablZGANSBINS->SetCorrect();
        }
        if (!isUseKNS)
        {
            if (pow(fabs(calcData->x_dist-calcData->x_sens),2)+pow(fabs(calcData->z_dist-calcData->z_sens),2) <= pow(traekList[actualName].R,2))
            {
                qDebug()<<"T "<<T;
                // дошли до очередной точки траектории
                if (actual_num<pointsVec.size())
                {
                    calcData->x_dist = pointsVec[actual_num].x;
                    calcData->z_dist = pointsVec[actual_num].y;
                    actual_num++;
                }
                else
                {
                    // дошли до последней точки траектории
                    if (traekList[actualName].R != 1)
                        traekList[actualName].R = 1;
                    else
                        not_end = false;
                }
            }
        }
        else
        {
            if (pow(fabs(calcData->x_dist-calcData->x_kns),2)+pow(fabs(calcData->z_dist-calcData->z_kns),2) <= pow(traekList[actualName].R,2))
            {
                qDebug()<<"T "<<T;
                // дошли до очередной точки траектории
                if (actual_num<pointsVec.size())
                {
                    calcData->x_dist = pointsVec[actual_num].x;
                    calcData->z_dist = pointsVec[actual_num].y;
                    actual_num++;
                }
                else
                {
                    // дошли до последней точки траектории
                    //if (traekList[actualName].R != 1)
                    //    traekList[actualName].R = 1;
                    //else
                        not_end = false;
                }
            }
        }

        Runge(Ttimer); // интегрирование производим на каждом шаге
        T+=Ttimer; //увеличиваем время
        if (graphCount == graphPeriod) // период добавления точек в отображение траектории
        {
            ui->widgetGraphic->addPoint(actualName,calcData->z_now,calcData->x_now);
            graphicPsiBINS->addPoint("psiBINS", T, sensorsData.psiBINS);
            graphicPsiMagn->addPoint("psiMagn", T, sensorsData.psiMagn);
            graphicVxBINS->addPoint("VxBINS", T, sensorsData.VxBINS);
            graphicVzBINS->addPoint("VzBINS", T, sensorsData.VzBINS);
            graphicVxLag->addPoint("VxLag", T, sensorsData.VxLag);
            graphicVzLag->addPoint("VzLag", T, sensorsData.VzLag);
            graphicXSNS->addPoint("XSNS", T, sensorsData.xSNS);
            graphicXSNS->addPoint("Xideal", T, calcData->x_now);
            graphicZSNS->addPoint("ZSNS", T, sensorsData.zSNS);
            graphicXGANS->addPoint("XGANS", T, sensorsData.xGANS);
            graphicZGANS->addPoint("ZGANS", T, sensorsData.zGANS);

            graphicXZ->addPoint("SNS", sspData->zSNS, sspData->xSNS);
            graphicXZ->addPoint("BINS_Lag", sspData->zBINSLag, sspData->xBINSLag);
            graphicXZ->addPoint("Magn_Lag", sspData->zMagnLag, sspData->xMagnLag);
            graphicXZ->addPoint("BINS", sspData->zBINS, sspData->xBINS);
            graphicXZ->addPoint("GANS", sspData->zGANS, sspData->xGANS);
            graphicXZ->addPoint("SNS_BINS", nablZSNSBINS->GetP(), nablXSNSBINS->GetP());
            graphicXZ->addPoint("GANS_BINS_Lag", nablZGANSBINSLag->GetP(), nablXGANSBINSLag->GetP());
            graphicXZ->addPoint("GANS_Magn_Lag", nablZGANSMagnLag->GetP(), nablXGANSMagnLag->GetP());
            graphicXZ->addPoint("GANS_BINS", nablZGANSBINS->GetP(), nablXGANSBINS->GetP());

            graphicX->addPoint("SNS", T, sspData->xSNS);
            graphicX->addPoint("BINS_Lag", T, sspData->xBINSLag);
            graphicX->addPoint("Magn_Lag", T, sspData->xMagnLag);
            graphicX->addPoint("BINS", T, sspData->xBINS);
            graphicX->addPoint("GANS", T, sspData->xGANS);
            graphicX->addPoint("SNS_BINS", T, nablXSNSBINS->GetP());
            graphicX->addPoint("GANS_BINS_Lag", T, nablXGANSBINSLag->GetP());
            graphicX->addPoint("GANS_Magn_Lag", T, nablXGANSMagnLag->GetP());
            graphicX->addPoint("GANS_BINS", T, nablXGANSBINS->GetP());

            graphicZ->addPoint("SNS", T, sspData->zSNS);
            graphicZ->addPoint("BINS_Lag", T, sspData->zBINSLag);
            graphicZ->addPoint("Magn_Lag", T, sspData->zMagnLag);
            graphicZ->addPoint("BINS", T, sspData->zBINS);
            graphicZ->addPoint("GANS", T, sspData->zGANS);
            graphicZ->addPoint("SNS_BINS", T, nablZSNSBINS->GetP());
            graphicZ->addPoint("GANS_BINS_Lag", T, nablZGANSBINSLag->GetP());
            graphicZ->addPoint("GANS_Magn_Lag", T, nablZGANSMagnLag->GetP());
            graphicZ->addPoint("GANS_BINS", T, nablZGANSBINS->GetP());

            graphicdX->addPoint("SNS", T, sspData->dxSNS);
            graphicdX->addPoint("BINS_Lag", T, sspData->dxBINSLag);
            graphicdX->addPoint("Magn_Lag", T, sspData->dxMagnLag);
            graphicdX->addPoint("BINS", T, sspData->dxBINS);
            graphicdX->addPoint("GANS", T, sspData->dxGANS);
            graphicdX->addPoint("SNS_BINS", T, nablData->dxSNSBINS);
            graphicdX->addPoint("GANS_BINS_Lag", T, nablData->dxGANSBINSLag);
            graphicdX->addPoint("GANS_Magn_Lag", T, nablData->dxGANSMagnLag);
            graphicdX->addPoint("GANS_BINS", T, nablData->dxGANSBINS);

            graphicdZ->addPoint("SNS", T, sspData->dzSNS);
            graphicdZ->addPoint("BINS_Lag", T, sspData->dzBINSLag);
            graphicdZ->addPoint("Magn_Lag", T, sspData->dzMagnLag);
            graphicdZ->addPoint("BINS", T, sspData->dzBINS);
            graphicdZ->addPoint("GANS", T, sspData->dzGANS);
            graphicdZ->addPoint("SNS_BINS", T, nablData->dzSNSBINS);
            graphicdZ->addPoint("GANS_BINS_Lag", T, nablData->dzGANSBINSLag);
            graphicdZ->addPoint("GANS_Magn_Lag", T, nablData->dzGANSMagnLag);
            graphicdZ->addPoint("GANS_BINS", T, nablData->dzGANSBINS);

            graphicVx->addPoint("BINS_Lag", T, sspData->vxBINSLag);
            graphicVx->addPoint("Magn_Lag", T, sspData->vxMagnLag);
            graphicVx->addPoint("BINS", T, sspData->vxBINS);
            graphicVx->addPoint("SNS_BINS", T, nablXSNSBINS->GetVp());
            graphicVx->addPoint("GANS_BINS_Lag", T, nablXGANSBINSLag->GetVp());
            graphicVx->addPoint("GANS_Magn_Lag", T, nablXGANSMagnLag->GetVp());
            graphicVx->addPoint("GANS_BINS", T, nablXGANSBINS->GetVp());

            graphicVz->addPoint("BINS_Lag", T, sspData->vzBINSLag);
            graphicVz->addPoint("Magn_Lag", T, sspData->vzMagnLag);
            graphicVz->addPoint("BINS", T, sspData->vzBINS);
            graphicVz->addPoint("SNS_BINS", T, nablZGANSBINS->GetVp());
            graphicVz->addPoint("GANS_BINS_Lag", T, nablZGANSBINSLag->GetVp());
            graphicVz->addPoint("GANS_Magn_Lag", T, nablZGANSMagnLag->GetVp());
            graphicVz->addPoint("GANS_BINS", T, nablZGANSBINS->GetVp());

            graphicdVx->addPoint("BINS_Lag", T, sspData->dVxBINSLag);
            graphicdVx->addPoint("Magn_Lag", T, sspData->dVxMagnLag);
            graphicdVx->addPoint("BINS", T, sspData->dVxBINS);
            graphicdVx->addPoint("SNS_BINS", T, nablData->dVxSNSBINS);
            graphicdVx->addPoint("GANS_BINS_Lag", T, nablData->dVxGANSBINSLag);
            graphicdVx->addPoint("GANS_Magn_Lag", T, nablData->dVxGANSMagnLag);
            graphicdVx->addPoint("GANS_BINS", T, nablData->dVxGANSBINS);

            graphicdVz->addPoint("BINS_Lag", T, sspData->dVzBINSLag);
            graphicdVz->addPoint("Magn_Lag", T, sspData->dVzMagnLag);
            graphicdVz->addPoint("BINS", T, sspData->dVzBINS);
            graphicdVz->addPoint("SNS_BINS", T, nablData->dVzSNSBINS);
            graphicdVz->addPoint("GANS_BINS_Lag", T, nablData->dVzGANSBINSLag);
            graphicdVz->addPoint("GANS_Magn_Lag", T, nablData->dVzGANSMagnLag);
            graphicdVz->addPoint("GANS_BINS", T, nablData->dVzGANSBINS);

            graphCount = 0;

        }
        //qDebug()<<"T "<<T <<" L_dist "<<calcData->L_dist<<"x_dis "<<calcData->x_dist<<" x_sens "<<calcData->x_sens; //<<" x "<<"X Kalman2 "<<kalman->getKalman2_X()<<" X SSP "<<sspData->xBINSLag;
        if (T>MaxTime)
            not_end = false;

        // увеличиваем счетчики периодов
        systCount++;
        xCount++;
        zCount++;
        graphCount++;
        psiBINSCount++;
        psiMagnCount++;
        VxBINSCount++;
        VzBINSCount++;
        VxLagCount++;
        VzLagCount++;
        xSNSCount++;
        zSNSCount++;
        xGANSCount++;
        zGANSCount++;
        SNSCount++;
        BINSLagCount++;
        MagnLagCount++;
        BINSCount++;
        GANSCount++;
        SNSBINSCount++;
        GANSBINSLagCount++;
        GANSMagnLagCount++;
        GANSBINSCount++;
        nablData->correctGANSBINSCount++;
        nablData->correctGANSBINSLagCount++;
        nablData->correctGANSMagnLagCount++;
        nablData->correctSNSBINSCount++;

        // для обновления отображения на экране
        QApplication::processEvents();
    }
}

void MainWidget::Control_system()
{
    // контура управления

    // контур марша - считаем задающее воздействие на двигатели
    //calcData->Ux = calcData->Umax; // в моем случае разомкнутый максимум
    //calcData->Ux = 10;

    // контур курса - считаем задающее воздействие на двигатели
    double Psi_arctg = 0;
    if (!isUseKNS)
        Psi_arctg = (atan2(calcData->z_dist-calcData->z_sens,calcData->x_dist-calcData->x_sens))*Kc; // угол курса как арктангенс разницы координат в диапазоне +-180
    else
        Psi_arctg = (atan2(calcData->z_dist-calcData->z_kns,calcData->x_dist-calcData->x_kns))*Kc; // угол курса как арктангенс разницы координат в диапазоне +-180
    // коэффициент K3
    /*double Psi_arctgK3 = Psi_arctg;//kontrData.K3*Psi_arctg;
    // ПИ-регулятор
    double Psi_Pi = calcData->xz_pi*calcData->Ktxz;
    if (Psi_Pi>1)
        Psi_Pi = 1;
    else if (Psi_Pi<-1)
        Psi_Pi = -1;
    Psi_arctg = Psi_arctgK3+Psi_Pi;*/
    // перевод в абсолютный угол
    /*if (Psi_arctg<0)
        Psi_arctg = 360+Psi_arctg; // переводим в диапазон 0-360*/
    if ((Psi_arctg-calcData->psi_dist[1]) > 170)  calcData->count_psi=calcData->count_psi-1;
    if ((Psi_arctg-calcData->psi_dist[1]) < (-170)) calcData->count_psi=calcData->count_psi+1;
    calcData->psi_dist[1]=Psi_arctg;
    calcData->psi_dist[0] = Psi_arctg+360*calcData->count_psi;
    //qDebug()<<"psi "<<Psi_arctg;

    // контур марша
    /*if ((calcData->psi_now-calcData->psi_now1[1]) > 170) calcData->psi_now_count=calcData->psi_now_count-1;
    if ((calcData->psi_now-calcData->psi_now1[1]) < (-170)) calcData->psi_now_count=calcData->psi_now_count+1;
    calcData->psi_now1[1]=calcData->psi_now;
    calcData->psi_now1[0] = calcData->psi_now+360*calcData->count_psi;*/
    if (!isUseKNS)
        calcData->L_dist = (calcData->x_dist-calcData->x_sens)*(cos(calcData->psi_now)) + (calcData->z_dist-calcData->z_sens)*(sin(calcData->psi_now));
    else
        calcData->L_dist = (calcData->x_dist-calcData->x_kns)*(cos(calcData->psi_now)) + (calcData->z_dist-calcData->z_kns)*(sin(calcData->psi_now));

    // ПИ-регулятор
    double L_Pi = calcData->xz_pi*calcData->Ktxz;
    if (L_Pi>10)
        L_Pi = 10;
    else if (L_Pi<-10)
        L_Pi = -10;
    calcData->L_pi = calcData->L_dist+L_Pi;

    // теперь формируем напряжение
    //calcData->Upsi = ((calcData->psi_dist[0]-calcData->psi_now*Kc)+calcData->psi_pi*calcData->Ktpsi)*kontrData.K1-calcData->wy*Kc*kontrData.K2;
    calcData->Upsi = ((calcData->psi_dist[0]-calcData->psi_now*Kc))*kontrData.K1-calcData->wy*Kc*kontrData.K2;
    calcData->Ux = (calcData->L_pi)*kontrData.K3-calcData->Vx*kontrData.K4;

    // учитываем ограничение напряжения
    if (calcData->Upsi>calcData->Umax)
        calcData->Upsi = calcData->Umax;
    else if (calcData->Upsi<-calcData->Umax)
        calcData->Upsi = -calcData->Umax;
    if (calcData->Ux>10)
        calcData->Ux = 10;
    else if (calcData->Ux<-10)
        calcData->Ux = -10;
}

void MainWidget::Vehicles_Model()
{
    // модель аппарата

    // контур марша
    calcData->dVx = (calcData->Fdx-calcData->Cx1*calcData->Vx*fabs(calcData->Vx)-calcData->Cx2*calcData->Vx)/(calcData->L11+calcData->m);

    // контур курса
    calcData->dwy = (calcData->Mdy-calcData->Cwy1*calcData->wy*fabs(calcData->wy)-calcData->Cwy2*calcData->wy)/(calcData->L55+calcData->Iy);
    calcData->dpsi = calcData->wy;

    // ДРК
    calcData->dFdx = (calcData->Kdv_x*calcData->Ux-calcData->Fdx)/calcData->Tdv;
    calcData->dMdy = (calcData->Kdv_psi*calcData->Upsi-calcData->Mdy)/calcData->Tdv;

    // контур координат
    calcData->dx = calcData->Vx*cos(calcData->psi_now)+traekList[actualName].Vx;
    calcData->dz = calcData->Vx*sin(calcData->psi_now)+traekList[actualName].Vz;

    //  ПИ-регулятор контура курса
    calcData->dpsi_pi = (calcData->psi_dist[0]-calcData->psi_now*Kc)*kontrData.K1;

    // ПИ-регулятор контура координат
    calcData->dxz_pi = calcData->L_dist;//*kontrData.K3;
}

void MainWidget::Runge(double step)
{
    // создаем переменные для метода Рунге-Кутта 4-го порядка
    CalcStruct* tempCalc = new CalcStruct();

    double tempVx_t = 0;
    double tempVz_t = 0;
    double tempDwyBINS_t = 0;
    double tempXSSPBINSGDL = 0;
    double tempZSSPBINSGDL = 0;
    double tempXSSPMagnBINS = 0;
    double tempZSSPMagnBINS = 0;
    double tempXSSPBINS = 0;
    double tempZSSPBINS = 0;

    double tempdVx = 0;
    double tempdVz = 0;
    double tempDwyBINS = 0;
    double tempVxSSPBINSGDL = 0;
    double tempVzSSPBINSGDL = 0;
    double tempVxSSPMagnBINS = 0;
    double tempVzSSPMagnBINS = 0;
    double tempVxSSPBINS = 0;
    double tempVzSSPBINS = 0;

    // по лекциям
    Vehicles_Model();

    tempCalc->x_now = calcData->x_now;
    tempCalc->z_now = calcData->z_now;
    tempCalc->Vx = calcData->Vx;
    tempCalc->wy = calcData->wy;
    tempCalc->psi_now = calcData->psi_now;
    tempCalc->Fdx = calcData->Fdx;
    tempCalc->Mdy = calcData->Mdy;
    tempCalc->x_gans = calcData->x_gans;
    tempCalc->z_gans = calcData->z_gans;
    tempCalc->psi_pi = calcData->psi_pi;
    tempCalc->xz_pi = calcData->xz_pi;
    tempVx_t = traekList[actualName].Vx_t;
    tempVz_t = traekList[actualName].Vz_t;
    tempDwyBINS_t = sensorsData.DwyBINS_t;
    tempXSSPBINSGDL = sspData->xBINSLag;
    tempZSSPBINSGDL = sspData->zBINSLag;
    tempXSSPMagnBINS = sspData->xMagnLag;
    tempZSSPMagnBINS = sspData->zMagnLag;
    tempXSSPBINS = sspData->xBINS;
    tempZSSPBINS = sspData->zBINS;

    tempCalc->dx = calcData->dx;
    tempCalc->dz = calcData->dz;
    tempCalc->dVx = calcData->dVx;
    tempCalc->dwy = calcData->dwy;
    tempCalc->dpsi = calcData->dpsi;
    tempCalc->dFdx = calcData->dFdx;
    tempCalc->dMdy = calcData->dMdy;
    tempCalc->dpsi_pi = calcData->dpsi_pi;
    tempCalc->dxz_pi = calcData->dxz_pi;
    tempdVx = traekList[actualName].dVx;
    tempdVz = traekList[actualName].dVz;
    tempDwyBINS = sensorsData.DwyBINS;
    tempVxSSPBINSGDL = sspData->vxBINSLag;
    tempVzSSPBINSGDL = sspData->vzBINSLag;
    tempVxSSPMagnBINS = sspData->vxMagnLag;
    tempVzSSPMagnBINS = sspData->vzMagnLag;
    tempVxSSPBINS = sspData->vxBINS;
    tempVzSSPBINS = sspData->vzBINS;

    calcData->x_now = tempCalc->x_now+0.5*step*calcData->dx;
    calcData->z_now = tempCalc->z_now+0.5*step*calcData->dz;
    calcData->Vx = tempCalc->Vx+0.5*step*calcData->dVx;
    calcData->wy = tempCalc->wy+0.5*step*calcData->dwy;
    calcData->psi_now = tempCalc->psi_now+0.5*step*calcData->dpsi;
    calcData->Fdx = tempCalc->Fdx+0.5*step*calcData->dFdx;
    calcData->Mdy = tempCalc->Mdy+0.5*step*calcData->dMdy;
    calcData->x_gans = tempCalc->x_gans+0.5*step*calcData->Vx;
    calcData->z_gans = 0;
    calcData->psi_pi = tempCalc->psi_pi+0.5*step*calcData->dpsi_pi;
    calcData->xz_pi = tempCalc->xz_pi+0.5*step*calcData->dxz_pi;
    traekList[actualName].Vx_t = tempVx_t + 0.5*step*traekList[actualName].dVx;
    traekList[actualName].Vz_t = tempVz_t + 0.5*step*traekList[actualName].dVz;
    sensorsData.DwyBINS_t = tempDwyBINS_t + 0.5*step*sensorsData.DwyBINS;
    sspData->xBINSLag = tempXSSPBINSGDL + 0.5*step*sspData->vxBINSLag;
    sspData->zBINSLag = tempZSSPBINSGDL + 0.5*step*sspData->vzBINSLag;
    sspData->xMagnLag = tempXSSPMagnBINS + 0.5*step*sspData->vxMagnLag;
    sspData->zMagnLag = tempZSSPMagnBINS + 0.5*step*sspData->vzMagnLag;
    sspData->xBINS = tempXSSPBINS + 0.5*step*sspData->vxBINS;
    sspData->zBINS = tempZSSPBINS + 0.5*step*sspData->vzBINS;

    // второй подход
    Vehicles_Model();

    tempCalc->dx = tempCalc->dx + 2.0*calcData->dx;
    tempCalc->dz = tempCalc->dz + 2.0*calcData->dz;
    tempCalc->dVx = tempCalc->dVx + 2.0*calcData->dVx;
    tempCalc->dwy = tempCalc->dwy + 2.0*calcData->dwy;
    tempCalc->dpsi = tempCalc->dpsi + 2.0*calcData->dpsi;
    tempCalc->dFdx = tempCalc->dFdx + 2.0*calcData->dFdx;
    tempCalc->dMdy = tempCalc->dMdy + 2.0*calcData->dMdy;
    tempCalc->dpsi_pi = tempCalc->dpsi_pi + 2.0*calcData->dpsi_pi;
    tempCalc->dxz_pi = tempCalc->dxz_pi + 2.0*calcData->dxz_pi;
    tempdVx = tempdVx + 2.0*traekList[actualName].dVx;
    tempdVz = tempdVz + 2.0*traekList[actualName].dVz;
    tempDwyBINS = tempDwyBINS + 2.0*sensorsData.DwyBINS;
    tempVxSSPBINSGDL = tempVxSSPBINSGDL+ 2.0*sspData->vxBINSLag;
    tempVzSSPBINSGDL = tempVzSSPBINSGDL+ 2.0*sspData->vzBINSLag;
    tempVxSSPMagnBINS = tempVxSSPMagnBINS+ 2.0*sspData->vxMagnLag;
    tempVzSSPMagnBINS = tempVzSSPMagnBINS+ 2.0*sspData->vzMagnLag;
    tempVxSSPBINS = tempVxSSPBINS+ 2.0*sspData->vxBINS;
    tempVzSSPBINS = tempVzSSPBINS+ 2.0*sspData->vzBINS;

    calcData->x_now = tempCalc->x_now+0.5*step*calcData->dx;
    calcData->z_now = tempCalc->z_now+0.5*step*calcData->dz;
    calcData->Vx = tempCalc->Vx+0.5*step*calcData->dVx;
    calcData->wy = tempCalc->wy+0.5*step*calcData->dwy;
    calcData->psi_now = tempCalc->psi_now+0.5*step*calcData->dpsi;
    calcData->Fdx = tempCalc->Fdx+0.5*step*calcData->dFdx;
    calcData->Mdy = tempCalc->Mdy+0.5*step*calcData->dMdy;
    calcData->x_gans = tempCalc->x_gans+0.5*step*calcData->Vx;
    calcData->z_gans = 0;
    calcData->psi_pi = tempCalc->psi_pi+0.5*step*calcData->dpsi_pi;
    calcData->xz_pi = tempCalc->xz_pi+0.5*step*calcData->dxz_pi;
    traekList[actualName].Vx_t = tempVx_t + 0.5*step*traekList[actualName].dVx;
    traekList[actualName].Vz_t = tempVz_t + 0.5*step*traekList[actualName].dVz;
    sensorsData.DwyBINS_t = tempDwyBINS_t + 0.5*step*sensorsData.DwyBINS;
    sspData->xBINSLag = tempXSSPBINSGDL + 0.5*step*sspData->vxBINSLag;
    sspData->zBINSLag = tempZSSPBINSGDL + 0.5*step*sspData->vzBINSLag;
    sspData->xMagnLag = tempXSSPMagnBINS + 0.5*step*sspData->vxMagnLag;
    sspData->zMagnLag = tempZSSPMagnBINS + 0.5*step*sspData->vzMagnLag;
    sspData->xBINS = tempXSSPBINS + 0.5*step*sspData->vxBINS;
    sspData->zBINS = tempZSSPBINS + 0.5*step*sspData->vzBINS;

    // третий подход
    Vehicles_Model();

    tempCalc->dx = tempCalc->dx + 2.0*calcData->dx;
    tempCalc->dz = tempCalc->dz + 2.0*calcData->dz;
    tempCalc->dVx = tempCalc->dVx + 2.0*calcData->dVx;
    tempCalc->dwy = tempCalc->dwy + 2.0*calcData->dwy;
    tempCalc->dpsi = tempCalc->dpsi + 2.0*calcData->dpsi;
    tempCalc->dFdx = tempCalc->dFdx + 2.0*calcData->dFdx;
    tempCalc->dMdy = tempCalc->dMdy + 2.0*calcData->dMdy;
    tempCalc->dpsi_pi = tempCalc->dpsi_pi + 2.0*calcData->dpsi_pi;
    tempCalc->dxz_pi = tempCalc->dxz_pi + 2.0*calcData->dxz_pi;
    tempdVx = tempdVx + 2.0*traekList[actualName].dVx;
    tempdVz = tempdVz + 2.0*traekList[actualName].dVz;
    tempDwyBINS = tempDwyBINS + 2.0*sensorsData.DwyBINS;
    tempVxSSPBINSGDL = tempVxSSPBINSGDL+ 2.0*sspData->vxBINSLag;
    tempVzSSPBINSGDL = tempVzSSPBINSGDL+ 2.0*sspData->vzBINSLag;
    tempVxSSPMagnBINS = tempVxSSPMagnBINS+ 2.0*sspData->vxMagnLag;
    tempVzSSPMagnBINS = tempVzSSPMagnBINS+ 2.0*sspData->vzMagnLag;
    tempVxSSPBINS = tempVxSSPBINS+ 2.0*sspData->vxBINS;
    tempVzSSPBINS = tempVzSSPBINS+ 2.0*sspData->vzBINS;

    calcData->x_now = tempCalc->x_now+step*calcData->dx;
    calcData->z_now = tempCalc->z_now+step*calcData->dz;
    calcData->Vx = tempCalc->Vx+step*calcData->dVx;
    calcData->wy = tempCalc->wy+step*calcData->dwy;
    calcData->psi_now = tempCalc->psi_now+step*calcData->dpsi;
    calcData->Fdx = tempCalc->Fdx+step*calcData->dFdx;
    calcData->Mdy = tempCalc->Mdy+step*calcData->dMdy;
    calcData->x_gans = tempCalc->x_gans+step*calcData->Vx;
    calcData->z_gans = 0;
    calcData->psi_pi = tempCalc->psi_pi+step*calcData->dpsi_pi;
    calcData->xz_pi = tempCalc->xz_pi+step*calcData->dxz_pi;
    traekList[actualName].Vx_t = tempVx_t + step*traekList[actualName].dVx;
    traekList[actualName].Vz_t = tempVz_t + step*traekList[actualName].dVz;
    sensorsData.DwyBINS_t = tempDwyBINS_t + step*sensorsData.DwyBINS;
    sspData->xBINSLag = tempXSSPBINSGDL + step*sspData->vxBINSLag;
    sspData->zBINSLag = tempZSSPBINSGDL + step*sspData->vzBINSLag;
    sspData->xMagnLag = tempXSSPMagnBINS + step*sspData->vxMagnLag;
    sspData->zMagnLag = tempZSSPMagnBINS + step*sspData->vzMagnLag;
    sspData->xBINS = tempXSSPBINS + step*sspData->vxBINS;
    sspData->zBINS = tempZSSPBINS + step*sspData->vzBINS;

    // четвертый подход
    Vehicles_Model();

    calcData->x_now = tempCalc->x_now+(step/6.0)*(tempCalc->dx + calcData->dx);
    calcData->z_now = tempCalc->z_now+(step/6.0)*(tempCalc->dz + calcData->dz);
    calcData->Vx = tempCalc->Vx+(step/6.0)*(tempCalc->dVx + calcData->dVx);
    calcData->wy = tempCalc->wy+(step/6.0)*(tempCalc->dwy + calcData->dwy);
    calcData->psi_now = tempCalc->psi_now+(step/6.0)*(tempCalc->dpsi + calcData->dpsi);
    calcData->Fdx = tempCalc->Fdx+(step/6.0)*(tempCalc->dFdx + calcData->dFdx);
    calcData->Mdy = tempCalc->Mdy+(step/6.0)*(tempCalc->dMdy + calcData->dMdy);
    calcData->x_gans = tempCalc->x_gans+(step/6.0)*(tempCalc->Vx+calcData->Vx);
    calcData->z_gans = 0;
    calcData->psi_pi = tempCalc->psi_pi+(step/6.0)*(tempCalc->dpsi_pi + calcData->dpsi_pi);
    calcData->xz_pi = tempCalc->xz_pi+(step/6.0)*(tempCalc->dxz_pi + calcData->dxz_pi);
    traekList[actualName].Vx_t = tempVx_t + (step/6.0)*(tempdVx + traekList[actualName].dVx);
    traekList[actualName].Vz_t = tempVz_t + (step/6.0)*(tempdVz + traekList[actualName].dVz);
    sensorsData.DwyBINS_t = tempDwyBINS_t + (step/6.0)*(tempDwyBINS + sensorsData.DwyBINS);
    sspData->xBINSLag = tempXSSPBINSGDL + (step/6.0)*(tempVxSSPBINSGDL + sspData->vxBINSLag);
    sspData->zBINSLag = tempZSSPBINSGDL + (step/6.0)*(tempVzSSPBINSGDL + sspData->vzBINSLag);
    sspData->xMagnLag = tempXSSPMagnBINS + (step/6.0)*(tempVxSSPMagnBINS + sspData->vxMagnLag);
    sspData->zMagnLag = tempZSSPMagnBINS + (step/6.0)*(tempVzSSPMagnBINS + sspData->vzMagnLag);
    sspData->xBINS = tempXSSPBINS + (step/6.0)*(tempVxSSPBINS + sspData->vxBINS);
    sspData->zBINS = tempZSSPBINS + (step/6.0)*(tempVzSSPBINS + sspData->vzBINS);

    // методом прямоугольника - пока единственный рабоает
    /*Vehicles_Model();
    SSPCalc();

    calcData->x_now += step*calcData->dx;
    calcData->z_now += step*calcData->dz;
    calcData->Vx += step*calcData->dVx;
    calcData->wy += step*calcData->dwy;
    calcData->psi_now += step*calcData->wy;
    calcData->Fdx += step*calcData->dFdx;
    calcData->Mdy += step*calcData->dMdy;
    traekList[actualName].Vx_t+= step*traekList[actualName].dVx;
    traekList[actualName].Vz_t += step*traekList[actualName].dVz;
    sensorsData.DwyBINS_t += step*sensorsData.DwyBINS;

    sspData.Ksi += step*sspData.Vksi;
    sspData.Dzita += step*sspData.Vdzi;*/

    delete tempCalc;
}

void MainWidget::SNSCalc()
{
    // расчет ССП по СНС
    sspData->xSNS = sensorsData.xSNS;
    sspData->zSNS = sensorsData.zSNS;

    // расчет ошибок
    sspData->dxSNS = calcData->x_sens - sspData->xSNS;
    sspData->dzSNS = calcData->z_sens - sspData->zSNS;
}

void MainWidget::BINSLagCalc()
{
    // расчет ССП по БИНС и ГДЛ
    double cosPsi = 0;
    double sinPsi = 0;
    double psiBINS = 0;
    if (sensorsData.psiBINS>180)
        psiBINS = (-1)*(sensorsData.psiBINS-180);
    else if (sensorsData.psiBINS<-180)
        psiBINS = (-1)*(sensorsData.psiBINS-180);
    else
        psiBINS = sensorsData.psiBINS;
    if (psiBINS>0 && psiBINS<90)
    {
        cosPsi = cos(psiBINS/Kc);
        sinPsi = sin(psiBINS/Kc);
    }
    else if (psiBINS==0)
    {
        cosPsi = 1;
        sinPsi = 0;
    }
    else if (psiBINS == 90)
    {
        cosPsi = 0;
        sinPsi = 1;
    }
    else if (psiBINS>90 && psiBINS<180)
    {
        psiBINS = 180-psiBINS;
        cosPsi = (-1)*cos(psiBINS/Kc);
        sinPsi = sin(psiBINS/Kc);
    }
    else if (psiBINS == 180)
    {
        cosPsi = -1;
        sinPsi = 0;
    }
    else if (psiBINS<0 && psiBINS> -90)
    {
        psiBINS = (-1)*psiBINS;
        cosPsi = cos(psiBINS/Kc);
        sinPsi = (-1)*sin(psiBINS/Kc);
    }
    else if (psiBINS == -90)
    {
        cosPsi = 0;
        sinPsi = -1;
    }
    else if (psiBINS<-90 && psiBINS>-180)
    {
        psiBINS = 180+psiBINS;
        cosPsi = (-1)*cos(psiBINS/Kc);
        sinPsi = (-1)*sin(psiBINS/Kc);
    }
    else if (psiBINS == -180)
    {
        cosPsi = 0;
        sinPsi = -1;
    }
    sspData->vxBINSLag = sensorsData.VxLag*cosPsi - sensorsData.VzLag*sinPsi;
    sspData->vzBINSLag = sensorsData.VxLag*sinPsi + sensorsData.VzLag*cosPsi;
    //qDebug()<<"cosPsi "<<cosPsi<<" sinPsi "<<sinPsi<<"vx "<<sensorsData.VxLag<<"vz "<<sensorsData.VzLag<<"vksi "<<sspData->vxBINSLag<<"vdzita "<<sspData->vzBINSLag<<"xGANS "<<sensorsData.xGANS<<"zGANS "<<sensorsData.zGANS;

    sspData->dVxBINSLag = calcData->dx - sspData->vxBINSLag;
    sspData->dVzBINSLag = calcData->dz - sspData->vzBINSLag;
    sspData->dxBINSLag = calcData->x_sens - sspData->xBINSLag;
    sspData->dzBINSLag = calcData->z_sens - sspData->zBINSLag;
}

void MainWidget::MagnLagCalc()
{
    // расчет ССП по Магнитометру и ГДЛ
    sspData->vxMagnLag = sensorsData.VxLag*cos(sensorsData.psiMagn/Kc) - sensorsData.VzLag*sin(sensorsData.psiMagn/Kc);
    sspData->vzMagnLag = sensorsData.VxLag*sin(sensorsData.psiMagn/Kc) + sensorsData.VzLag*cos(sensorsData.psiMagn/Kc);

    sspData->dVxMagnLag = calcData->dx - sspData->vxMagnLag;
    sspData->dVzMagnLag = calcData->dz - sspData->vzMagnLag;
    sspData->dxMagnLag = calcData->x_sens - sspData->xMagnLag;
    sspData->dzMagnLag = calcData->z_sens - sspData->zMagnLag;
}

void MainWidget::BINSCalc()
{
    // расчет ССП по БИНС
    sspData->vxBINS = sensorsData.VxBINS;
    sspData->vzBINS = sensorsData.VzBINS;

    sspData->dVxBINS = calcData->dx - sspData->vxBINS;
    sspData->dVzBINS = calcData->dz - sspData->vzBINS;
    sspData->dxBINS = calcData->x_sens - sspData->xBINS;
    sspData->dzBINS = calcData->z_sens - sspData->zBINS;
}

void MainWidget::GANSCalc()
{
    // расет ССП по ГАНС
    sspData->xGANS = sensorsData.xGANS;
    sspData->zGANS = sensorsData.zGANS;

    sspData->dxGANS = calcData->x_sens - sspData->xGANS;
    sspData->dzGANS = calcData->z_sens - sspData->zGANS;
}

void MainWidget::SNSBINSCalc(double T)
{
    // расчет наблюдателя по СНС и БИНС
    nablData->vxSNSBINS = sspData->vxBINS;
    nablData->vzSNSBINS = sspData->vzBINS;
    //qDebug()<<"T "<<T<<" time "<<nablData->timeSNSBINS<<" correct Count "<<nablData->correctSNSBINSCount<<" period2 "<<nablData->period2SNSBINS;
    if (nablData->timeSNSBINS>T)
    {
        // еще не время для коррекции
        nablData->xSNSBINS = sspData->xSNS;
        nablData->zSNSBINS = sspData->zSNS;
    }
    else
    {
        // время для коррекции
        if (nablData->correctSNSBINSCount >= nablData->period2SNSBINS)
        {
            nablData->xSNSBINS = sspData->xSNS;
            nablData->zSNSBINS = sspData->zSNS;
            nablXSNSBINS->Correct(nablData->xSNSBINS);
            nablZSNSBINS->Correct(nablData->zSNSBINS);
            nablData->correctSNSBINSCount = 0;
        }
    }
    nablXSNSBINS->Calc(nablData->periodSNSBINS*Ttimer,nablData->xSNSBINS,nablData->vxSNSBINS);
    nablZSNSBINS->Calc(nablData->periodSNSBINS*Ttimer,nablData->zSNSBINS,nablData->vzSNSBINS);

    nablData->dxSNSBINS = calcData->x_sens - nablXSNSBINS->GetP();
    nablData->dzSNSBINS = calcData->z_sens - nablZSNSBINS->GetP();
    nablData->dVxSNSBINS = calcData->dx - nablXSNSBINS->GetVp();
    nablData->dVzSNSBINS = calcData->dz - nablZSNSBINS->GetVp();
}

void MainWidget::GANSBINSLagCalc(double T)
{
    // расчет наблюдателя по ГАНС и БИНС с ГДЛ
    nablData->vxGANSBINSLag = sspData->vxBINSLag;
    nablData->vzGANSBINSLag = sspData->vzBINSLag;
    if (nablData->timeGANSBINSLag>T)
    {
        // еще не время для коррекции
        nablData->xGANSBINSLag = sspData->xGANS;
        nablData->zGANSBINSLag = sspData->zGANS;
        //qDebug()<<"in new z "<<nablData->zGANSBINSLag<<" to write "<<sspData->zGANS;
    }
    else
    {
        // время для коррекции
        if (nablData->correctGANSBINSLagCount >= nablData->period2GANSBINSLag)
        {
            nablData->xGANSBINSLag = sspData->xGANS;
            nablData->zGANSBINSLag = sspData->zGANS;
            nablXGANSBINSLag->Correct(nablData->xGANSBINSLag);
            nablZGANSBINSLag->Correct(nablData->zGANSBINSLag);
            nablData->correctGANSBINSLagCount = 0;
        }
    }
    //qDebug()<<"x nabl "<<nablData->xGANSBINSLag<<"z nabl "<<nablData->zGANSBINSLag;
    //double temp_z = nablData->zGANSBINSLag;
    nablXGANSBINSLag->Calc(nablData->periodGANSBINSLag*Ttimer,nablData->xGANSBINSLag,nablData->vxGANSBINSLag);
    nablZGANSBINSLag->Calc(nablData->periodGANSBINSLag*Ttimer,nablData->zGANSBINSLag,nablData->vzGANSBINSLag);

    nablData->dxGANSBINSLag = calcData->x_sens - nablXGANSBINSLag->GetP();
    nablData->dzGANSBINSLag = calcData->z_sens - nablZGANSBINSLag->GetP();
    nablData->dVxGANSBINSLag = calcData->dx - nablXGANSBINSLag->GetVp();
    nablData->dVzGANSBINSLag = calcData->dz - nablZGANSBINSLag->GetVp();
}

void MainWidget::GANSMagnLagCalc(double T)
{
    // расчет наблюдателя по ГАНС и магнитометр с ГДЛ
    nablData->vxGANSMagnLag = sspData->vxMagnLag;
    nablData->vzGANSMagnLag = sspData->vzMagnLag;
    if (nablData->timeGANSMagnLag>T)
    {
        // еще не время для коррекции
        nablData->xGANSMagnLag = sspData->xGANS;
        nablData->zGANSMagnLag = sspData->zGANS;
    }
    else
    {
        // время для коррекции
        if (nablData->correctGANSMagnLagCount >= nablData->period2GANSMagnLag)
        {
            nablData->xGANSMagnLag = sspData->xGANS;
            nablData->zGANSMagnLag = sspData->zGANS;
            nablXGANSMagnLag->Correct(nablData->xGANSMagnLag);
            nablZGANSMagnLag->Correct(nablData->zGANSMagnLag);
            nablData->correctGANSMagnLagCount = 0;
        }
    }
    nablXGANSMagnLag->Calc(nablData->periodGANSMagnLag*Ttimer,nablData->xGANSMagnLag,nablData->vxGANSMagnLag);
    nablZGANSMagnLag->Calc(nablData->periodGANSMagnLag*Ttimer,nablData->zGANSMagnLag,nablData->vzGANSMagnLag);

    nablData->dxGANSMagnLag = calcData->x_sens - nablXGANSMagnLag->GetP();
    nablData->dzGANSMagnLag = calcData->z_sens - nablZGANSMagnLag->GetP();
    nablData->dVxGANSMagnLag = calcData->dx - nablXGANSMagnLag->GetVp();
    nablData->dVzGANSMagnLag = calcData->dz - nablZGANSMagnLag->GetVp();
}

void MainWidget::GANSBINSCalc(double T)
{
    // расчет наблюдателя по ГАНС и БИНС
    nablData->vxGANSBINS = sspData->vxBINS;
    nablData->vzGANSBINS = sspData->vzBINS;
    if (nablData->timeGANSBINSLag>T)
    {
        // еще не время для коррекции
        nablData->xGANSBINS = sspData->xGANS;
        nablData->zGANSBINS = sspData->zGANS;
    }
    else
    {
        // время для коррекции
        if (nablData->correctGANSBINSCount >= nablData->period2GANSBINS)
        {
            nablData->xGANSBINS = sspData->xGANS;
            nablData->zGANSBINS = sspData->zGANS;
            nablXGANSBINS->Correct(nablData->xGANSBINS);
            nablZGANSBINS->Correct(nablData->zGANSBINS);
            nablData->correctGANSBINSCount = 0;
        }
    }
    nablXGANSBINS->Calc(nablData->periodGANSBINS*Ttimer,nablData->xGANSBINS,nablData->vxGANSBINS);
    nablZGANSBINS->Calc(nablData->periodGANSBINS*Ttimer,nablData->zGANSBINS,nablData->vzGANSBINS);

    nablData->dxGANSBINS = calcData->x_sens - nablXGANSBINS->GetP();
    nablData->dzGANSBINS = calcData->z_sens - nablZGANSBINS->GetP();
    nablData->dVxGANSBINS = calcData->dx - nablXGANSBINS->GetVp();
    nablData->dVzGANSBINS = calcData->dz - nablZGANSBINS->GetVp();
}

void MainWidget::on_pushButtonSetKonturs_clicked()
{
    // нажата кнопка установить параметры контура
    kontrData.K1 = ui->lineEditK1->text().toDouble();
    kontrData.K2 = ui->lineEditK2->text().toDouble();
    kontrData.K3 = ui->lineEditK3->text().toDouble();
    kontrData.K4 = ui->lineEditK3->text().toDouble();
    kontrData.systPeriod = ui->lineEditTsyst->text().toDouble();
    kontrData.xPeriod = kontrData.zPeriod = ui->lineEditTcoord->text().toDouble();
    Ttimer = ui->lineEditTtime->text().toDouble();
}

void MainWidget::ReadSensorsParams()
{
    // нажата кнопка применить параметры датчиков

    // угол курса БИНС
    if (ui->checkBoxBINSPsi0->isChecked())
        sensorsData.psi0BINS = ui->lineEditBINSPsi0->text().toDouble();
    else
        sensorsData.psi0BINS = 0;
    if (ui->checkBoxBINSwy->isChecked())
        sensorsData.DwyBINS = ui->lineEditBINSwy->text().toDouble();
    else
        sensorsData.DwyBINS = 0;
    sensorsData.psiPBINS = ui->checkBoxBINSPsiDelay->isChecked();
    if (ui->checkBoxBINSPsiT->isChecked())
        if (ui->lineEditBINSPsiT->text().toDouble() == 0)
            sensorsData.psiBINSPeriod = MaxTime+10;
        else
            sensorsData.psiBINSPeriod = ui->lineEditBINSPsiT->text().toDouble();
    else
        sensorsData.psiBINSPeriod = MaxTime+10;

    // угол курса магинтометра
    if (ui->checkBoxMagnPsi0->isChecked())
        sensorsData.psi0Magn = ui->lineEditMagnPsi0->text().toDouble();
    else
        sensorsData.psi0Magn = 0;
    if (ui->checkBoxMagnPsiS->isChecked())
    {
        sensorsData.psiAMagn = ui->lineEditMagnPsiA->text().toDouble();
        sensorsData.psiWMagn = ui->lineEditMagnPsiw->text().toDouble();
    }
    else
    {
        sensorsData.psiAMagn = 0;
        sensorsData.psiWMagn = 0;
    }
    sensorsData.psiPMagn = ui->checkBoxMagnPsiDelay->isChecked();
    if (ui->checkBoxMagnPsiT->isChecked())
        if (ui->lineEditMagnPsiT->text().toDouble() == 0)
            sensorsData.psiMagnPeriod = MaxTime+10;
        else
            sensorsData.psiMagnPeriod = ui->lineEditMagnPsiT->text().toDouble();
    else
        sensorsData.psiMagnPeriod = MaxTime+10;

    // северная скорость БИНС
    if (ui->checkBoxBINSVx0->isChecked())
        sensorsData.Vx0BINS = ui->lineEditBINSVx0->text().toDouble();
    else
        sensorsData.Vx0BINS = 0;
    if (ui->checkBoxBINSVx1->isChecked())
        sensorsData.AxBINS = ui->lineEditBINSVx1->text().toDouble();
    else
        sensorsData.AxBINS = 0;
    if (ui->checkBoxBINSVx2->isChecked())
        sensorsData.AAxBINS = ui->lineEditBINSVx2->text().toDouble();
    else
        sensorsData.AAxBINS = 0;
    sensorsData.VxPBINS = ui->checkBoxBINSVxDelay->isChecked();
    if (ui->checkBoxBINSVxT->isChecked())
        if (ui->lineEditBINSVxT->text().toDouble() == 0)
            sensorsData.VxBINSPeriod = MaxTime+10;
        else
            sensorsData.VxBINSPeriod = ui->lineEditBINSVxT->text().toDouble();
    else
        sensorsData.VxBINSPeriod = MaxTime+10;

    // восточная скорость БИНС
    if (ui->checkBoxBINSVz0->isChecked())
        sensorsData.Vz0BINS = ui->lineEditBINSVz0->text().toDouble();
    else
        sensorsData.Vz0BINS = 0;
    if (ui->checkBoxBINSVz1->isChecked())
        sensorsData.AzBINS = ui->lineEditBINSVz1->text().toDouble();
    else
        sensorsData.AzBINS = 0;
    if (ui->checkBoxBINSVz2->isChecked())
        sensorsData.AAzBINS = ui->lineEditBINSVz2->text().toDouble();
    else
        sensorsData.AAzBINS = 0;
    sensorsData.VzPBINS = ui->checkBoxBINSVzDelay->isChecked();
    if (ui->checkBoxBINSVzT->isChecked())
        if (ui->lineEditBINSVzT->text().toDouble() == 0)
            sensorsData.VzBINSPeriod = MaxTime+10;
        else
            sensorsData.VzBINSPeriod = ui->lineEditBINSVzT->text().toDouble();
    else
        sensorsData.VzBINSPeriod = MaxTime+10;

    // продольная скорость ГДЛ
    if (ui->checkBoxLagmx->isChecked())
        sensorsData.mxLag = ui->lineEditLagmx->text().toDouble();
    else
        sensorsData.mxLag = 0;
    if (ui->checkBoxLagVx0->isChecked())
        sensorsData.Vx0Lag = ui->lineEditLagVx0->text().toDouble();
    else
        sensorsData.Vx0Lag = 0;
    if (ui->checkBoxLagVxS->isChecked())
    {
        sensorsData.VxALag = ui->lineEditLagVxA->text().toDouble();
        sensorsData.VxWLag = ui->lineEditLagVxw->text().toDouble();
    }
    else
    {
        sensorsData.VxALag = 0;
        sensorsData.VxWLag = 0;
    }
    sensorsData.VxPLag = ui->checkBoxLagVxDelay->isChecked();
    if (ui->checkBoxLagVxT->isChecked())
        if (ui->lineEditLagVxT->text().toDouble() == 0)
            sensorsData.VxLagPeriod = MaxTime+10;
        else
            sensorsData.VxLagPeriod = ui->lineEditLagVxT->text().toDouble();
    else
        sensorsData.VxLagPeriod = MaxTime+10;

    // поперечная скорость ГДЛ
    if (ui->checkBoxLagmz->isChecked())
        sensorsData.mzLag = ui->lineEditLagmz->text().toDouble();
    else
        sensorsData.mzLag = 0;
    if (ui->checkBoxLagVz0->isChecked())
        sensorsData.Vz0Lag = ui->lineEditLagVz0->text().toDouble();
    else
        sensorsData.Vz0Lag = 0;
    if (ui->checkBoxLagVzS->isChecked())
    {
        sensorsData.VzALag = ui->lineEditLagVzA->text().toDouble();
        sensorsData.VzWLag = ui->lineEditLagVzw->text().toDouble();
    }
    else
    {
        sensorsData.VzALag = 0;
        sensorsData.VzWLag = 0;
    }
    sensorsData.VzPLag = ui->checkBoxLagVzDelay->isChecked();
    if (ui->checkBoxLagVzT->isChecked())
        if (ui->lineEditLagVzT->text().toDouble() == 0)
            sensorsData.VzLagPeriod = MaxTime+10;
        else
            sensorsData.VzLagPeriod = ui->lineEditLagVzT->text().toDouble();
    else
        sensorsData.VzLagPeriod = MaxTime+10;

    // X СНС
    if (ui->checkBoxSNSX0->isChecked())
        sensorsData.x0SNS = ui->lineEditSNSX0->text().toDouble();
    else
        sensorsData.x0SNS = 0;
    if (ui->checkBoxSNSXS->isChecked())
    {
        sensorsData.xASNS = ui->lineEditSNSXA->text().toDouble();
        sensorsData.xWSNS = ui->lineEditSNSXw->text().toDouble();
    }
    else
    {
        sensorsData.xASNS = 0;
        sensorsData.xWSNS = 0;
    }
    sensorsData.xPSNS = ui->checkBoxSNSxDelay->isChecked();
    if (ui->checkBoxSNSXT->isChecked())
        if (ui->lineEditSNSXT->text().toDouble() == 0)
            sensorsData.xSNSPeriod = MaxTime+10;
        else
            sensorsData.xSNSPeriod = ui->lineEditSNSXT->text().toDouble();
    else
        sensorsData.xSNSPeriod = MaxTime+10;

    // Z СНС
    if (ui->checkBoxSNSZ0->isChecked())
        sensorsData.z0SNS = ui->lineEditSNSZ0->text().toDouble();
    else
        sensorsData.z0SNS = 0;
    if (ui->checkBoxSNSZS->isChecked())
    {
        sensorsData.zASNS = ui->lineEditSNSZA->text().toDouble();
        sensorsData.zWSNS = ui->lineEditSNSZw->text().toDouble();
    }
    else
    {
        sensorsData.zASNS = 0;
        sensorsData.zWSNS = 0;
    }
    sensorsData.zPSNS = ui->checkBoxSNSzDelay->isChecked();
    if (ui->checkBoxSNSZT->isChecked())
        if (ui->lineEditSNSZT->text().toDouble() == 0)
            sensorsData.zSNSPeriod = MaxTime+10;
        else
            sensorsData.zSNSPeriod = ui->lineEditSNSZT->text().toDouble();
    else
        sensorsData.zSNSPeriod = MaxTime+10;

    // X ГАНС
    if (ui->checkBoxGANSX0->isChecked())
        sensorsData.x0GANS = ui->lineEditGANSX0->text().toDouble();
    else
        sensorsData.x0GANS = 0;
    if (ui->checkBoxGANSXS->isChecked())
    {
        sensorsData.xAGANS = ui->lineEditGANSxA->text().toDouble();
        sensorsData.xWGANS = ui->lineEditGANSxw->text().toDouble();
    }
    else
    {
        sensorsData.xAGANS = 0;
        sensorsData.xWGANS = 0;
    }
    sensorsData.xPGANS = ui->checkBoxGANSXDelay->isChecked();
    if (ui->checkBoxGANSXT->isChecked())
        if (ui->lineEditGANSXT->text().toDouble() == 0)
            sensorsData.xGANSPeriod = MaxTime+10;
        else
            sensorsData.xGANSPeriod = ui->lineEditGANSXT->text().toDouble();
    else
        sensorsData.xGANSPeriod = MaxTime+10;

    // Z ГАНС
    if (ui->checkBoxGANSZ0->isChecked())
        sensorsData.z0GANS = ui->lineEditGANSX0->text().toDouble();
    else
        sensorsData.z0GANS = 0;
    if (ui->checkBoxGANSZS->isChecked())
    {
        sensorsData.zAGANS = ui->lineEditGANSzA->text().toDouble();
        sensorsData.zWGANS = ui->lineEditGANSzw->text().toDouble();
    }
    else
    {
        sensorsData.zAGANS = 0;
        sensorsData.zWGANS = 0;
    }
    sensorsData.zPGANS = ui->checkBoxGANSZDelay->isChecked();
    if (ui->checkBoxGANSZT->isChecked())
        if (ui->lineEditGANSZT->text().toDouble() == 0)
            sensorsData.zGANSPeriod = MaxTime+10;
        else
            sensorsData.zGANSPeriod = ui->lineEditGANSZT->text().toDouble();
    else
        sensorsData.zGANSPeriod = MaxTime+10;

    // ССП
    if (ui->checkBoxgraphicSNSClear->isChecked())
        if (ui->lineEditSSPSNST->text().toDouble() == 0)
            sspData->periodSNS = MaxTime+10;
        else
            sspData->periodSNS = ui->lineEditSSPSNST->text().toDouble();
    else
        sspData->periodSNS = MaxTime+10;
    if (ui->checkBoxgraphicBINSGDLClear->isChecked())
        if (ui->lineEditSSPBINSLagT->text().toDouble() == 0)
            sspData->periodBINSLag = MaxTime+10;
        else
            sspData->periodBINSLag = ui->lineEditSSPBINSLagT->text().toDouble();
    else
        sspData->periodBINSLag = MaxTime+10;
    if (ui->checkBoxgraphicMagnGDLClear->isChecked())
        if (ui->lineEditSSPMagnLagT->text().toDouble() == 0)
            sspData->periodMagnLag = MaxTime+10;
        else
            sspData->periodMagnLag = ui->lineEditSSPMagnLagT->text().toDouble();
    else
        sspData->periodMagnLag = MaxTime+10;
    if (ui->checkBoxgraphicBINSClear->isChecked())
        if (ui->lineEditSSPBINST->text().toDouble() == 0)
            sspData->periodBINS = MaxTime+10;
        else
            sspData->periodBINS = ui->lineEditSSPBINST->text().toDouble();
    else
        sspData->periodBINS = MaxTime+10;
    if (ui->checkBoxgraphicGANSClear->isChecked())
        if (ui->lineEditSSPGANST->text().toDouble() == 0)
            sspData->periodGANS = MaxTime+10;
        else
            sspData->periodGANS = ui->lineEditSSPGANST->text().toDouble();
    else
        sspData->periodGANS = MaxTime+10;

    // Наблюдатели
    if (ui->checkBoxgraphicSNSBINS->isChecked())
        if (ui->lineEditSNSBINSTn->text().toDouble() == 0)
        {
            nablData->periodSNSBINS = MaxTime+10;
            nablData->timeSNSBINS = MaxTime+10;
            nablData->period2SNSBINS = MaxTime+10;
        }
        else
        {
            nablData->periodSNSBINS = ui->lineEditSNSBINSTn->text().toDouble();
            nablData->timeSNSBINS = ui->lineEditSNSBINStc->text().toDouble();
            nablData->period2SNSBINS = ui->lineEditSNSBINSTc->text().toDouble();
        }
    else
    {
        nablData->periodSNSBINS = MaxTime+10;
        nablData->timeSNSBINS = MaxTime+10;
        nablData->period2SNSBINS = MaxTime+10;
    }
    if (ui->checkBoxgraphicGANSBINSLag->isChecked())
        if (ui->lineEditGANSBINSLagTn->text().toDouble() == 0)
        {
            nablData->periodGANSBINSLag = MaxTime+10;
            nablData->timeGANSBINSLag = MaxTime+10;
            nablData->period2GANSBINSLag = MaxTime+10;
        }
        else
        {
            nablData->periodGANSBINSLag = ui->lineEditGANSBINSLagTn->text().toDouble();
            nablData->timeGANSBINSLag = ui->lineEditGANSBINSLagtc->text().toDouble();
            nablData->period2GANSBINSLag = ui->lineEditGANSBINSLagTc->text().toDouble();
        }
    else
    {
        nablData->periodGANSBINSLag = MaxTime+10;
        nablData->timeGANSBINSLag = MaxTime+10;
        nablData->period2GANSBINSLag = MaxTime+10;
    }
    if (ui->checkBoxgraphicGANSMagnLag->isChecked())
        if (ui->lineEditGANSMagnLagTn->text().toDouble() == 0)
        {
            nablData->periodGANSMagnLag = MaxTime+10;
            nablData->timeGANSMagnLag = MaxTime+10;
            nablData->period2GANSMagnLag = MaxTime+10;
        }
        else
        {
            nablData->periodGANSMagnLag = ui->lineEditGANSMagnLagTn->text().toDouble();
            nablData->timeGANSMagnLag = ui->lineEditGANSMagnLagtc->text().toDouble();
            nablData->period2GANSMagnLag = ui->lineEditGANSMagnLagTc->text().toDouble();
        }
    else
    {
        nablData->periodGANSMagnLag = MaxTime+10;
        nablData->timeGANSMagnLag = MaxTime+10;
        nablData->period2GANSMagnLag = MaxTime+10;
    }
    if (ui->checkBoxgraphicGANSBINS->isChecked())
        if (ui->lineEditGANSBINSTn->text().toDouble() == 0)
        {
            nablData->periodGANSBINS = MaxTime+10;
            nablData->timeGANSBINS = MaxTime+10;
            nablData->period2GANSBINS = MaxTime+10;
        }
        else
        {
            nablData->periodGANSBINS = ui->lineEditGANSBINSTn->text().toDouble();
            nablData->timeGANSBINS = ui->lineEditGANSBINStc->text().toDouble();
            nablData->period2GANSBINS = ui->lineEditGANSBINSTc->text().toDouble();
        }
    else
    {
        nablData->periodGANSBINS = MaxTime+10;
        nablData->timeGANSBINS = MaxTime+10;
        nablData->period2GANSBINS = MaxTime+10;
    }
}

void MainWidget::on_pushButtonBINSPsiGraphic_clicked(bool checked)
{
    if (checked)
        graphicPsiBINS->show();
    else
        graphicPsiBINS->hide();
}

void MainWidget::on_pushButtonBINSVxGraphic_clicked(bool checked)
{
    if (checked)
        graphicVxBINS->show();
    else
        graphicVxBINS->hide();
}

void MainWidget::on_pushButtonBINSVzGraphic_clicked(bool checked)
{
    if (checked)
        graphicVzBINS->show();
    else
        graphicVzBINS->hide();
}

void MainWidget::on_pushButtonMagnGraphic_clicked(bool checked)
{
    if (checked)
        graphicPsiMagn->show();
    else
        graphicPsiMagn->hide();
}

void MainWidget::on_pushButtonLagVxGraphic_clicked(bool checked)
{
    if (checked)
        graphicVxLag->show();
    else
        graphicVxLag->hide();
}

void MainWidget::on_pushButtonLagVzGraphic_clicked(bool checked)
{
    if (checked)
        graphicVzLag->show();
    else
        graphicVzLag->hide();
}

void MainWidget::on_pushButtonGANSXGraphic_clicked(bool checked)
{
    if (checked)
        graphicXGANS->show();
    else
        graphicXGANS->hide();
}

void MainWidget::on_pushButtonGANSZGraphic_clicked(bool checked)
{
    if (checked)
        graphicZGANS->show();
    else
        graphicZGANS->hide();
}

void MainWidget::on_pushButtonSNSXGraphic_clicked(bool checked)
{
    if (checked)
        graphicXSNS->show();
    else
        graphicXSNS->hide();
}

void MainWidget::on_pushButtonSNSZGraphic_clicked(bool checked)
{
    if (checked)
        graphicZSNS->show();
    else
        graphicZSNS->hide();
}

/*void MainWidget::on_pushButtonSSPClear_clicked(bool checked)
{
    if (checked)
        graphicSSP->show();
    else
        graphicSSP->hide();
}*/

void MainWidget::SetGraphicSSPVisible()
{
    if (ui->checkBoxgraphicSNSClear->isChecked())
    {
        graphicXZ->setDataVisible("SNS");
        graphicX->setDataVisible("SNS");
        graphicZ->setDataVisible("SNS");
        graphicdX->setDataVisible("SNS");
        graphicdZ->setDataVisible("SNS");
    }
    else
    {
        graphicXZ->setDataInvisible("SNS");
        graphicX->setDataInvisible("SNS");
        graphicZ->setDataInvisible("SNS");
        graphicdX->setDataInvisible("SNS");
        graphicdZ->setDataInvisible("SNS");
    }
    if (ui->checkBoxgraphicBINSGDLClear->isChecked())
    {
        graphicXZ->setDataVisible("BINS_Lag");
        graphicX->setDataVisible("BINS_Lag");
        graphicZ->setDataVisible("BINS_Lag");
        graphicdX->setDataVisible("BINS_Lag");
        graphicdZ->setDataVisible("BINS_Lag");
        graphicVx->setDataVisible("BINS_Lag");
        graphicVz->setDataVisible("BINS_Lag");
        graphicdVx->setDataVisible("BINS_Lag");
        graphicdVz->setDataVisible("BINS_Lag");
    }
    else
    {
        graphicXZ->setDataInvisible("BINS_Lag");
        graphicX->setDataInvisible("BINS_Lag");
        graphicZ->setDataInvisible("BINS_Lag");
        graphicdX->setDataInvisible("BINS_Lag");
        graphicdZ->setDataInvisible("BINS_Lag");
        graphicVx->setDataInvisible("BINS_Lag");
        graphicVz->setDataInvisible("BINS_Lag");
        graphicdVx->setDataInvisible("BINS_Lag");
        graphicdVz->setDataInvisible("BINS_Lag");
    }
    if (ui->checkBoxgraphicMagnGDLClear->isChecked())
    {
        graphicXZ->setDataVisible("Magn_Lag");
        graphicX->setDataVisible("Magn_Lag");
        graphicZ->setDataVisible("Magn_Lag");
        graphicdX->setDataVisible("Magn_Lag");
        graphicdZ->setDataVisible("Magn_Lag");
        graphicVx->setDataVisible("Magn_Lag");
        graphicVz->setDataVisible("Magn_Lag");
        graphicdVx->setDataVisible("Magn_Lag");
        graphicdVz->setDataVisible("Magn_Lag");
    }
    else
    {
        graphicXZ->setDataInvisible("Magn_Lag");
        graphicX->setDataInvisible("Magn_Lag");
        graphicZ->setDataInvisible("Magn_Lag");
        graphicdX->setDataInvisible("Magn_Lag");
        graphicdZ->setDataInvisible("Magn_Lag");
        graphicVx->setDataInvisible("Magn_Lag");
        graphicVz->setDataInvisible("Magn_Lag");
        graphicdVx->setDataInvisible("Magn_Lag");
        graphicdVz->setDataInvisible("Magn_Lag");
    }
    if (ui->checkBoxgraphicBINSClear->isChecked())
    {
        graphicXZ->setDataVisible("BINS");
        graphicX->setDataVisible("BINS");
        graphicZ->setDataVisible("BINS");
        graphicdX->setDataVisible("BINS");
        graphicdZ->setDataVisible("BINS");
        graphicVx->setDataVisible("BINS");
        graphicVz->setDataVisible("BINS");
        graphicdVx->setDataVisible("BINS");
        graphicdVz->setDataVisible("BINS");
    }
    else
    {
        graphicXZ->setDataInvisible("BINS");
        graphicX->setDataInvisible("BINS");
        graphicZ->setDataInvisible("BINS");
        graphicdX->setDataInvisible("BINS");
        graphicdZ->setDataInvisible("BINS");
        graphicVx->setDataInvisible("BINS");
        graphicVz->setDataInvisible("BINS");
        graphicdVx->setDataInvisible("BINS");
        graphicdVz->setDataInvisible("BINS");
    }
    if (ui->checkBoxgraphicGANSClear->isChecked())
    {
        graphicXZ->setDataVisible("GANS");
        graphicX->setDataVisible("GANS");
        graphicZ->setDataVisible("GANS");
        graphicdX->setDataVisible("GANS");
        graphicdZ->setDataVisible("GANS");
        graphicVx->setDataVisible("GANS");
        graphicVz->setDataVisible("GANS");
        graphicdVx->setDataVisible("GANS");
        graphicdVz->setDataVisible("GANS");
    }
    else
    {
        graphicXZ->setDataInvisible("GANS");
        graphicX->setDataInvisible("GANS");
        graphicZ->setDataInvisible("GANS");
        graphicdX->setDataInvisible("GANS");
        graphicdZ->setDataInvisible("GANS");
        graphicVx->setDataInvisible("GANS");
        graphicVz->setDataInvisible("GANS");
        graphicdVx->setDataInvisible("GANS");
        graphicdVz->setDataInvisible("GANS");
    }
    if (ui->checkBoxgraphicSNSBINS->isChecked())
    {
        graphicXZ->setDataVisible("SNS_BINS");
        graphicX->setDataVisible("SNS_BINS");
        graphicZ->setDataVisible("SNS_BINS");
        graphicdX->setDataVisible("SNS_BINS");
        graphicdZ->setDataVisible("SNS_BINS");
        graphicVx->setDataVisible("SNS_BINS");
        graphicVz->setDataVisible("SNS_BINS");
        graphicdVx->setDataVisible("SNS_BINS");
        graphicdVz->setDataVisible("SNS_BINS");
    }
    else
    {
        graphicXZ->setDataInvisible("SNS_BINS");
        graphicX->setDataInvisible("SNS_BINS");
        graphicZ->setDataInvisible("SNS_BINS");
        graphicdX->setDataInvisible("SNS_BINS");
        graphicdZ->setDataInvisible("SNS_BINS");
        graphicVx->setDataInvisible("SNS_BINS");
        graphicVz->setDataInvisible("SNS_BINS");
        graphicdVx->setDataInvisible("SNS_BINS");
        graphicdVz->setDataInvisible("SNS_BINS");
    }
    if (ui->checkBoxgraphicGANSBINSLag->isChecked())
    {
        graphicXZ->setDataVisible("GANS_BINS_Lag");
        graphicX->setDataVisible("GANS_BINS_Lag");
        graphicZ->setDataVisible("GANS_BINS_Lag");
        graphicdX->setDataVisible("GANS_BINS_Lag");
        graphicdZ->setDataVisible("GANS_BINS_Lag");
        graphicVx->setDataVisible("GANS_BINS_Lag");
        graphicVz->setDataVisible("GANS_BINS_Lag");
        graphicdVx->setDataVisible("GANS_BINS_Lag");
        graphicdVz->setDataVisible("GANS_BINS_Lag");
    }
    else
    {
        graphicXZ->setDataInvisible("GANS_BINS_Lag");
        graphicX->setDataInvisible("GANS_BINS_Lag");
        graphicZ->setDataInvisible("GANS_BINS_Lag");
        graphicdX->setDataInvisible("GANS_BINS_Lag");
        graphicdZ->setDataInvisible("GANS_BINS_Lag");
        graphicVx->setDataInvisible("GANS_BINS_Lag");
        graphicVz->setDataInvisible("GANS_BINS_Lag");
        graphicdVx->setDataInvisible("GANS_BINS_Lag");
        graphicdVz->setDataInvisible("GANS_BINS_Lag");
    }
    if (ui->checkBoxgraphicGANSMagnLag->isChecked())
    {
        graphicXZ->setDataVisible("GANS_Magn_Lag");
        graphicX->setDataVisible("GANS_Magn_Lag");
        graphicZ->setDataVisible("GANS_Magn_Lag");
        graphicdX->setDataVisible("GANS_Magn_Lag");
        graphicdZ->setDataVisible("GANS_Magn_Lag");
        graphicVx->setDataVisible("GANS_Magn_Lag");
        graphicVz->setDataVisible("GANS_Magn_Lag");
        graphicdVx->setDataVisible("GANS_Magn_Lag");
        graphicdVz->setDataVisible("GANS_Magn_Lag");
    }
    else
    {
        graphicXZ->setDataInvisible("GANS_Magn_Lag");
        graphicX->setDataInvisible("GANS_Magn_Lag");
        graphicZ->setDataInvisible("GANS_Magn_Lag");
        graphicdX->setDataInvisible("GANS_Magn_Lag");
        graphicdZ->setDataInvisible("GANS_Magn_Lag");
        graphicVx->setDataInvisible("GANS_Magn_Lag");
        graphicVz->setDataInvisible("GANS_Magn_Lag");
        graphicdVx->setDataInvisible("GANS_Magn_Lag");
        graphicdVz->setDataInvisible("GANS_Magn_Lag");
    }
    if (ui->checkBoxgraphicGANSBINS->isChecked())
    {
        graphicXZ->setDataVisible("GANS_BINS");
        graphicX->setDataVisible("GANS_BINS");
        graphicZ->setDataVisible("GANS_BINS");
        graphicdX->setDataVisible("GANS_BINS");
        graphicdZ->setDataVisible("GANS_BINS");
        graphicVx->setDataVisible("GANS_BINS");
        graphicVz->setDataVisible("GANS_BINS");
        graphicdVx->setDataVisible("GANS_BINS");
        graphicdVz->setDataVisible("GANS_BINS");
    }
    else
    {
        graphicXZ->setDataInvisible("GANS_BINS");
        graphicX->setDataInvisible("GANS_BINS");
        graphicZ->setDataInvisible("GANS_BINS");
        graphicdX->setDataInvisible("GANS_BINS");
        graphicdZ->setDataInvisible("GANS_BINS");
        graphicVx->setDataInvisible("GANS_BINS");
        graphicVz->setDataInvisible("GANS_BINS");
        graphicdVx->setDataInvisible("GANS_BINS");
        graphicdVz->setDataInvisible("GANS_BINS");
    }
}

void MainWidget::ClearSensorsData()
{
    // очистка структур датчиков
    sensorsData.psiBINS = 0;
    sensorsData.DwyBINS_t = 0;

    sensorsData.psiMagn = 0;

    sensorsData.VxBINS = 0;

    sensorsData.VzBINS = 0;

    sensorsData.VxLag = 0;

    sensorsData.VzLag = 0;

    sensorsData.xSNS = 0;

    sensorsData.zSNS = 0;

    sensorsData.xGANS = 0;

    sensorsData.zGANS = 0;
}

void MainWidget::on_pushButtonloadTraek_clicked()
{
    // нажата кнопка загрузить траекторию
    ui->widgetGraphic->clearGraphic();
    pointsVec.clear();
    PointStruct p;
    //p.r = ui->lineEditR->text().toDouble();

    p.x = 0;
    p.y = 0;
    pointsVec.append(p);
    ui->widgetGraphic->addPoint("заданная",p.y,p.x);

    p.x = 100;
    p.y = 0;
    pointsVec.append(p);
    ui->widgetGraphic->addPoint("заданная",p.y,p.x);

    p.x = 100;
    p.y = 20;
    pointsVec.append(p);
    ui->widgetGraphic->addPoint("заданная",p.y,p.x);

    p.x = 0;
    p.y = 20;
    pointsVec.append(p);
    ui->widgetGraphic->addPoint("заданная",p.y,p.x);

    p.x = 0;
    p.y = 40;
    pointsVec.append(p);
    ui->widgetGraphic->addPoint("заданная",p.y,p.x);

    p.x = 100;
    p.y = 40;
    pointsVec.append(p);
    ui->widgetGraphic->addPoint("заданная",p.y,p.x);

    p.x = 100;
    p.y = 60;
    pointsVec.append(p);
    ui->widgetGraphic->addPoint("заданная",p.y,p.x);

    p.x = 0;
    p.y = 60;
    pointsVec.append(p);
    ui->widgetGraphic->addPoint("заданная",p.y,p.x);
}

void MainWidget::on_pushButtongraphicSSP_clicked(bool checked)
{
    // нажата кнопка графиков ССП
    if (checked)
        graphicXZ->show();
    else
        graphicXZ->hide();
}

void MainWidget::on_pushButtongraphicSSPX_clicked(bool checked)
{
    // нажата кнопка графика X ССП
    if (checked)
        graphicX->show();
    else
        graphicX->hide();
}

void MainWidget::on_pushButtongraphicSSPZ_clicked(bool checked)
{
    // нажата кнопка графика Z ССП
    if (checked)
        graphicZ->show();
    else
        graphicZ->hide();
}

void MainWidget::on_pushButtongraphicSSPdX_clicked(bool checked)
{
    // нажата кнопка графика dX ССП
    if (checked)
        graphicdX->show();
    else
        graphicdX->hide();
}

void MainWidget::on_pushButtongraphicSSPdZ_clicked(bool checked)
{
    // нажата кнопка графика dZ ССП
    if (checked)
        graphicdZ->show();
    else
        graphicdZ->hide();
}

void MainWidget::on_pushButtongraphicSSPVx_clicked(bool checked)
{
    // нажата кнопка графика Vx ССП
    if (checked)
        graphicVx->show();
    else
        graphicVx->hide();
}

void MainWidget::on_pushButtongraphicSSPVz_clicked(bool checked)
{
    // нажата кнопка графика Vz ССП
    if (checked)
        graphicVz->show();
    else
        graphicVz->hide();
}

void MainWidget::on_pushButtongraphicSSPdVx_clicked(bool checked)
{
    // нажата кнопка графика dVx ССП
    if (checked)
        graphicdVx->show();
    else
        graphicdVx->hide();
}

void MainWidget::on_pushButtongraphicSSPdVz_clicked(bool checked)
{
    // нажата кнопка графика Vz ССП
    if (checked)
        graphicdVz->show();
    else
        graphicdVz->hide();
}

void MainWidget::on_pushButtonFileFind_clicked()
{
    // нажата кнопка выбрать файл
    fileName = QFileDialog::getOpenFileName(this,
          tr("Выберите файл с данными"), "/home", tr("Text Files (*.csv)"));
    ui->lineEditFile->setText(fileName);
}

void MainWidget::on_pushButtonFileSelect_clicked()
{
    // нажата кнопка подтверждения выбора файла
    QFile file(fileName);
    file.open(QIODevice::ReadOnly);
    QString line = file.readLine();
    QList<QString> list = line.split(";");
    ui->comboBoxPx->addItems(list);
    ui->comboBoxPz->addItems(list);
    ui->comboBoxvpx->addItems(list);
    ui->comboBoxvpz->addItems(list);
    file.close();
}

void MainWidget::on_pushButtonFileNablSelect_clicked()
{
    // нажата кнопка выбора полей данных
    QFile file(fileName);
    file.open(QIODevice::ReadOnly);
    QString line = file.readLine();
    QString strPx = ui->comboBoxPx->currentText();
    QString strPz = ui->comboBoxPz->currentText();
    QString strVx = ui->comboBoxvpx->currentText();
    QString strVz = ui->comboBoxvpz->currentText();
    QVector<QStringRef> vec = line.splitRef(";");
    for (int i=0; i<vec.size(); i++)
    {
        if (vec[i] == strPx)
            numPx = i;
        if (vec[i] == strVx)
            numVpx = i;
        if (vec[i] == strPz)
            numPz = i;
        if (vec[i] == strVz)
            numVpz = i;
    }
    //qDebug()<<"Px "<<numPx<<"Vpx "<<numVpx<<" Pz "<<numPz<<" Vpz "<<numVpz;
    file.close();
}

void MainWidget::on_pushButtonFileCalc_clicked()
{
    // нажата кнопка запуска расчетов
    QFile file(fileName);
    file.open(QIODevice::ReadOnly);
    QString line = file.readLine();
    QVector<QStringRef> vec;
    //if (numPz != 0 && numVpz != 0)
    //    ui->widgetResult->addData("sourceVp", true);
    //ui->widgetResult->addData("result", true);
    //ui->widgetResult->addData("sourseP",true);
    double dT = 0;
    nablXResult = new Nabludatel8(this);
    nablZResult = new Nabludatel8(this);
    graphicdVp = new GraphicWindow("dVp", "Время, с","",0);
    graphicdVp->SetFileName("dVp");
    graphicdVp->addData("dVp0",true);
    graphicdVp->addData("dVp1",true);
    graphicdVp->addData("dVp2",true);
    graphicdVp->addData("dVp3",true);
    graphicdVp->addData("dVp4",true);
    graphicdVp->addData("dVp5",true);
    graphicdVp->addData("dVp6",true);
    graphicdVp->addData("dVp7",true);
    graphicdVp->show();
    while(!file.atEnd())
    {
        line = file.readLine();
        vec = line.splitRef(";");
        double Px = vec[numPx].toDouble();
        double Vpx = vec[numVpx].toDouble();
        double step = 1;
        nablXResult->Calc(step,Px,Vpx);
        double Pz = 0;
        double Vpz = 0;
        if (numPz != -1 && numVpz != -1)
        {
            Pz = vec[numPz].toDouble();
            Vpz = vec[numVpz].toDouble();
            nablZResult->Calc(step,Pz,Vpz);
            ui->widgetResult->addPoint("result",nablZResult->GetP(),nablXResult->GetP());
            ui->widgetResult->addPoint("sourseP",Pz,Px);
        }
        else
        {
            ui->widgetResult->addPoint("result",dT,nablXResult->GetP());
            ui->widgetResult->addPoint("sourseP",dT,Px);
        }
        graphicdVp->addPoint("dVp0",dT,nablXResult->GetVp0());
        graphicdVp->addPoint("dVp1",dT,nablXResult->GetVp1());
        graphicdVp->addPoint("dVp2",dT,nablXResult->GetVp2());
        graphicdVp->addPoint("dVp3",dT,nablXResult->GetVp3());
        graphicdVp->addPoint("dVp4",dT,nablXResult->GetVp4());
        graphicdVp->addPoint("dVp5",dT,nablXResult->GetVp5());
        graphicdVp->addPoint("dVp6",dT,nablXResult->GetVp6());
        graphicdVp->addPoint("dVp7",dT,nablXResult->GetVp7());
        dT+=step;
        QApplication::processEvents();
    }
}
