#ifndef NABLUDATELC0_H
#define NABLUDATELC0_H

#include <QObject>
#include <QDebug>

#include "Nabludatel/nabludatel3.h"

class NabludatelC0 : public QObject
{
    Q_OBJECT
public:
    explicit NabludatelC0(QObject *parent = nullptr);

private:

    double P_now = 0; // оценка текущая P_now = Naqbludatel
    double dP_now = 0; // производная dP_now = A2-(Vp0-Vp1)
    double Vp = 0; // скорость сейчас с учетом всех поправок для интегрирования Vp = Nabludatel
    double Vp0 = 0; // текущая оценка постоянной ошибки скорости, является текущей оценкой всей ошибки при работающем наблюдателе Vp0 = A2-Vp
    double dVp1 = 0; // коэффициент линейно растущей со временем ошибки скорости dVp1 = Nabludatel
    double Vp1 = 0; // результат интегрирования линейно растущей со врменем ошибки скорости Vp1 = Integral(dVp1)

    double A1 = 0; // измерение величины
    double A2 = 0; // изммерение скорости

    bool isCorrect = false; // так как строить логику долго, переключение между работой по наблюдателю и коррекцией производится флагом
                            // само переключение лучше производить сразу по смене периода получения данных, чтобы минимизировать ошибку

    Nabludatel3* nabl; // наблюдатель для оценок при высокой частоте

public slots:

    void Calc(double step, double a1_new, double a2_new);
    void SetCorrect();
    void SetK(double k1_new, double k2_new, double k3_new);
    void Runge(double step);
    void Func();
    void Correct(double a1_new);

    double GetP();
    double GetVp();
    double GetdVp0();
    double GetdVp1();
    double GetdVp2();
};

#endif // NABLUDATELC0_H
