#include "nabludatel8.h"

Nabludatel8::Nabludatel8(QObject *parent) : QObject(parent)
{
    // тут ничего
}

void Nabludatel8::SetK(double k1_new, double k2_new,double k3_new,double k4_new,double k5_new,double k6_new,double k7_new,double k8_new)
{
    K1 = k1_new;
    K2 = k2_new;
    K3 = k3_new;
    K4 = k4_new;
    K5 = k5_new;
    K6 = k6_new;
    K7 = k7_new;
    K8 = k8_new;
}

void Nabludatel8::Calc(double step, double a1_new, double a2_new)
{
    A1 =a1_new;
    A2 = a2_new;
    Runge(step);
}

void Nabludatel8::Runge(double step)
{
    // создаем переменные для метода Рунге-Кутта 4-го порядка
    double tempP = 0;
    double tempVp1 = 0;
    double tempVp2 = 0;
    double tempVp3 = 0;
    double tempVp4 = 0;
    double tempVp5 = 0;
    double tempVp6 = 0;
    double tempVp7 = 0;
    double tempIntVp2 = 0;

    double tempdP = 0;
    double tempdVp1 = 0;
    double tempdVp2 = 0;
    double tempdVp3 = 0;
    double tempdVp4 = 0;
    double tempdVp5 = 0;
    double tempdVp6 = 0;
    double tempdVp7 = 0;

    // по лекциям
    Func();

    tempP = P;
    tempVp1 = Vp1;
    tempVp2 = Vp2;
    tempVp3 = Vp3;
    tempVp4 = Vp4;
    tempVp5 = Vp5;
    tempVp6 = Vp6;
    tempVp7 = Vp7;
    tempIntVp2 = IntVp2;

    tempdP = dP;
    tempdVp1 = dVp1;
    tempdVp2 = dVp2;
    tempdVp3 = dVp3;
    tempdVp4 = dVp4;
    tempdVp5 = dVp5;
    tempdVp6 = dVp6;
    tempdVp7 = dVp7;

    P = tempP + 0.5*step*dP;
    Vp1 = tempVp1 + 0.5*step*dVp1;
    Vp2 = tempVp2 + 0.5*step*dVp2;
    Vp3 = tempVp2 + 0.5*step*dVp3;
    Vp4 = tempVp2 + 0.5*step*dVp4;
    Vp5 = tempVp2 + 0.5*step*dVp5;
    Vp6 = tempVp2 + 0.5*step*dVp6;
    Vp7 = tempVp2 + 0.5*step*dVp7;
    IntVp2 = tempIntVp2 + 0.5*step*Vp2;

    // второй подход
    Func();

    tempdP = tempdP + 2.0*dP;
    tempdVp1 = tempdVp1 + 2.0*dVp1;
    tempdVp2 = tempdVp2 + 2.0*dVp2;
    tempdVp3 = tempdVp3 + 2.0*dVp3;
    tempdVp4 = tempdVp4 + 2.0*dVp4;
    tempdVp5 = tempdVp5 + 2.0*dVp5;
    tempdVp6 = tempdVp6 + 2.0*dVp6;
    tempdVp7 = tempdVp7 + 2.0*dVp7;

    P = tempP + 0.5*step*dP;
    Vp1 = tempVp1 + 0.5*step*dVp1;
    Vp2 = tempVp2 + 0.5*step*dVp2;
    Vp3 = tempVp3 + 0.5*step*dVp3;
    Vp4 = tempVp4 + 0.5*step*dVp4;
    Vp5 = tempVp5 + 0.5*step*dVp5;
    Vp6 = tempVp6 + 0.5*step*dVp6;
    Vp7 = tempVp7 + 0.5*step*dVp7;
    IntVp2 = tempIntVp2 + 0.5*step*Vp2;

    // третий подход
    Func();

    tempdP = tempdP + 2.0*dP;
    tempdVp1 = tempdVp1 + 2.0*dVp1;
    tempdVp2 = tempdVp2 + 2.0*dVp2;
    tempdVp3 = tempdVp3 + 2.0*dVp3;
    tempdVp4 = tempdVp4 + 2.0*dVp4;
    tempdVp5 = tempdVp5 + 2.0*dVp5;
    tempdVp6 = tempdVp6 + 2.0*dVp6;
    tempdVp7 = tempdVp7 + 2.0*dVp7;

    P = tempP + step*dP;
    Vp1 = tempVp1 + step*dVp1;
    Vp2 = tempVp2 + step*dVp2;
    Vp3 = tempVp3 + step*dVp3;
    Vp4 = tempVp4 + step*dVp4;
    Vp5 = tempVp5 + step*dVp5;
    Vp6 = tempVp6 + step*dVp6;
    Vp7 = tempVp7 + step*dVp7;
    IntVp2 = tempIntVp2 + step*Vp2;

    // четвертый подход
    Func();

    P = tempP + (step/6.0)*(tempdP + dP);
    Vp1 = tempVp1 + (step/6.0)*(tempdVp1 + dVp1);
    Vp2 = tempVp2 + (step/6.0)*(tempdVp2 + dVp2);
    Vp3 = tempVp3 + (step/6.0)*(tempdVp3 + dVp3);
    Vp4 = tempVp4 + (step/6.0)*(tempdVp4 + dVp4);
    Vp5 = tempVp5 + (step/6.0)*(tempdVp5 + dVp5);
    Vp6 = tempVp6 + (step/6.0)*(tempdVp6 + dVp6);
    Vp7 = tempVp7 + (step/6.0)*(tempdVp7 + dVp7);
    IntVp2 = tempIntVp2 + (step/6.0)*(tempVp2+Vp2);
}

void Nabludatel8::Func()
{
    dP = A2+Vp1+K1*(A1-P);
    dVp1 = Vp2+K2*(A1-P);
    dVp2 = Vp3+K3*(A1-P);
    dVp3 = Vp4+K4*(A1-P);
    dVp4 = Vp5+K5*(A1-P);
    dVp5 = Vp6+K6*(A1-P);
    dVp6 = Vp7+K7*(A1-P);
    dVp3 = K4*(A1-P);
}

double Nabludatel8::GetP()
{
    return P;
}

double Nabludatel8::GetVp()
{
    return A2+Vp1;
}

double Nabludatel8::GetVp0()
{
    return Vp0;
}

double Nabludatel8::GetVp1()
{
    return Vp1;
}

double Nabludatel8::GetVp2()
{
    return Vp2;
}

double Nabludatel8::GetVp3()
{
    return Vp3;
}

double Nabludatel8::GetVp4()
{
    return Vp4;
}

double Nabludatel8::GetVp5()
{
    return Vp5;
}

double Nabludatel8::GetVp6()
{
    return Vp6;
}

double Nabludatel8::GetVp7()
{
    return Vp7;
}
