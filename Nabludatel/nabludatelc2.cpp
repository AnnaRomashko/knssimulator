#include "nabludatelc2.h"

NabludatelC2::NabludatelC2(QObject *parent) : QObject(parent)
{
    // создаем наблюдатель
    nabl = new Nabludatel2(this);

    // создаем фильтр
    filt = new FNHfilter(this);
}

void NabludatelC2::SetK(double k1_new, double k2_new)
{
    nabl->SetK(k1_new,k2_new);
}

void NabludatelC2::Calc(double step, double a1_new, double a2_new)
{
    if (!isCorrect) // работа по наблюдателю
    {
        // просто запускаем наблюдатель
        A2=a2_new;
        A1=a1_new;
        nabl->Calc(step, A1,A2);
        P_now = nabl->GetP();
        Vp = nabl->GetVp();
        Vp0 = -nabl->GetVp0(); // текущая оценка постоянной ошибки
        dVp0 = -nabl->GetdVp0(); // текущая оценка производной постоянной ошибки
        dP_now = Vp;
        Vp0f_before[0] = Vp0;
        dVp0f_before[0] = dVp0;
        filt->filter(TauVp0,step,Vp0f_before,Vp0f_after,0);
        filt->filter(TaudVp0,step,dVp0f_before,dVp0f_after,0);
        //qDebug()<<"P_now "<<P_now<<"Vp "<<Vp<<" Vp0 "<<Vp0<<"A1 "<<A1<<"A2 "<<A2;
    }
    else // режим коррекции
    {
        // чтобы не менять фактический приход данных с медленного датчика задержка реализуется в коррекции
        A2 = a2_new;
        Vp = A2-Vp0f_after[0];//-Vp01;
        Runge(step);
        //qDebug()<<"Vp0 "<<Vp0<<"dVp1 "<<dVp1<<" Vp1 "<<Vp1<<"A1 "<<A1<<"A2 "<<A2;
        //qDebug()<<"P_now "<<P_now<<"A1 "<<A1<<"A2 "<<A2;
    }
}

void NabludatelC2::Correct(double a1_new)
{
    A1 = a1_new;
    P_now = A1;
    /*Vp0 = A2-nabl->GetVp(); // ошибка текущая
    dVp1 = -nabl->GetVp1(); // коэффициент растущей со врменем ошибки*/
    //Vp0 = -nabl->GetVp0(); // ошибка текущая
    //Vp1 = 0;
    //dVp1 = -nabl->GetVp1(); // коэффициент растущей со врменем ошибки
}

void NabludatelC2::Func()
{
    dP_now = Vp; //(Vp0+Vp1);
    //Vp = A2-Vp1; //(Vp0+Vp1);
}

void NabludatelC2::SetCorrect()
{
    isCorrect = true;
}

double NabludatelC2::GetP()
{
    return P_now;
}

double NabludatelC2::GetVp()
{
    return Vp;
}

double NabludatelC2::GetVp0()
{
    return Vp0;
}

double NabludatelC2::GetdVp0()
{
    return dVp0;
}

double NabludatelC2::GetVp0f()
{
    return Vp0f_after[0];
}

double NabludatelC2::GetdVp0f()
{
    return dVp0f_after[0];
}

void NabludatelC2::Runge(double step)
{
    // создаем переменные для метода Рунге-Кутта 4-го порядка
    double tempP_now = 0;
    double tempVp01 = 0;

    double tempdP_now = 0;
    double tempdVp0 = 0;

    // по лекциям
    Func();

    tempP_now = P_now;
    tempVp01 = Vp01;

    tempdP_now = dP_now;
    tempdVp0 = dVp0f_after[0];

    P_now = tempP_now + 0.5*step*dP_now;
    Vp01 = tempVp01 + 0.5*step*dVp0f_after[0];

    // второй подход
    Func();

    tempdP_now = tempdP_now + 2.0*dP_now;
    tempdVp0 = tempdVp0 + 2.0*dVp0f_after[0];

    P_now = tempP_now + 0.5*step*dP_now;
    Vp01 = tempVp01 + 0.5*step*dVp0f_after[0];

    // третий подход
    Func();

    tempdP_now = tempdP_now + 2.0*dP_now;
    tempdVp0 = tempdVp0 + 2.0*dVp0f_after[0];

    P_now = tempP_now + step*dP_now;
    Vp01 = tempVp01 + step*dVp0f_after[0];

    // четвертый подход
    Func();

    P_now = tempP_now + (step/6.0)*(tempdP_now + dP_now);
    Vp01 = tempVp01 + (step/6.0)*(tempdVp0 + dVp0f_after[0]);
}
