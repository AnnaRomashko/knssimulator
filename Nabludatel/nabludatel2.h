#ifndef NABLUDATEL2_H
#define NABLUDATEL2_H

#include <QObject>

class Nabludatel2 : public QObject
{
    Q_OBJECT
public:
    explicit Nabludatel2(QObject *parent = nullptr);

private:

    double P = 0;
    double dP = 0;
    double Vp0 = 0;
    double dVp0 = 0;
    double K1 = 1.5;
    double K2 = 2;

    double A1 = 0;
    double A2 = 0;

public slots:

    void SetK(double k1_new, double k2_new);
    void Calc(double step, double a1_new, double a2_new);
    void Func();
    void Runge(double step);

    double GetP();
    double GetVp();
    double GetVp0();
    double GetdVp0();
};

#endif // NABLUDATEL2_H
