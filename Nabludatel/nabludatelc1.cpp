#include "nabludatelc1.h"

NabludatelC1::NabludatelC1(QObject *parent) : QObject(parent)
{
    // создаем наблюдатель
    nabl = new Nabludatel3(this);
}

void NabludatelC1::SetK(double k1_new, double k2_new, double k3_new)
{
    nabl->SetK(k1_new,k2_new,k3_new);
}

void NabludatelC1::Calc(double step, double a1_new, double a2_new)
{
    if (!isCorrect) // работа по наблюдателю
    {
        // просто запускаем наблюдатель
        A2=a2_new;
        A1=a1_new;
        nabl->Calc(step, A1,A2);
        P_now = nabl->GetP();
        /*Vp = nabl->GetVp();
        Vp0 = A2-Vp; // ошибка текущая
        dVp1 = -nabl->GetVp1(); // коэффициент растущей со врменем ошибки
        dP_now = Vp;
        Vp1 = A2-Vp0;*/
        Vp = nabl->GetVp();
        Vp0 = -nabl->GetVp0(); // ошибка текущая
        dVp1 = -nabl->GetVp1(); // коэффициент растущей со врменем ошибки
        dP_now = Vp;
        Vp1 = -nabl->GetVp0();
        qDebug()<<"Vp0 "<<Vp0<<"dVp1 "<<dVp1<<" Vp1 "<<Vp1<<"A1 "<<A1<<"A2 "<<A2;
    }
    else // режим коррекции
    {
        // чтобы не менять фактический приход данных с медленного датчика задержка реализуется в коррекции
        A2 = a2_new;
        nabl->Calc(step,A1,A2);
        Runge(step);
        qDebug()<<"Vp0 "<<Vp0<<"dVp1 "<<dVp1<<" Vp1 "<<Vp1<<"A1 "<<A1<<"A2 "<<A2;
    }
}

void NabludatelC1::Correct(double a1_new)
{
    A1 = a1_new;
    P_now = A1;
    /*Vp0 = A2-nabl->GetVp(); // ошибка текущая
    dVp1 = -nabl->GetVp1(); // коэффициент растущей со врменем ошибки*/
    Vp0 = -nabl->GetVp0(); // ошибка текущая
    Vp1 = 0;
    dVp1 = -nabl->GetVp1(); // коэффициент растущей со врменем ошибки
}

void NabludatelC1::Func()
{
    dP_now = A2-Vp1; //(Vp0+Vp1);
    Vp = A2-Vp1; //(Vp0+Vp1);
}

void NabludatelC1::SetCorrect()
{
    isCorrect = true;
}

double NabludatelC1::GetP()
{
    return P_now;
}

double NabludatelC1::GetVp()
{
    return Vp;
}

void NabludatelC1::Runge(double step)
{
    // создаем переменные для метода Рунге-Кутта 4-го порядка
    double tempP_now = 0;
    double tempVp1 = 0;

    double tempdP_now = 0;
    double tempdVp1 = 0;

    // по лекциям
    Func();

    tempP_now = P_now;
    tempVp1 = Vp1;

    tempdP_now = dP_now;
    tempdVp1 = dVp1;

    P_now = tempP_now + 0.5*step*dP_now;
    Vp1 = tempVp1 + 0.5*step*dVp1;

    // второй подход
    Func();

    tempdP_now = tempdP_now + 2.0*dP_now;
    tempdVp1 = tempdVp1 + 2.0*dVp1;

    P_now = tempP_now + 0.5*step*dP_now;
    Vp1 = tempVp1 + 0.5*step*dVp1;

    // третий подход
    Func();

    tempdP_now = tempdP_now + 2.0*dP_now;
    tempdVp1 = tempdVp1 + 2.0*dVp1;

    P_now = tempP_now + step*dP_now;
    Vp1 = tempVp1 + step*dVp1;

    // четвертый подход
    Func();

    P_now = tempP_now + (step/6.0)*(tempdP_now + dP_now);
    Vp1 = tempVp1 + (step/6.0)*(tempdVp1 + dVp1);
}
