#include "nabludatel3.h"

Nabludatel3::Nabludatel3(QObject *parent) : QObject(parent)
{
    // тут ничего
}

void Nabludatel3::SetK(double k1_new, double k2_new,double k3_new)
{
    K1 = k1_new;
    K2 = k2_new;
    K3 = k3_new;
}

void Nabludatel3::Calc(double step, double a1_new, double a2_new)
{
    A1 =a1_new;
    A2 = a2_new;
    Runge(step);
}

void Nabludatel3::Runge(double step)
{
    // создаем переменные для метода Рунге-Кутта 4-го порядка
    double tempP = 0;
    double tempVp1 = 0;
    double tempVp2 = 0;
    double tempIntVp2 = 0;

    double tempdP = 0;
    double tempdVp1 = 0;
    double tempdVp2 = 0;

    // по лекциям
    Func();

    tempP = P;
    tempVp1 = Vp1;
    tempVp2 = Vp2;
    tempIntVp2 = IntVp2;

    tempdP = dP;
    tempdVp1 = dVp1;
    tempdVp2 = dVp2;

    P = tempP + 0.5*step*dP;
    Vp1 = tempVp1 + 0.5*step*dVp1;
    Vp2 = tempVp2 + 0.5*step*dVp2;
    IntVp2 = tempIntVp2 + 0.5*step*Vp2;

    // второй подход
    Func();

    tempdP = tempdP + 2.0*dP;
    tempdVp1 = tempdVp1 + 2.0*dVp1;
    tempdVp2 = tempdVp2 + 2.0*dVp2;

    P = tempP + 0.5*step*dP;
    Vp1 = tempVp1 + 0.5*step*dVp1;
    Vp2 = tempVp2 + 0.5*step*dVp2;
    IntVp2 = tempIntVp2 + 0.5*step*Vp2;

    // третий подход
    Func();

    tempdP = tempdP + 2.0*dP;
    tempdVp1 = tempdVp1 + 2.0*dVp1;
    tempdVp2 = tempdVp2 + 2.0*dVp2;

    P = tempP + step*dP;
    Vp1 = tempVp1 + step*dVp1;
    Vp2 = tempVp2 + step*dVp2;
    IntVp2 = tempIntVp2 + step*Vp2;

    // четвертый подход
    Func();

    P = tempP + (step/6.0)*(tempdP + dP);
    Vp1 = tempVp1 + (step/6.0)*(tempdVp1 + dVp1);
    Vp2 = tempVp2 + (step/6.0)*(tempdVp2 + dVp2);
    IntVp2 = tempIntVp2 + (step/6.0)*(tempVp2+Vp2);
}

void Nabludatel3::Func()
{
    dP = A2+Vp1+K1*(A1-P);
    dVp1 = Vp2+K2*(A1-P);
    dVp2 = K3*(A1-P);
}

double Nabludatel3::GetP()
{
    return P;
}

double Nabludatel3::GetVp()
{
    return A2+Vp1;
}

double Nabludatel3::GetVp0()
{
    //Vp0 = dVp1-Vp2;
    return Vp1;
}

double Nabludatel3::GetVp1()
{
    return Vp2;
}

double Nabludatel3::GetVp2()
{
    return dVp2;
}

double Nabludatel3::GetdVp0()
{
    //Vp0 = dVp1-Vp2;
    Vp0 = Vp1-IntVp2;
    return Vp0;
}

double Nabludatel3::GetdVp1()
{
    return dVp1;
}
