#ifndef NABLUDATELC3_H
#define NABLUDATELC3_H

#include <QObject>
#include <QDebug>

#include "Nabludatel/nabludatel3.h"
#include "Filters/fnhfilter.h"

class NabludatelC3 : public QObject
{
    Q_OBJECT
public:
    explicit NabludatelC3(QObject *parent = nullptr);

private:

    double P_now = 0; // оценка текущая P_now = Naqbludatel
    double dP_now = 0; // производная dP_now = A2-Vp0
    double Vp = 0; // скорость сейчас с учетом всех поправок для интегрирования Vp = Nabludatel
    double Vp0 = 0; // текущая оценка постоянной ошибки скорости, является текущей оценкой всей ошибки при работающем наблюдателе Vp0 = Nabludatel
    double Vp1 = 0; // текущая оценка коэффициента растущей со времнем ошибки скорости Vp1 = Nabludatel
    double dVp1 = 0; // текущая оценка производной коэффициента растущей со времнем ошибки скорости dVp1 = Nabludatel, должна быть 0
    double Vp2 = 0; // текущая суммарная оценка ошибки скорости Vp2 = Vp0 + Vp1t
    double dVp2 = 0; // текущая оценка производной суммарной оценки ошибки скорости, должна быть Vp1
    double IntVp1 = 0; // результат интегрирования фильтрованного коэффициента линейной ошибки скорости Vp1;
    double IntVp2 = 0; // результат интегрирования фильтрованного коэффициента линейной ошибки скорости Vp2;
    double Vp0f_after[2] = {0,0}; // результат фильтрации постоянной ошибки по скорости ФНЧ
    double Vp1f_after[2] = {0,0}; // результат фильтрации коэффициента линейно растущей со времненем ошибки ФНЧ
    double dVp1f_after[2] = {0,0}; // результат фильтрации производной коэффициента линейно растущей со времненем ошибки ФНЧ
    double Vp2f_after[2] = {0,0}; // результат фильтрации суммарной ошибки ФНЧ
    double dVp2f_after[2] = {0,0}; // результат фильтрации оценки производной суммарной ошибки ФНЧ
    double A2_after[2] = {0,0}; // фильтр скорости для равного запаздывания
    double Vp_after[2] = {0,0}; // фильтр скорости для равного запаздывания
    double Vp0f_before[2] = {0,0}; // постоянная ошибка по скорости до фильтрации
    double Vp1f_before[2] = {0,0}; // коэффициент линейно растущей со времнем ошибки по скорости до фильтрации
    double dVp1f_before[2] = {0,0}; // производная коэффициента линейно растущей со времненем ошибки до фильтрации
    double Vp2f_before[2] = {0,0}; // суммарная ошибки до фильтрации
    double dVp2f_before[2] = {0,0}; // оценка производной суммарной ошибки до фильтрации
    double A2_before[2] = {0,0}; // фильтр скорости для равного запазывания
    double Vp_before[2] = {0,0}; // фильтр скорости для равного запазывания

    double TauVp0 = 8; // постоянная времени фильтра постоянной ошибки скорости
    double TauVp1 = 8; // постоянняа времени фильтра коэффициента линейно растущей со временем ошибки скорости
    double TaudVp1 = 8; // постоянная времнеи фильтра производной коэффициента линейно растущей ошибки скорости
    double TauVp2 = 8; // постоянная времени фильтра оценки суммарной ошибки скорости
    double TaudVp2 = 8; // постоянная времени фильтра производной оценки суммарной ошибки скорости
    double TauA2 = 8; // постоянная времени фильтра скорости для равного запаздывания

    double A1 = 0; // измерение величины
    double A2 = 0; // изммерение скорости

    bool isCorrect = false; // так как строить логику долго, переключение между работой по наблюдателю и коррекцией производится флагом
                            // само переключение лучше производить сразу по смене периода получения данных, чтобы минимизировать ошибку

    Nabludatel3* nabl; // наблюдатель для оценок при высокой частоте
    FNHfilter* filt; // фильтр для коррекции

public slots:

    void Calc(double step, double a1_new, double a2_new);
    void SetCorrect();
    void SetK(double k1_new, double k2_new, double k3_new);
    void Runge(double step);
    void Func();
    void Correct(double a1_new);

    double GetP();
    double GetVp();
    double GetVp0();
    double GetVp0f();
    double GetVp1();
    double GetVp1f();
    double GetdVp1();
    double GetdVp1f();
    double GetVp2();
    double GetVp2f();
    double GetdVp2();
    double GetdVp2f();
};

#endif // NABLUDATELC3_H
