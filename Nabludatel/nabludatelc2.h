#ifndef NABLUDATELC2_H
#define NABLUDATELC2_H

#include <QObject>
#include <QDebug>

#include "Nabludatel/nabludatel2.h"
#include "Filters/fnhfilter.h"

class NabludatelC2 : public QObject
{
    Q_OBJECT
public:
    explicit NabludatelC2(QObject *parent = nullptr);

private:

    double P_now = 0; // оценка текущая P_now = Naqbludatel
    double dP_now = 0; // производная dP_now = A2-Vp0
    double Vp = 0; // скорость сейчас с учетом всех поправок для интегрирования Vp = Nabludatel
    double Vp0 = 0; // текущая оценка постоянной ошибки скорости, является текущей оценкой всей ошибки при работающем наблюдателе Vp0 = Nabludatel
    double dVp0 = 0; // производная постоянной ошибки скорости dVp0 = Nabludatel, должна быть 0
    double Vp0f_after[2] = {0,0}; // результат фильтрации постоянной ошибки по скорости ФНЧ
    double dVp0f_after[2] = {0,0}; // результат фильтрациипроизводной постоянной ошибки скорости ФНЧ
    double Vp0f_before[2] = {0,0}; // постоянная ошибка по скорости до фильтрации
    double dVp0f_before[2] = {0,0}; // производная постоянной ошибки по скорости до фильтрации
    double Vp01 = 0; // результатт интегрирования производной постоянной ошибки, которая должна быть нулевой

    double TauVp0 = 8; // постоянная времени фильтра постоянной ошибки скорости
    double TaudVp0 = 8; // постоянняа времени фильтра производной постояннйо ошибки скорости

    double A1 = 0; // измерение величины
    double A2 = 0; // изммерение скорости

    bool isCorrect = false; // так как строить логику долго, переключение между работой по наблюдателю и коррекцией производится флагом
                            // само переключение лучше производить сразу по смене периода получения данных, чтобы минимизировать ошибку

    Nabludatel2* nabl; // наблюдатель для оценок при высокой частоте
    FNHfilter* filt; // фильтр для коррекции

public slots:

    void Calc(double step, double a1_new, double a2_new);
    void SetCorrect();
    void SetK(double k1_new, double k2_new);
    void Runge(double step);
    void Func();
    void Correct(double a1_new);

    double GetP();
    double GetVp();
    double GetVp0();
    double GetVp0f();
    double GetdVp0();
    double GetdVp0f();
};


#endif // NABLUDATELC2_H
