#include "nabludatel2.h"
#include <QDebug>

Nabludatel2::Nabludatel2(QObject *parent) : QObject(parent)
{
    // тут ничего
}

void Nabludatel2::SetK(double k1_new, double k2_new)
{
    K1 = k1_new;
    K2 = k2_new;
}

void Nabludatel2::Calc(double step, double a1_new, double a2_new)
{
    A1 =a1_new;
    A2 = a2_new;
    Runge(step);
    qDebug()<<"A1 "<<A1<<" A2 "<<A2;
}

void Nabludatel2::Runge(double step)
{
    // создаем переменные для метода Рунге-Кутта 4-го порядка
    double tempP = 0;
    double tempVp0 = 0;

    double tempdP = 0;
    double tempdVp0 = 0;

    // по лекциям
    Func();

    tempP = P;
    tempVp0 = Vp0;

    tempdP = dP;
    tempdVp0 = dVp0;

    P = tempP + 0.5*step*dP;
    Vp0 = tempVp0 + 0.5*step*dVp0;

    // второй подход
    Func();

    tempdP = tempdP + 2.0*dP;
    tempdVp0 = tempdVp0 + 2.0*dVp0;

    P = tempP + 0.5*step*dP;
    Vp0 = tempVp0 + 0.5*step*dVp0;

    // третий подход
    Func();

    tempdP = tempdP + 2.0*dP;
    tempdVp0 = tempdVp0 + 2.0*dVp0;

    P = tempP + step*dP;
    Vp0 = tempVp0 + step*dVp0;

    // четвертый подход
    Func();

    P = tempP + (step/6.0)*(tempdP + dP);
    Vp0 = tempVp0 + (step/6.0)*(tempdVp0 + dVp0);
}

void Nabludatel2::Func()
{
    dP = A2+Vp0+K1*(A1-P);
    dVp0 = K2*(A1-P);
}

double Nabludatel2::GetP()
{
    return P;
}

double Nabludatel2::GetVp()
{
    return A2+Vp0;
}

double Nabludatel2::GetVp0()
{
    return Vp0;
}

double Nabludatel2::GetdVp0()
{
    return dVp0;
}
