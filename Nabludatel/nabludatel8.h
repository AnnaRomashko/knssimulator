#ifndef NABLUDATEL8_H
#define NABLUDATEL8_H

#include <QObject>

class Nabludatel8 : public QObject
{
    Q_OBJECT
public:
    explicit Nabludatel8(QObject *parent = nullptr);

private:

    double P = 0;
    double dP = 0;
    double Vp0 = 0;
    double Vp1 =0;
    double Vp2 = 0;
    double Vp3 = 0;
    double Vp4 = 0;
    double Vp5 = 0;
    double Vp6 = 0;
    double Vp7 = 0;
    double dVp1 = 0;
    double dVp2 = 0;
    double dVp3 = 0;
    double dVp4 = 0;
    double dVp5 = 0;
    double dVp6 = 0;
    double dVp7 = 0;
    double IntVp2 = 0;
    double K1 = 2.04;
    double K2 = 1.2;
    double K3 = 0.31;
    double K4 = 0.031;
    double K5 = 0.0031;
    double K6 = 0.00031;
    double K7 = 0.000031;
    double K8 = 0.0000031;

    double A1 = 0;
    double A2 = 0;

public slots:

    void SetK(double k1_new, double k2_new, double k3_new, double k4_new, double k5_new, double k6_new, double k7_new, double k8_new);
    void Calc(double step, double a1_new, double a2_new);
    void Func();
    void Runge(double step);

    double GetP();
    double GetVp();
    double GetVp0();
    double GetVp1();
    double GetVp2();
    double GetVp3();
    double GetVp4();
    double GetVp5();
    double GetVp6();
    double GetVp7();

    //double GetdVp0();
    //double GetdVp1();
};

#endif // NABLUDATEL8_H
