#include "nabludatelc3.h"

NabludatelC3::NabludatelC3(QObject *parent) : QObject(parent)
{
    // создаем наблюдатель
    nabl = new Nabludatel3(this);

    // создаем фильтр
    filt = new FNHfilter(this);
}

void NabludatelC3::SetK(double k1_new, double k2_new, double k3_new)
{
    nabl->SetK(k1_new,k2_new,k3_new);
}

void NabludatelC3::Calc(double step, double a1_new, double a2_new)
{
    if (!isCorrect) // работа по наблюдателю
    {
        // просто запускаем наблюдатель
        A2=a2_new;
        A1=a1_new;
        nabl->Calc(step, A1,A2);
        P_now = nabl->GetP();
        Vp = nabl->GetVp();
        Vp0 = -nabl->GetdVp0(); // текущая оценка постоянной ошибки
        Vp1 = -nabl->GetVp1(); // текущая оценка линейной ошибки
        dVp1 = -nabl->GetVp2(); // текущая оценка производной линейной ошибки
        Vp2 = -nabl->GetVp0(); // текущая оценка суммарной ошибки
        dVp2  =-nabl->GetdVp1(); // текущая оценка производной суммарной ошибки
        dP_now = Vp;
        Vp0f_before[0] = Vp0;
        Vp1f_before[0] = Vp1;
        dVp1f_before[0] = dVp1;
        Vp2f_before[0] = Vp2;
        dVp2f_before[0] = dVp2;
        A2_before[0] = A2;
        Vp_before[0] = Vp;
        filt->filter(TauVp0,step,Vp0f_before,Vp0f_after,0);
        filt->filter(TauVp1,step,Vp1f_before,Vp1f_after,0);
        filt->filter(TaudVp1,step,dVp1f_before,dVp1f_after,0);
        filt->filter(TauVp2,step,Vp2f_before,Vp2f_after,0);
        filt->filter(TaudVp2,step,dVp2f_before,dVp2f_after,0);
        filt->filter(TauA2,step,A2_before,A2_after,0);
        filt->filter(TauA2,step,Vp_before,Vp_after,0);
        //Vp2 = A2_after[0]-Vp_after[0];
        //Runge(step);
        //Vp0 = IntVp1-Vp2;
        //qDebug()<<"Vp0 "<<Vp0<<"dVp0 "<<dVp0<<"A1 "<<A1<<"A2 "<<A2;
    }
    else // режим коррекции
    {
        // чтобы не менять фактический приход данных с медленного датчика задержка реализуется в коррекции
        A2 = a2_new;
        Vp = A2-IntVp1-Vp2;//-Vp0f_after[0];//+(Vp2f_after[0]-Vp0f_after[0]);
        Runge(step);
        //qDebug()<<"Vp0 "<<Vp0<<"dVp1 "<<dVp1<<" Vp1 "<<Vp1<<"A1 "<<A1<<"A2 "<<A2;
        //qDebug()<<"P_now "<<P_now;
    }
}

void NabludatelC3::Correct(double a1_new)
{
    A1 = a1_new;
    P_now = A1;
    /*Vp0 = A2-nabl->GetVp(); // ошибка текущая
    dVp1 = -nabl->GetVp1(); // коэффициент растущей со врменем ошибки*/
    //Vp0 = -nabl->GetVp0(); // ошибка текущая
    //Vp1 = 0;
    //dVp1 = -nabl->GetVp1(); // коэффициент растущей со врменем ошибки
}

void NabludatelC3::Func()
{
    dP_now = Vp; //(Vp0+Vp1);
    //Vp = A2-Vp1; //(Vp0+Vp1);
}

void NabludatelC3::SetCorrect()
{
    isCorrect = true;
    //Vp0 = A2-Vp;
}

double NabludatelC3::GetP()
{
    return P_now;
}

double NabludatelC3::GetVp()
{
    return Vp;
}

double NabludatelC3::GetVp0()
{
    return Vp0;
}

double NabludatelC3::GetVp0f()
{
    return Vp0f_after[0];
}

double NabludatelC3::GetVp1()
{
    return Vp1;
}

double NabludatelC3::GetVp1f()
{
    return Vp1f_after[0];
}

double NabludatelC3::GetdVp1()
{
    return dVp1;
}

double NabludatelC3::GetdVp1f()
{
    return dVp1f_after[0];
}

double NabludatelC3::GetVp2()
{
    return Vp2;
}

double NabludatelC3::GetVp2f()
{
    return Vp2f_after[0];
}

double NabludatelC3::GetdVp2()
{
    return dVp2;
}

double NabludatelC3::GetdVp2f()
{
    return dVp2f_after[0];
}

void NabludatelC3::Runge(double step)
{
    // создаем переменные для метода Рунге-Кутта 4-го порядка
    double tempP_now = 0;
    double tempIntVp1 = 0;
    double tempIntVp2 = 0;

    double tempdP_now = 0;
    double tempVp1 = 0;
    double tempVp2 = 0;

    tempIntVp1 = IntVp1;

    tempVp1 = Vp1f_after[0];

    IntVp1 = tempIntVp1 + 0.5*step*Vp1f_after[0];

    // второй подход
    tempVp1 = tempVp1 + 2.0*Vp1f_after[0];

    IntVp1 = tempIntVp1 + 0.5*step*Vp1f_after[0];

    // третий подход
    tempVp1 = tempVp1 + 2.0*Vp1f_after[0];

    IntVp1 = tempIntVp1 + step*Vp1f_after[0];

    // четвертый подход
    IntVp1 = tempIntVp1 + (step/6.0)*(tempVp1 + Vp1f_after[0]);
    /*tempIntVp1 = IntVp1;

    tempVp1 = Vp1;

    IntVp1 = tempIntVp1 + 0.5*step*Vp1;

    // второй подход
    tempVp1 = tempVp1 + 2.0*Vp1;

    IntVp1 = tempIntVp1 + 0.5*step*Vp1;

    // третий подход
    tempVp1 = tempVp1 + 2.0*Vp1;

    IntVp1 = tempIntVp1 + step*Vp1;

    // четвертый подход
    IntVp1 = tempIntVp1 + (step/6.0)*(tempVp1 + Vp1);*/

    // по лекциям
    if (!isCorrect)
    {
        //tempIntVp1 = IntVp1;
        tempIntVp2 = IntVp2;

        //tempVp1 = Vp1f_after[0];
        tempVp2 = Vp2f_after[0];

        //IntVp1 = tempIntVp1 + 0.5*step*Vp1f_after[0];
        IntVp2 = tempIntVp2 + 0.5*step*Vp2f_after[0];

        // второй подход
        //tempVp1 = tempVp1 + 2.0*Vp1f_after[0];
        tempVp2 = tempVp2 + 2.0*Vp2f_after[0];

        //IntVp1 = tempIntVp1 + 0.5*step*Vp1f_after[0];
        IntVp2 = tempIntVp2 + 0.5*step*Vp2f_after[0];

        // третий подход
        //tempVp1 = tempVp1 + 2.0*Vp1f_after[0];
        tempVp2 = tempVp2 + 2.0*Vp2f_after[0];

        //IntVp1 = tempIntVp1 + step*Vp1f_after[0];
        IntVp2 = tempIntVp2 + step*Vp2f_after[0];

        // четвертый подход
        //IntVp1 = tempIntVp1 + (step/6.0)*(tempVp1 + Vp1f_after[0]);
        IntVp2 = tempIntVp2 + (step/6.0)*(tempVp2 + Vp2f_after[0]);
    }
    else
    {
        Func();

        tempP_now = P_now;

        tempdP_now = dP_now;

        P_now = tempP_now + 0.5*step*dP_now;

        // второй подход
        Func();

        tempdP_now = tempdP_now + 2.0*dP_now;

        P_now = tempP_now + 0.5*step*dP_now;

        // третий подход
        Func();

        tempdP_now = tempdP_now + 2.0*dP_now;

        P_now = tempP_now + step*dP_now;

        // четвертый подход
        Func();

        P_now = tempP_now + (step/6.0)*(tempdP_now + dP_now);
    }
}

