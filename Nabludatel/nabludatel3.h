#ifndef NABLUDATEL3_H
#define NABLUDATEL3_H


#include <QObject>

class Nabludatel3 : public QObject
{
    Q_OBJECT
public:
    explicit Nabludatel3(QObject *parent = nullptr);

private:

    double P = 0;
    double dP = 0;
    double Vp0 = 0;
    double Vp1 =0;
    double dVp1 = 0;
    double Vp2 = 0;
    double dVp2 = 0;
    double IntVp2 = 0;
    double K1 = 2.04;
    double K2 = 1.2;
    double K3 = 0.31;

    double A1 = 0;
    double A2 = 0;

public slots:

    void SetK(double k1_new, double k2_new, double k3_new);
    void Calc(double step, double a1_new, double a2_new);
    void Func();
    void Runge(double step);

    double GetP();
    double GetVp();
    double GetVp0();
    double GetVp1();
    double GetVp2();

    double GetdVp0();
    double GetdVp1();
};


#endif // NABLUDATEL3_H
