#include "kalman.h"

Kalman::Kalman(QObject *parent) : QObject(parent)
{
    // зададим размеры матрицам
    InitKalman1();
    InitKalman2();

    // зададим начальные значения
    resetMetrix();
}

void Kalman::resetMetrix()
{
    // большинство матриц 4х4 нужно обнулить
    for (int i=0;i<kalman1_n;i++)
    {
        for (int j=0;j<1;j++)
        {
            kalman1.X_now(i,j) = 0;
            kalman1.X_prev(i,j) = 0;
            kalman1.X_mid(i,j) = 0;
            kalman1.KZ(i,j) = 0;
        }
    }
    for (int i=0;i<kalman1_n;i++)
    {
        for (int j=0;j<kalman1_n;j++)
        {
            kalman1.P_now(i,j) = 0;
            kalman1.P_mid(i,j) = 0;
            kalman1.P_prev(i,j) = 0;
            kalman1.F_now(i,j) = 0;
            kalman1.F_now_t(i,j) = 0;
            kalman1.PF_t(i,j) = 0;
            kalman1.FPF_t(i,j) = 0;
            kalman1.KH(i,j) = 0;
            kalman1.EKH(i,j) = 0;
            kalman1.E(i,j) = 0;
            kalman1.QG(i,j) = 0;
        }
    }
    for (int i=0;i<kalman1_m;i++)
    {
        for (int j=0;j<kalman1_m;j++)
        {
            kalman1.R_now(i,j) = 0;
            kalman1.HPH_t(i,j) = 0;
            kalman1.HPHR_sum(i,j) = 0;
            kalman1.HPHR_1(i,j) = 0;
        }
    }
    for (int i=0;i<kalman1_m;i++)
    {
        for (int j=0;j<1;j++)
        {
            kalman1.Z_now(i,j) = 0;
            kalman1.Z_mid(i,j) = 0;
            kalman1.Z_prev(i,j) = 0;
        }
    }
    for (int i=0;i<kalman1_n;i++)
    {
        for (int j=0;j<kalman1_m;j++)
        {
            kalman1.PH_t(i,j) = 0;
            kalman1.HR(i,j) = 0;
            kalman1.K_now(i,j) = 0;
            kalman1.H_now_t(i,j) = 0;

            kalman1.H_now(j,i) = 0;
        }
    }
    for (int i=0;i<kalman1_n;i++)
    {
        for (int j=0;j<kalman1_l;j++)
        {
            kalman1.G_now(i,j) = 0;
            kalman1.GQ(i,j) = 0;

            kalman1.G_now_t(j,i) = 0;
        }
    }
    for (int i=0;i<kalman1_l;i++)
    {
        for (int j=0;j<kalman1_l;j++)
        {
            kalman1.Q_now(i,j) = 0;
        }
    }
    kalman1.G_now(0,0) = 2;
    kalman1.G_now(0,1) = 1;
    kalman1.G_now(1,0) = 1;
    kalman1.G_now(1,1) = 2;
    kalman1.G_now(2,0) = 0.2;
    kalman1.G_now(2,1) = 0.1;
    kalman1.G_now(3,0) = 0.1;
    kalman1.G_now(3,1) = 0.2;
    kalman1.G_now_t = trans(kalman1.G_now);
    kalman1.H_now(0,0) = 1;
    kalman1.H_now(1,1) = 1;
    kalman1.H_now_t = trans(kalman1.H_now);
    kalman1.Q_now(0,0) = 1;
    kalman1.Q_now(1,1) = 1;
    kalman1.E(0,0) = 1;
    kalman1.E(1,1) = 1;
    kalman1.E(2,2) = 1;
    kalman1.E(3,3) = 1;
    kalman1.R_now(0,0) = 10;
    kalman1.R_now(1,1) = 10;
    for (int i=0;i<kalman1_n;i++)
        for(int j=0;j<kalman1_n;j++)
        {
            kalman1.P_now(i,j) = 1;
        }
    kalman1.P_now(0,0) = 5;
    kalman1.P_now(1,1) = 5;
    kalman1.P_now(2,2) = 5;
    kalman1.P_now(3,3) = 5;
    //std::cout<<X_prev<<std::endl;

    // для фильтра Калмана 2
    for (int i=0;i<kalman1_n;i++)
    {
        for (int j=0;j<1;j++)
        {
            kalman2.X_now(i,j) = 0;
            kalman2.dX_now(i,j) = 0;
            kalman2.FX(i,j) = 0;
            kalman2.BU(i,j) = 0;
            kalman2.KZ(i,j) = 0;
        }
    }
    for (int i=0;i<kalman1_n;i++)
    {
        for (int j=0;j<kalman1_n;j++)
        {
            kalman2.P_now(i,j) = 0;
            kalman2.dP_now(i,j) = 0;
            kalman2.F_now(i,j) = 0;
            kalman2.F_now_t(i,j) = 0;
            kalman2.FP(i,j) = 0;
            kalman2.PF_t(i,j) = 0;
            kalman2.PHRH(i,j) = 0;
            kalman2.PHRHP(i,j) = 0;
        }
    }
    for (int i=0;i<kalman1_m;i++)
    {
        for (int j=0;j<kalman1_m;j++)
        {
            kalman2.R_now(i,j) = 0;
            kalman2.R_now_1(i,j) = 0;
        }
    }
    for (int i=0;i<kalman1_m;i++)
    {
        for (int j=0;j<1;j++)
        {
            kalman2.Z_now(i,j) = 0;
            kalman2.HX(i,j) = 0;
            kalman2.ZHX(i,j) = 0;
        }
    }
    for (int i=0;i<kalman1_n;i++)
    {
        for (int j=0;j<kalman1_m;j++)
        {
            kalman2.PH_t(i,j) = 0;
            kalman2.PHR_1(i,j) = 0;
            kalman2.PH_new_t(i,j) = 0;
            kalman2.K_now(i,j) = 0;
            kalman2.H_now_t(i,j) = 0;

            kalman2.H_now(j,i) = 0;
        }
    }
    for (int i=0;i<kalman1_n;i++)
    {
        for (int j=0;j<kalman1_l;j++)
        {
            kalman2.B_now(i,j) = 0;
        }
    }
    for (int i=0;i<kalman1_l;i++)
    {
        for (int j=0;j<1;j++)
        {
            kalman2.U_now(i,j) = 0;
        }
    }
    kalman2.H_now(0,0) = 1;
    kalman2.H_now(1,1) = 1;
    kalman2.H_now_t = trans(kalman2.H_now);
    kalman2.R_now(0,0) = 25;
    kalman2.R_now(1,1) = 25;
    bool temp_bool = true;
    kalman2.R_now_1 = gjinverse<double>(kalman2.R_now,temp_bool);
    /*for (int i=0;i<kalman1_n;i++)
        for(int j=0;j<kalman1_n;j++)
        {
            kalman2.P_now(i,j) = 0.01;
        }*/
    kalman2.P_now(0,0) = 0.001;
    kalman2.P_now(1,1) = 0.001;
    kalman2.P_now(2,2) = 0.001;
    kalman2.P_now(3,3) = 0.001;
}

void Kalman::InitKalman1()
{
    kalman1.X_now.resize(kalman1_n,1);
    kalman1.X_mid.resize(kalman1_n,1);
    kalman1.X_prev.resize(kalman1_n,1);
    kalman1.P_now.resize(kalman1_n,kalman1_n);
    kalman1.P_mid.resize(kalman1_n,kalman1_n);
    kalman1.P_prev.resize(kalman1_n,kalman1_n);
    kalman1.R_now.resize(kalman1_m,kalman1_m);
    kalman1.F_now.resize(kalman1_n,kalman1_n);
    kalman1.F_now_t.resize(kalman1_n,kalman1_n);
    kalman1.Z_now.resize(kalman1_m,1);
    kalman1.Z_mid.resize(kalman1_m,1);
    kalman1.Z_prev.resize(kalman1_m,1);
    kalman1.PF_t.resize(kalman1_n,kalman1_n);
    kalman1.FPF_t.resize(kalman1_n,kalman1_n);
    kalman1.PH_t.resize(kalman1_n,kalman1_m);
    kalman1.HPH_t.resize(kalman1_m,kalman1_m);
    kalman1.HPHR_sum.resize(kalman1_m,kalman1_m);
    kalman1.HPHR_1.resize(kalman1_m,kalman1_m);
    kalman1.HR.resize(kalman1_n,kalman1_m);
    kalman1.K_now.resize(kalman1_n,kalman1_m);
    kalman1.KH.resize(kalman1_n,kalman1_n);
    kalman1.EKH.resize(kalman1_n,kalman1_n);
    kalman1.KZ.resize(kalman1_n,1);
    kalman1.GQ.resize(kalman1_n,kalman1_l);
    kalman1.QG.resize(kalman1_n,kalman1_n);

    kalman1.H_now.resize(kalman1_m,kalman1_n);
    kalman1.H_now_t.resize(kalman1_n,kalman1_m);
    kalman1.Q_now.resize(kalman1_l,kalman1_l);
    kalman1.E.resize(kalman1_n,kalman1_n);
    kalman1.G_now.resize(kalman1_n,kalman1_l);
    kalman1.G_now_t.resize(kalman1_l,kalman1_n);
}

void Kalman::InitKalman2()
{
    kalman2.X_now.resize(kalman1_n,1);
    kalman2.dX_now.resize(kalman1_n,1);
    kalman2.FX.resize(kalman1_n,1);
    kalman2.BU.resize(kalman1_n,1);
    kalman2.KZ.resize(kalman1_n,1);

    kalman2.P_now.resize(kalman1_n,kalman1_n);
    kalman2.dP_now.resize(kalman1_n,kalman1_n);
    kalman2.F_now.resize(kalman1_n,kalman1_n);
    kalman2.F_now_t.resize(kalman1_n,kalman1_n);
    kalman2.FP.resize(kalman1_n,kalman1_n);
    kalman2.PF_t.resize(kalman1_n,kalman1_n);
    kalman2.PHRH.resize(kalman1_n,kalman1_n);
    kalman2.PHRHP.resize(kalman1_n,kalman1_n);

    kalman2.R_now.resize(kalman1_m,kalman1_m);
    kalman2.R_now_1.resize(kalman1_m,kalman1_m);

    kalman2.Z_now.resize(kalman1_m,1);
    kalman2.HX.resize(kalman1_m,1);
    kalman2.ZHX.resize(kalman1_m,1);

    kalman2.PH_t.resize(kalman1_n,kalman1_m);
    kalman2.PHR_1.resize(kalman1_n,kalman1_m);
    kalman2.PH_new_t.resize(kalman1_n,kalman1_m);
    kalman2.K_now.resize(kalman1_n,kalman1_m);
    kalman2.H_now_t.resize(kalman1_n,kalman1_m);

    kalman2.H_now.resize(kalman1_m,kalman1_n);

    kalman2.B_now.resize(kalman1_n,kalman1_l);

    kalman2.U_now.resize(kalman1_l,1);
}

void Kalman::calcKalman(double dx, double dz, double psi)
{
    // первый шаг
    kalman1.X_prev = kalman1.X_now;
    kalman1.F_now(0,2) = cos(psi);
    kalman1.F_now(0,3) = -sin(psi);
    kalman1.F_now(1,2) = sin(psi);
    kalman1.F_now(1,3) = cos(psi);
    kalman1.Z_now(0,0) = dx;
    kalman1.Z_now(1,0) = dz;
    /*kalman1.G_now(0,0) = cos(psi);
    kalman1.G_now(0,1) = -sin(psi);
    kalman1.G_now(1,0) = sin(psi);
    kalman1.G_now(1,1) = cos(psi);
    kalman1.G_now_t = trans(kalman1.G_now);*/
    kalman1.P_prev = kalman1.P_now;
    kalman1.X_mid = prod(kalman1.F_now,kalman1.X_prev);

    //qDebug()<<"first step";

    // второй шаг
    kalman1.Z_prev = prod(kalman1.H_now, kalman1.X_mid);

    //qDebug()<<"second step";

    // третий шаг
    for (int i=0;i<kalman1.Z_now.size1();i++)
        for (int j=0;j<kalman1.Z_now.size2();j++)
            kalman1.Z_mid(i,j) = kalman1.Z_now(i,j)-kalman1.Z_prev(i,j);
    //std::cout<<"Z_now "<<kalman1.Z_now<<std::endl;
    //std::cout<<"Z_prev "<<kalman1.Z_prev<<std::endl;

    //qDebug()<<"third step";

    // четвертый шаг
    kalman1.F_now_t = trans(kalman1.F_now);
    kalman1.PF_t = prod(kalman1.P_prev,kalman1.F_now_t);
    kalman1.FPF_t = prod(kalman1.F_now,kalman1.PF_t);
    kalman1.GQ = prod(kalman1.G_now,kalman1.Q_now);
    kalman1.QG = prod(kalman1.GQ,kalman1.G_now_t);
    for (int i=0;i<kalman1_n;i++)
        for (int j=0;j<kalman1_n;j++)
            kalman1.P_mid(i,j) = kalman1.FPF_t(i,j) + kalman1.QG(i,j);
    //kalman1.P_mid = kalman1.FPF_t;
    //std::cout<<"P_mid "<<kalman1.P_mid<<std::endl;

    // пятый шаг
    kalman1.PH_t = prod(kalman1.P_mid,kalman1.H_now_t);
    kalman1.HPH_t = prod(kalman1.H_now,kalman1.PH_t);
    for (int i=0;i<kalman1_m;i++)
        for (int j=0;j<kalman1_m;j++)
            kalman1.HPHR_sum(i,j) = kalman1.HPH_t(i,j)+kalman1.R_now(i,j);
    bool temp_bool = true;
    kalman1.HPHR_1 = gjinverse<double>(kalman1.HPHR_sum,temp_bool);
    kalman1.HR = prod(kalman1.H_now_t,kalman1.HPHR_1);
    kalman1.K_now = prod(kalman1.P_mid, kalman1.HR);
    //std::cout<<"HPHR_1 "<<kalman1.HPHR_1<<std::endl;
    //std::cout<<"HR "<<kalman1.HR<<std::endl;
    //std::cout<<"K_now  "<<kalman1.K_now <<std::endl;

    // шестой шаг
    kalman1.KH = prod(kalman1.K_now,kalman1.H_now);
    for (int i=0;i<kalman1_n;i++)
        for (int j=0;j<kalman1_n;j++)
            kalman1.EKH(i,j) = kalman1.E(i,j)-kalman1.KH(i,j);
    kalman1.P_now = prod(kalman1.EKH,kalman1.P_mid);
    //std::cout<<"KH "<<kalman1.KH <<std::endl;
    //std::cout<<"EKH "<<kalman1.EKH <<std::endl;
    //std::cout<<"P_now "<<kalman1.P_now <<std::endl;

    // седьмой шаг
    kalman1.KZ = prod(kalman1.K_now,kalman1.Z_mid);
    for (int i=0;i<kalman1_n;i++)
        for (int j=0;j<1;j++)
            kalman1.X_now(i,j) = kalman1.KZ(i,j)+kalman1.X_mid(i,j);
    //std::cout<<"KZ "<<kalman1.KZ<<std::endl;
    //std::cout<<"X_now "<<kalman1.X_now<<std::endl;
    //std::cout<<"X_mid "<<kalman1.X_mid<<std::endl;
}

double Kalman::getDX()
{
    return kalman1.X_now(0,0);
}

double Kalman::getDZ()
{
    return kalman1.X_now(1,0);
}

double Kalman::getDVx()
{
    return kalman1.X_now(2,0);
}

double Kalman::getDVz()
{
    return kalman1.X_now(3,0);
}

void Kalman::calcKalman2(double x, double z, double psi, double vx, double vz, double step)
{
    // первый шаг
    kalman2.HX = prod(kalman2.H_now,kalman2.X_now);
    kalman2.Z_now(0,0) = x;
    kalman2.Z_now(1,0) = z;
    for (int i=0; i<kalman1_m;i++)
        kalman2.ZHX(i,0) = kalman2.Z_now(i,0)-kalman2.HX(i,0);
    std::cout<<"ZHX "<<kalman2.ZHX<<std::endl;
    std::cout<<"H "<<kalman2.H_now<<std::endl;
    std::cout<<"X "<<kalman2.X_now<<std::endl;
    std::cout<<"Z "<<kalman2.Z_now<<std::endl;

    // второй шаг
    kalman2.F_now(0,2) = cos(psi);
    kalman2.F_now(0,3) = -sin(psi);
    kalman2.F_now(1,2) = sin(psi);
    kalman2.F_now(1,3) = cos(psi);
    std::cout<<"F "<<kalman2.F_now<<std::endl;
    kalman2.F_now_t = trans(kalman2.F_now);

    // третий шаг
    RungeKalman2_P(step);
    std::cout<<"P "<<kalman2.P_now<<std::endl;

    // четвертый шаг
    kalman2.B_now(0,0) = cos(psi);
    kalman2.B_now(0,1) = -sin(psi);
    kalman2.B_now(1,0) = sin(psi);
    kalman2.B_now(1,1) = cos(psi);
    kalman2.U_now(0,0) = vx;
    kalman2.U_now(1,0) = vz;

    // пятый шаг
    RungeKalman2_X(step);
}

void Kalman::CalcKalman2_P()
{
    kalman2.FP = prod(kalman2.F_now,kalman2.P_now);
    std::cout<<"FP "<<kalman2.FP<<std::endl;
    kalman2.PF_t = prod(kalman2.P_now,kalman2.F_now_t);
    std::cout<<"PF_t "<<kalman2.PF_t<<std::endl;
    kalman2.PH_t = prod(kalman2.P_now,kalman2.H_now_t);
    std::cout<<"PH_t "<<kalman2.PH_t<<std::endl;
    kalman2.PHR_1 = prod(kalman2.PH_t,kalman2.R_now_1);
    std::cout<<"PHR_1 "<<kalman2.PHR_1<<std::endl;
    kalman2.PHRH = prod(kalman2.PHR_1,kalman2.H_now);
    std::cout<<"PHRH "<<kalman2.PHRH<<std::endl;
    kalman2.PHRHP = prod(kalman2.PHRH, kalman2.P_now);
    std::cout<<"PHRHP "<<kalman2.PHRHP<<std::endl;
    for (int i=0;i<kalman1_n;i++)
        for (int j=0;j<kalman1_n;j++)
            kalman2.dP_now(i,j) = kalman2.FP(i,j) + kalman2.PF_t(i,j) + kalman2.PHRHP(i,j);
    std::cout<<"dP "<<kalman2.dP_now<<std::endl;
    /*std::cout<<"FP "<<kalman2.FP<<std::endl;
    std::cout<<"PF_t "<<kalman2.PF_t<<std::endl;
    std::cout<<"PH_t "<<kalman2.PH_t<<std::endl;
    std::cout<<"PHR_1 "<<kalman2.PHR_1<<std::endl;
    std::cout<<"PHRH "<<kalman2.PHRH<<std::endl;
    std::cout<<"PHRHP "<<kalman2.PHRHP<<std::endl;*/
}

void Kalman::CalcKalman2_X()
{
    kalman2.PH_new_t = prod(kalman2.P_now,kalman2.H_now_t);
    kalman2.K_now = prod(kalman2.PH_new_t,kalman2.R_now_1);
    kalman2.KZ = prod(kalman2.K_now,kalman2.ZHX);
    kalman2.FX = prod(kalman2.F_now,kalman2.X_now);
    kalman2.BU = prod(kalman2.B_now,kalman2.U_now);
    for (int i=0;i<kalman1_n;i++)
        kalman2.dX_now(i,0) = kalman2.FX(i,0) + kalman2.BU(i,0) + kalman2.KZ(i,0);
}

void Kalman::RungeKalman2_P(double step)
{
    // создаем переменные для метода Рунге-Кутта 4-го порядка
    /*matrix<double> tempP;
    matrix<double> tempDP;

    tempP.resize(kalman1_n,kalman1_n);
    tempDP.resize(kalman1_n,kalman1_n);

    for (int i=0;i<kalman1_n; i++)
        for (int j=0; j<kalman1_n; j++)
        {
            tempP(i,j) = 0;
            tempDP(i,j) = 0;
        }

    // по лекциям
    CalcKalman2_P();

    tempP = kalman2.P_now;
    tempDP = kalman2.dP_now;

    for (int i=0;i<kalman1_n; i++)
        for (int j=0; j<kalman1_n; j++)
            kalman2.P_now(i,j) = tempP(i,j)+0.5*step*kalman2.dP_now(i,j);

    // второй подход
    CalcKalman2_P();

    for (int i=0;i<kalman1_n; i++)
        for (int j=0; j<kalman1_n; j++)
            tempDP(i,j) = tempDP(i,j)+2.0*kalman2.dP_now(i,j);

    for (int i=0;i<kalman1_n; i++)
        for (int j=0; j<kalman1_n; j++)
            kalman2.P_now(i,j) = tempP(i,j)+0.5*step*kalman2.dP_now(i,j);

    // третий подход
    CalcKalman2_P();

    for (int i=0;i<kalman1_n; i++)
        for (int j=0; j<kalman1_n; j++)
            tempDP(i,j) = tempDP(i,j)+2.0*kalman2.dP_now(i,j);

    for (int i=0;i<kalman1_n; i++)
        for (int j=0; j<kalman1_n; j++)
            kalman2.P_now(i,j) = tempP(i,j)+step*kalman2.dP_now(i,j);

    // четвертый подход
    CalcKalman2_P();

    for (int i=0;i<kalman1_n; i++)
        for (int j=0; j<kalman1_n; j++)
            kalman2.P_now(i,j) = tempP(i,j)+(step/6.0)*(tempDP(i,j)+kalman2.dP_now(i,j));*/

    // метод прямоугольников
    CalcKalman2_P();
    for (int i=0;i<kalman1_n; i++)
        for (int j=0; j<kalman1_n; j++)
            kalman2.P_now(i,j) += step*kalman2.dP_now(i,j);
}

void Kalman::RungeKalman2_X(double step)
{
    // создаем переменные для метода Рунге-Кутта 4-го порядка
    matrix<double> tempX;
    matrix<double> tempDX;

    tempX.resize(kalman1_n,1);
    tempDX.resize(kalman1_n,1);

    for (int i=0;i<kalman1_n; i++)
        for (int j=0; j<1; j++)
        {
            tempX(i,j) = 0;
            tempDX(i,j) = 0;
        }

    // по лекциям
    CalcKalman2_X();

    tempX = kalman2.X_now;
    tempDX = kalman2.dX_now;

    for (int i=0;i<kalman1_n; i++)
        for (int j=0; j<1; j++)
            kalman2.X_now(i,j) = tempX(i,j)+0.5*step*kalman2.dX_now(i,j);

    // второй подход
    CalcKalman2_X();

    for (int i=0;i<kalman1_n; i++)
        for (int j=0; j<1; j++)
            tempDX(i,j) = tempDX(i,j)+2.0*kalman2.dX_now(i,j);

    for (int i=0;i<kalman1_n; i++)
        for (int j=0; j<1; j++)
            kalman2.X_now(i,j) = tempX(i,j)+0.5*step*kalman2.dX_now(i,j);

    // третий подход
    CalcKalman2_X();

    for (int i=0;i<kalman1_n; i++)
        for (int j=0; j<1; j++)
            tempDX(i,j) = tempDX(i,j)+2.0*kalman2.dX_now(i,j);

    for (int i=0;i<kalman1_n; i++)
        for (int j=0; j<1; j++)
            kalman2.X_now(i,j) = tempX(i,j)+step*kalman2.dX_now(i,j);

    // четвертый подход
    CalcKalman2_X();

    for (int i=0;i<kalman1_n; i++)
        for (int j=0; j<1; j++)
            kalman2.X_now(i,j) = tempX(i,j)+(step/6.0)*(tempDX(i,j)+kalman2.dX_now(i,j));
}

double Kalman::getKalman2_X()
{
    return kalman2.X_now(0,0);
}

double  Kalman::getKalman2_Z()
{
    return kalman2.X_now(1,0);
}

double  Kalman::getKalman2_dVx()
{
    return kalman2.X_now(2,0);
}

double  Kalman::getKalman2_dVz()
{
    return kalman2.X_now(3,0);
}
