#-------------------------------------------------
#
# Project created by QtCreator 2021-03-24T09:11:51
#
#-------------------------------------------------

QT       += core gui charts network

CONFIG += c++11 console
CONFIG -= app_bundle

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = KNSSimulator_full
TEMPLATE = app

##INCLUDEPATH += /usr/local/Cellar/boost/1.72.0/include
##LIBS += "-L/usr/local/Cellar/boost/1.72.0/lib/"
##LIBS += "-L/usr/local/opt/icu4c/lib/"

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        Connection/udpconnection.cpp \
        Filters/fnhfilter.cpp \
        Filters/headingfilters.cpp \
        Filters/noisefilters.cpp \
        Graphics/chartsgraphic.cpp \
        Graphics/graphicwindow.cpp \
        #Graphics/graphicwindow2.cpp \
        Kalman/kalman2.cpp \
        Kalman/perebor.cpp \
        Nabludatel/nabludatel2.cpp \
        Nabludatel/nabludatel3.cpp \
        Nabludatel/nabludatel8.cpp \
        Nabludatel/nabludatelc0.cpp \
        Nabludatel/nabludatelc1.cpp \
        Nabludatel/nabludatelc2.cpp \
        Nabludatel/nabludatelc3.cpp \
        SimpleContur/simplecontur.cpp \
        #kalman.cpp \
        main.cpp \
        mainwidget.cpp \
        #matrixoperations.cpp \
        shared_includes/configdata.cpp \
        shared_includes/kx_protocol.cpp \
        shared_includes/qkx_coeffs.cpp \
        shared_includes/qpiconfig.cpp

HEADERS += \
        Connection/udpconnection.h \
        Filters/fnhfilter.h \
        Filters/headingfilters.h \
        Filters/noisefilters.h \
        Graphics/chartsgraphic.h \
        Graphics/graphicwindow.h \
        #Graphics/graphicwindow2.h \
        Kalman/kalman2.h \
        Kalman/perebor.h \
        Nabludatel/nabludatel2.h \
        Nabludatel/nabludatel3.h \
        Nabludatel/nabludatel8.h \
        Nabludatel/nabludatelc0.h \
        Nabludatel/nabludatelc1.h \
        Nabludatel/nabludatelc2.h \
        Nabludatel/nabludatelc3.h \
        SimpleContur/simplecontur.h \
        inverse.h \
        #kalman.h \
        mainwidget.h \
        #matrixoperations.h \
        shared_includes/configdata.h \
        shared_includes/kx_protocol.h \
        shared_includes/qkx_coeffs.h \
        shared_includes/qpiconfig.h \
        structs.h

FORMS += \
        Graphics/graphicwindow.ui \
        #mainform.ui \
        #Graphics/graphicwindow2.ui \
        mainwidget.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    Sources/system_picture.qrc
