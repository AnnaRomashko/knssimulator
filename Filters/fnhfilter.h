#ifndef FNHFILTER_H
#define FNHFILTER_H

#include <QObject>

class FNHfilter : public QObject
{
    Q_OBJECT
public:
    explicit FNHfilter(QObject *parent = nullptr);

public slots:
    void filter(double Tau, double Td, double *Data, double *DataF, unsigned char reinit_filter);
};

#endif // FNHFILTER_H
