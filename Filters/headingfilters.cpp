#include "headingfilters.h"

HeadingFilters::HeadingFilters(QObject *parent) : QObject(parent)
{
    // задание размеров матриц фильтров
    SetSize3();

    // инициализация
    Reinit3();
}

void HeadingFilters::SetSize3()
{
    matrix3.X_now.resize(3,1);
    matrix3.X_mid.resize(3,1);
    matrix3.X_prev.resize(3,1);
    matrix3.P_now.resize(3,3);
    matrix3.P_mid.resize(3,3);
    matrix3.P_prev.resize(3,3);
    matrix3.R_now.resize(2,2);
    matrix3.F_now.resize(3,3);
    matrix3.F_now_t.resize(3,3);
    matrix3.Z_now.resize(2,1);
    matrix3.Z_mid.resize(2,1);
    matrix3.Z_prev.resize(2,1);
    matrix3.PF_t.resize(3,3);
    matrix3.FPF_t.resize(3,3);
    matrix3.PH_t.resize(3,2);
    matrix3.HPH_t.resize(2,2);
    matrix3.HPHR_sum.resize(2,2);
    matrix3.HPHR_1.resize(2,2);
    matrix3.HR.resize(3,2);
    matrix3.K_now.resize(3,2);
    matrix3.KH.resize(3,3);
    matrix3.EKH.resize(3,3);
    matrix3.KZ.resize(3,1);
    matrix3.GQ.resize(3,3);
    matrix3.QG.resize(3,3);

    matrix3.H_now.resize(2,3);
    matrix3.H_now_t.resize(3,2);
    matrix3.Q_now.resize(3,3);
    matrix3.E.resize(3,3);
    matrix3.G_now.resize(3,3);
    matrix3.G_now_t.resize(3,3);
}

void HeadingFilters::Reinit3()
{
    for (int i=0;i<3;i++)
    {
        for (int j=0;j<1;j++)
        {
            matrix3.X_now(i,j) = 0;
            matrix3.X_prev(i,j) = 0;
            matrix3.X_mid(i,j) = 0;
            matrix3.KZ(i,j) = 0;
        }
    }
    for (int i=0;i<3;i++)
    {
        for (int j=0;j<3;j++)
        {
            matrix3.P_now(i,j) = 0;
            matrix3.P_mid(i,j) = 0;
            matrix3.P_prev(i,j) = 0;
            matrix3.F_now(i,j) = 0;
            matrix3.F_now_t(i,j) = 0;
            matrix3.PF_t(i,j) = 0;
            matrix3.FPF_t(i,j) = 0;
            matrix3.KH(i,j) = 0;
            matrix3.EKH(i,j) = 0;
            matrix3.E(i,j) = 0;
            matrix3.QG(i,j) = 0;
        }
    }
    for (int i=0;i<2;i++)
    {
        for (int j=0;j<2;j++)
        {
            matrix3.R_now(i,j) = 0;
            matrix3.HPH_t(i,j) = 0;
            matrix3.HPHR_sum(i,j) = 0;
            matrix3.HPHR_1(i,j) = 0;
        }
    }
    for (int i=0;i<2;i++)
    {
        for (int j=0;j<1;j++)
        {
            matrix3.Z_now(i,j) = 0;
            matrix3.Z_mid(i,j) = 0;
            matrix3.Z_prev(i,j) = 0;
        }
    }
    for (int i=0;i<3;i++)
    {
        for (int j=0;j<2;j++)
        {
            matrix3.PH_t(i,j) = 0;
            matrix3.HR(i,j) = 0;
            matrix3.K_now(i,j) = 0;
            matrix3.H_now_t(i,j) = 0;

            matrix3.H_now(j,i) = 0;
        }
    }
    for (int i=0;i<3;i++)
    {
        for (int j=0;j<3;j++)
        {
            matrix3.G_now(i,j) = 0;
            matrix3.GQ(i,j) = 0;

            matrix3.G_now_t(j,i) = 0;
        }
    }
    for (int i=0;i<3;i++)
    {
        for (int j=0;j<3;j++)
        {
            matrix3.Q_now(i,j) = 0;
        }
    }
    for (int i=0;i<3;i++)
    {
        for (int j=0;j<3;j++)
        {
            matrix3.G_now(i,j) = 0.1;
        }
    }
    matrix3.G_now(0,1) = 10;
    matrix3.G_now(0,2) = -10;
    //matrix3.G_now(1,1) = 2;
    //matrix3.G_now(2,0) = 0.2;
    //matrix3.G_now(2,1) = 0.1;
    matrix3.G_now_t = trans(matrix3.G_now);

    matrix3.H_now(0,0) = 1;
    matrix3.H_now(1,1) = 1;
    matrix3.H_now_t = trans(matrix3.H_now);

    matrix3.Q_now(0,0) = 1;
    matrix3.Q_now(1,1) = 1;
    matrix3.Q_now(2,2) = 1;

    matrix3.E(0,0) = 1;
    matrix3.E(1,1) = 1;
    matrix3.E(2,2) = 1;

    matrix3.R_now(0,0) = 10;
    matrix3.R_now(1,1) = 10;
    for (int i=0;i<3;i++)
        for(int j=0;j<3;j++)
        {
            matrix3.P_now(i,j) = 1;
        }
    matrix3.P_now(0,0) = 5;
    matrix3.P_now(1,1) = 5;
    matrix3.P_now(2,2) = 5;
}

void HeadingFilters::SetQ3(matrix<double> Q_set)
{
    matrix3.Q_now = Q_set;
    //qDebug()<<"Q(1,1) "<<matrix3.Q_now(1,1);
}

void HeadingFilters::SetR3(matrix<double> R_set)
{
    matrix3.R_now = R_set;
    //qDebug()<<"R(1,1) "<<matrix3.R_now(1,1);
}

void HeadingFilters::Calc3(double psi, double dpsi)
{
    // первый шаг
    matrix3.X_prev = matrix3.X_now;
    matrix3.F_now(0,1) = 1;
    matrix3.F_now(0,2) = -1;
    matrix3.Z_now(0,0) = psi;
    matrix3.Z_now(1,0) = dpsi;
    /*kalman1.G_now(0,0) = cos(psi);
    kalman1.G_now(0,1) = -sin(psi);
    kalman1.G_now(1,0) = sin(psi);
    kalman1.G_now(1,1) = cos(psi);
    kalman1.G_now_t = trans(kalman1.G_now);*/
    matrix3.P_prev = matrix3.P_now;
    matrix3.X_mid = prod(matrix3.F_now,matrix3.X_prev);

    //qDebug()<<"first step";

    // второй шаг
    matrix3.Z_prev = prod(matrix3.H_now, matrix3.X_mid);

    //qDebug()<<"second step";

    // третий шаг
    for (int i=0;i<matrix3.Z_now.size1();i++)
        for (int j=0;j<matrix3.Z_now.size2();j++)
            matrix3.Z_mid(i,j) = matrix3.Z_now(i,j)-matrix3.Z_prev(i,j);
    //std::cout<<"Z_now "<<matrix3.Z_now<<std::endl;
    //std::cout<<"Z_prev "<<matrix3.Z_prev<<std::endl;

    //qDebug()<<"third step";

    // четвертый шаг
    matrix3.F_now_t = trans(matrix3.F_now);
    matrix3.PF_t = prod(matrix3.P_prev,matrix3.F_now_t);
    matrix3.FPF_t = prod(matrix3.F_now,matrix3.PF_t);
    matrix3.GQ = prod(matrix3.G_now,matrix3.Q_now);
    matrix3.QG = prod(matrix3.GQ,matrix3.G_now_t);
    for (int i=0;i<3;i++)
        for (int j=0;j<3;j++)
            matrix3.P_mid(i,j) = matrix3.FPF_t(i,j) + matrix3.QG(i,j);
    //kalman1.P_mid = kalman1.FPF_t;
    //std::cout<<"P_mid "<<kalman1.P_mid<<std::endl;

    // пятый шаг
    matrix3.PH_t = prod(matrix3.P_mid,matrix3.H_now_t);
    matrix3.HPH_t = prod(matrix3.H_now,matrix3.PH_t);
    for (int i=0;i<2;i++)
        for (int j=0;j<2;j++)
            matrix3.HPHR_sum(i,j) = matrix3.HPH_t(i,j)+matrix3.R_now(i,j);
    bool temp_bool = true;
    matrix3.HPHR_1 = gjinverse<double>(matrix3.HPHR_sum,temp_bool);
    matrix3.HR = prod(matrix3.H_now_t,matrix3.HPHR_1);
    matrix3.K_now = prod(matrix3.P_mid, matrix3.HR);
    //std::cout<<"HPHR_1 "<<kalman1.HPHR_1<<std::endl;
    //std::cout<<"HR "<<kalman1.HR<<std::endl;
    //std::cout<<"K_now  "<<kalman1.K_now <<std::endl;

    // шестой шаг
    matrix3.KH = prod(matrix3.K_now,matrix3.H_now);
    for (int i=0;i<3;i++)
        for (int j=0;j<3;j++)
            matrix3.EKH(i,j) = matrix3.E(i,j)-matrix3.KH(i,j);
    matrix3.P_now = prod(matrix3.EKH,matrix3.P_mid);
    //std::cout<<"KH "<<kalman1.KH <<std::endl;
    //std::cout<<"EKH "<<kalman1.EKH <<std::endl;
    //std::cout<<"P_now "<<kalman1.P_now <<std::endl;

    // седьмой шаг
    matrix3.KZ = prod(matrix3.K_now,matrix3.Z_mid);
    for (int i=0;i<3;i++)
        for (int j=0;j<1;j++)
            matrix3.X_now(i,j) = matrix3.KZ(i,j)+matrix3.X_mid(i,j);
    //std::cout<<"KZ "<<kalman1.KZ<<std::endl;
    //std::cout<<"X_now "<<kalman1.X_now<<std::endl;
    //std::cout<<"X_mid "<<kalman1.X_mid<<std::endl;
}

double HeadingFilters::Get3psi()
{
    return matrix3.X_now(0,0);
}

double HeadingFilters::Get3dpsi()
{
    return matrix3.X_now(1,0);
}

double HeadingFilters::Get3ddpsi()
{
    return matrix3.X_now(2,0);
}
