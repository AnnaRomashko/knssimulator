#ifndef NOISEFILTERS_H
#define NOISEFILTERS_H

#include <QObject>
#include <QDebug>

struct KalmanFilter
{
    KalmanFilter(double R)
    {
        R_now = R;
        Q_now = 0.01;
        P_prev = 1;
        P_mid = 1;
        P_now = 1;
    }

    //перменнные матрицы для фильтра
    double X_now = 0; // матрица текущих оценок
    double X_prev = 0; // матрица предыдущих оценок
    double P_now = 0; // матрица Р на текущем шаге
    double P_mid = 0; // матрица P на промежуточном шаге
    double P_prev = 0; // матрица Р на предыдущем шаге
    double R_now = 0; // матрица R
    double Q_now = 0; // матрица Q
    double Z_now = 0; // матрица Z
    double Z_mid = 0; // матрица Z
    double Z_prev = 0; // матрица Z
    double K_now = 0; // матрица коэффициентов
};

class NoiseFilters : public QObject
{
    Q_OBJECT
public:
    explicit NoiseFilters(QObject *parent = nullptr);

    // периоды дискретизации
    double periodXsns = 0;
    double periodZsns = 0;
    double periodVxlag = 0;
    double periodVzlag = 0;
    double periodPsiMagn =0;

public slots:

    // функции расчетов
    void CalcXsns(double step, double x);
    void CalcZsns(double step, double z);
    void CalcVxLag(double step, double Vx);
    void CalcVzLag(double step, double Vz);
    void CalcPsiMagn(double step, double psi);

    // функции получения данных
    double GetXsnsFNH();
    double GetZsnsFNH();
    double GetVxLagFNH();
    double GetVzLagFNH();
    double GetPsiMagnFNH();

    double GetXsnsKalman();
    double GetZsnsKalman();
    double GetVxLagKalman();
    double GetVzLagKalman();
    double GetPsiMagnKalman();

    // функции установки параметров ФНЧ фильтров
    void SetXsnsFNH(double T);
    void SetZsnsFNH(double T);
    void SetVxLagFNH(double T);
    void SetVzLagFNH(double T);
    void SetPsiMagnFNH(double T);

private:

    // переменные фильтров ФНЧ
    double XsnsFNH[8] = {0,0,0,0,0,0,10,1}; //0,-1,-2 Авх 0, -1, -2 Авых, T, dzita
    double ZsnsFNH[8] = {0,0,0,0,0,0,1,1};
    double VxlagFNH[8] = {0,0,0,0,0,0,10,1};
    double VzlagFNH[8] = {0,0,0,0,0,0,1,1};
    double PsimagnFNH[8] = {0,0,0,0,0,0,10,1};

    // переменные фильтров Калмана
    KalmanFilter* XsnsKalman;
    KalmanFilter* ZsnsKalman;
    KalmanFilter* VxlagKalman;
    KalmanFilter* VzlagKalman;
    KalmanFilter* PsimagnKalman;

private slots:

    // ФНЧ
    void CalcFNH(double step, double n, double* data);

    // Калман
    void CalcKalman(double n, KalmanFilter* data);
};

#endif // NOISEFILTERS_H
