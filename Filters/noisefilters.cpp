#include "noisefilters.h"

NoiseFilters::NoiseFilters(QObject *parent) : QObject(parent)
{
    XsnsKalman = new KalmanFilter(10);
    ZsnsKalman = new KalmanFilter(10);
    VxlagKalman = new KalmanFilter(0.0011);
    VzlagKalman = new KalmanFilter(10);
    PsimagnKalman = new KalmanFilter(0.01);
}

void NoiseFilters::CalcFNH(double step, double n, double* data)
{
    /*data[2] = data[1];
    data[1] = data[0];
    data[0] = n;
    data[5] = data[4];
    data[4] = data[3];
    double temp = 4*data[6]*data[6]+4*data[6]*step*data[7]+step*step;
    double temp2 = data[0]*(2*data[6]*step+step*step)+data[1]*2*step+data[2]*(step*step-2*step*data[6])-data[4]*(2*step*step-8*data[6])-data[5]*(4*data[6]*data[6]-4*data[6]*step*data[7]+step*step);
    if (abs(temp)<0.0000000001 || abs(temp2)<0.0000000001)
        data[3] = 0;
    else
        data[3] = temp2/temp;//(4*data[4]*data[4]*(data[1]-2*data[2]+data[3])+4*step*data[4]*1*(data[1]-data[3])+step*step*(data[1]+2*data[2]+data[3]));
    */

    data[1] = data[0];
    data[0] = n;
    data[4] = data[3];
    double Par = 2*data[6]/step;
    data[3]=(1/(1+Par))*((data[1]+data[0])-((1-Par)*data[4]));
}

void NoiseFilters::CalcKalman(double n, KalmanFilter *data)
{
    // предыдущие значения
    data->Z_now = n;
    data->X_prev= data->X_now;
    data->P_prev = data->P_now;

    //расчеты
    data->Z_mid = data->Z_now - data->X_prev;
    data->P_mid = data->P_prev + data->Q_now;
    data->K_now = data->P_mid/(data->P_mid+data->R_now);
    data->P_now = (1-data->K_now)*data->P_mid;
    //data->K_now = 0.5;
    data->X_now = data->K_now*data->Z_mid+data->X_prev;
}

void NoiseFilters::CalcXsns(double step, double x)
{
    CalcFNH(step,x, XsnsFNH);
    CalcKalman(x,XsnsKalman);
}

void NoiseFilters::CalcZsns(double step, double z)
{
    CalcFNH(step,z, ZsnsFNH);
    CalcKalman(z,ZsnsKalman);
}

void NoiseFilters::CalcVxLag(double step, double Vx)
{
    CalcFNH(step,Vx, VxlagFNH);
    CalcKalman(Vx,VxlagKalman);
}

void NoiseFilters::CalcVzLag(double step, double Vz)
{
    CalcFNH(step,Vz, VzlagFNH);
    CalcKalman(Vz,VzlagKalman);
}

void NoiseFilters::CalcPsiMagn(double step, double psi)
{
    CalcFNH(step,psi, PsimagnFNH);
    CalcKalman(psi,PsimagnKalman);
}

double NoiseFilters::GetXsnsFNH()
{
    return XsnsFNH[3];
}

double NoiseFilters::GetZsnsFNH()
{
    return ZsnsFNH[3];
}

double NoiseFilters::GetVxLagFNH()
{
    return VxlagFNH[3];
}

double NoiseFilters::GetVzLagFNH()
{
    return VzlagFNH[3];
}

double NoiseFilters::GetPsiMagnFNH()
{
    return PsimagnFNH[3];
}

double NoiseFilters::GetXsnsKalman()
{
    return XsnsKalman->X_now;
}

double NoiseFilters::GetZsnsKalman()
{
    return ZsnsKalman->X_now;
}

double NoiseFilters::GetVxLagKalman()
{
    return VxlagKalman->X_now;
}

double NoiseFilters::GetVzLagKalman()
{
    return VzlagKalman->X_now;
}

double NoiseFilters::GetPsiMagnKalman()
{
    return PsimagnKalman->X_now;
}

void NoiseFilters::SetXsnsFNH(double T)
{
    XsnsFNH[6] = T;
    qDebug()<<"T X now "<<XsnsFNH[6];
}

void NoiseFilters::SetZsnsFNH(double T)
{
    ZsnsFNH[6] = T;
}

void NoiseFilters::SetVxLagFNH(double T)
{
    VxlagFNH[6] = T;
}

void NoiseFilters::SetVzLagFNH(double T)
{
    VzlagFNH[6] = T;
}

void NoiseFilters::SetPsiMagnFNH(double T)
{
    PsimagnFNH[6] = T;
}
