#ifndef HEADINGFILTERS_H
#define HEADINGFILTERS_H

#include <QObject>
#include <QDebug>

#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>

#include "inverse.h"

using namespace boost::numeric::ublas;

struct Kalman3Matrix
{
    //перменнные матрицы для фильтра
    matrix<double> X_now; // матрица текущих оценок
    matrix<double> X_mid; // матрица на промежуточном шаге
    matrix<double> X_prev; // матрица предыдущих оценок
    matrix<double> P_now; // матрица Р на текущем шаге
    matrix<double> P_mid; // матрица Р на промежуточном шаге
    matrix<double> P_prev; // матрица Р на предыдущем шаге
    matrix<double> R_now; // матрица R
    matrix<double> F_now; // матрица F
    matrix<double> F_now_t; // матрица F транспонированная
    matrix<double> Z_now; // матрица Z
    matrix<double> Z_mid; // матрица Z
    matrix<double> Z_prev; // матрица Z
    matrix<double> PF_t;
    matrix<double> FPF_t;
    matrix<double> PH_t;
    matrix<double> HPH_t;
    matrix<double> HPHR_sum;
    matrix<double> HPHR_1;
    matrix<double> HR;
    matrix<double> K_now;
    matrix<double> KH;
    matrix<double> EKH;
    matrix<double> KZ;
    matrix<double> GQ;
    matrix<double> QG;

    // постоянные матрицы
    matrix<double> H_now; // матрица Н
    matrix<double> H_now_t; // транспонированная матрица H
    matrix<double> Q_now; // матрица Q
    matrix<double> E;
    matrix<double> G_now;
    matrix<double> G_now_t;
};

class HeadingFilters : public QObject
{
    Q_OBJECT
public:
    explicit HeadingFilters(QObject *parent = nullptr);

    // периоды дискретизации
    double fhPeriod = 1;
    double kfk3Period = 1;

private:
    Kalman3Matrix matrix3;

public slots:

    // установка коэффициентов из основной программы
    void SetR3(matrix<double> R_set);
    void SetQ3(matrix<double> Q_set);

    // расчет
    void Calc3(double psi, double dpsi);

    // получение результатов расчета
    double Get3psi();
    double Get3dpsi();
    double Get3ddpsi();

    // Переинициализация
    void Reinit3();

private slots:

    // задание размеров
    void SetSize3();
};

#endif // HEADINGFILTERS_H
