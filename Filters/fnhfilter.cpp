#include "fnhfilter.h"

FNHfilter::FNHfilter(QObject *parent) : QObject(parent)
{

}

void FNHfilter::filter(double Tau, double Td, double *Data, double *DataF, unsigned char reinit_filter)
{
    double Par;
    Par= 2*Tau/Td;
    DataF[0]=DataF[0]*1000;
    DataF[1]=DataF[1]*1000;
    Data[0]=Data[0]*1000;
    Data[1]=Data[1]*1000;

    if (reinit_filter)
    {
        DataF[0]=Data[0];
        DataF[1]=Data[0];
        Data[1]=Data[0];

    }
    else
    {
        DataF[0]=(1/(1+Par))*((Data[1]+Data[0])-((1-Par)*DataF[1]));
        Data[1]=Data[0]; DataF[1]=DataF[0];
    }
    DataF[0]=DataF[0]/1000;
    DataF[1]=DataF[1]/1000;
    Data[0]=Data[0]/1000;
    Data[1]=Data[1]/1000;
}
