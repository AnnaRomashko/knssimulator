#ifndef UDPCONNECTION_H
#define UDPCONNECTION_H

#include <QObject>
#include <QUdpSocket>
#include <QNetworkDatagram>
#include <QHostAddress>

#include <QJsonDocument>
#include <QJsonObject>
#include <QFile>
#include <QDir>

struct SendDataStruct
{
    double x = 0;
    double y = 0;
    double Vx = 0;
};

struct GetDataStruct
{
    double psi = 0;
    double x = 0;
    double y = 0;
    double dx = 0;
    double dy = 0;
    double Vpsi = 0;
};

class UDPConnection : public QObject
{
    Q_OBJECT
public:
    explicit UDPConnection(QObject *parent = nullptr);

private:
    QUdpSocket* socket;
    QNetworkDatagram datagrammGet;
    QNetworkDatagram datagrammSend;
    QHostAddress receiverAdress;
    QHostAddress senderAdress;
    int receiverPort;
    int senderPort;

signals:
    void getData(GetDataStruct data);

public slots:
   void makeSocket(QJsonObject obj);
   void sendData(SendDataStruct data);

};

#endif // UDPCONNECTION_H
