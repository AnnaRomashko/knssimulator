#include "udpconnection.h"

UDPConnection::UDPConnection(QObject *parent) : QObject(parent)
{
    socket = new QUdpSocket(this);
}

void UDPConnection::makeSocket(QJsonObject obj)
{
    if (obj.keys().contains("receiver"))
    {
        QJsonObject obj1 = obj["receiver"].toObject();
        receiverAdress = QHostAddress(obj1["ip"].toString());
        receiverPort = obj1["port"].toInt();

        datagrammSend.setSender(receiverAdress,receiverPort);
        socket->bind(receiverAdress,receiverPort);

        connect(socket,&QAbstractSocket::readyRead,[this]()
        {
           datagrammGet = socket->receiveDatagram();
           GetDataStruct dataGet;
           memcpy(&dataGet,datagrammGet.data(),sizeof(GetDataStruct));
           emit getData(dataGet);
        });
    }
    if (obj.keys().contains("sender"))
    {
        QJsonObject obj1 = obj["sender"].toObject();
        senderAdress = QHostAddress(obj1["ip"].toString());
        senderPort = obj1["port"].toInt();

        datagrammSend.setSender(receiverAdress,receiverPort);
        datagrammSend.setDestination(senderAdress,senderPort);
    }
}

void UDPConnection::sendData(SendDataStruct data)
{
    QByteArray array;
    array.resize(sizeof(SendDataStruct));
    array = QByteArray::fromRawData((char*)&data,sizeof(SendDataStruct));
    datagrammSend.setData(array);
    socket->writeDatagram(datagrammSend);
}
