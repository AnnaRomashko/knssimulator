﻿#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QWidget>
#include <QButtonGroup>
#include <QTimer>
#include <QFileDialog>
#include <math.h>
#include <random>

#include "Connection/udpconnection.h"
#include "Graphics/graphicwindow.h"
//#include "matrixoperations.h"
//#include "kalman.h"
#include "structs.h"

#include "Filters/noisefilters.h"
#include "Filters/headingfilters.h"
#include "SimpleContur/simplecontur.h"
#include "Kalman/perebor.h"
#include "Nabludatel/nabludatel8.h"

#include "./shared_includes/kx_protocol.h"
#include "./shared_includes/qkx_coeffs.h"

#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>

extern double X[2000][2];
extern QVector<double> K;

#define Kc 57.3
#define Rz 6371000
#define g 9.81
#define Wsh 0.00124

using namespace boost::numeric::ublas;

namespace Ui {
class MainWidget;
}

class MainWidget : public QWidget
{
    Q_OBJECT

public:
    explicit MainWidget(QWidget *parent = nullptr);
    ~MainWidget();

    x_protocol* X_Protocol;
    Qkx_coeffs* K_Protocol;

private slots:
    // точки заданной траектории
    void on_pushButtonAddPoint_clicked();
    //void on_pushButtonDeletePoint_clicked();

    // формирование рассчитанных траекторий
    void on_pushButtonAddTraek_clicked();

    // расчет траектории
    void calcTraek(); // расчет траектории
    void Control_system(); // контура управления
    //void BFS_DRK(); // работа ДРК
    void Vehicles_Model(); // модель аппарата
    void Runge(double step); // интегрирование

    // расчет ССП
    void SNSCalc(); // смена данных датчиков
    void BINSLagCalc(); // смена данных датчиков
    void MagnLagCalc(); // смена данных датчиков
    void BINSCalc(); // смена данных датчиков
    void GANSCalc(); // смена данных датчиков
    void SNSBINSCalc(double T); // смена данных датчиков
    void GANSBINSLagCalc(double T); // смена данных датчиков
    void GANSMagnLagCalc(double T); // смена данных датчиков
    void GANSBINSCalc(double T); // смена данных датчиков
    void SetGraphicSSPVisible(); // отображение графиков

    // работа с датчиками
    void ReadSensorsParams(); // чтение параметрв датчиков

    // очистка структур перед моделированием
    void ClearSensorsData();

    // для kx-пульт
    void K_receive();

    // Установка коэффициентов контура курса
    void on_pushButtonSetKonturs_clicked();
    
    // Отображение графиков датчиков
    void on_pushButtonBINSPsiGraphic_clicked(bool checked);
    void on_pushButtonBINSVxGraphic_clicked(bool checked);
    void on_pushButtonBINSVzGraphic_clicked(bool checked);
    void on_pushButtonMagnGraphic_clicked(bool checked);
    void on_pushButtonLagVxGraphic_clicked(bool checked);
    void on_pushButtonLagVzGraphic_clicked(bool checked);
    void on_pushButtonGANSXGraphic_clicked(bool checked);
    void on_pushButtonGANSZGraphic_clicked(bool checked);
    void on_pushButtonSNSXGraphic_clicked(bool checked);
    void on_pushButtonSNSZGraphic_clicked(bool checked);

    // Загрузка сохраненной траектории
    void on_pushButtonloadTraek_clicked();

    // Отображение графиков ССП
    void on_pushButtongraphicSSP_clicked(bool checked);
    void on_pushButtongraphicSSPX_clicked(bool checked);
    void on_pushButtongraphicSSPZ_clicked(bool checked);
    void on_pushButtongraphicSSPdX_clicked(bool checked);
    void on_pushButtongraphicSSPdZ_clicked(bool checked);
    void on_pushButtongraphicSSPVx_clicked(bool checked);
    void on_pushButtongraphicSSPVz_clicked(bool checked);
    void on_pushButtongraphicSSPdVx_clicked(bool checked);
    void on_pushButtongraphicSSPdVz_clicked(bool checked);

    void on_pushButtonFileFind_clicked();
    void on_pushButtonFileSelect_clicked();
    void on_pushButtonFileNablSelect_clicked();
    void on_pushButtonFileCalc_clicked();

private:
    Ui::MainWidget *ui;

    QButtonGroup groupPoint;

    QVector<PointStruct> pointsVec;
    int curPoint = 0;
    bool isGoing = false;

    QTimer* systTimer; // таймер дискретизации системы

    CalcStruct* calcData; // расчеты траектории
    KonturStruct kontrData; // параметры контура
    SensorsStruct sensorsData; // данные датчиков
    SSPStruct* sspData; // данные ССП
    NablStruct* nablData; // данные наблюдателей

    QMap<QString,TraekStruct> traekList; // харакетристики рассчитанных траекторий
    QString actualName; // текущая траектория
    int actual_num = 0;
    double Ttimer = 0.01; // период расчетов
    double MaxTime = 500; // максимальная длительность моделирования
    bool isUseKNS = false; // использовать ли данные КНС в системе управления
    int actualKNS = 0; // по какой КНС замыкать СУ

    // наблюдатели с коррекцией
    NabludatelC3* nablXSNSBINS;
    NabludatelC3* nablXGANSBINSLag;
    NabludatelC3* nablXGANSMagnLag;
    NabludatelC3* nablXGANSBINS;

    NabludatelC3* nablZSNSBINS;
    NabludatelC3* nablZGANSBINSLag;
    NabludatelC3* nablZGANSMagnLag;
    NabludatelC3* nablZGANSBINS;

    // графики датчиков
    GraphicWindow* graphicPsiBINS;
    GraphicWindow* graphicPsiMagn;
    GraphicWindow* graphicVxBINS;
    GraphicWindow* graphicVzBINS;
    GraphicWindow* graphicVxLag;
    GraphicWindow* graphicVzLag;
    GraphicWindow* graphicXSNS;
    GraphicWindow* graphicZSNS;
    GraphicWindow* graphicXGANS;
    GraphicWindow* graphicZGANS;

    // графики КНС
    GraphicWindow* graphicXZ;
    GraphicWindow* graphicX;
    GraphicWindow* graphicZ;
    GraphicWindow* graphicdX;
    GraphicWindow* graphicdZ;
    GraphicWindow* graphicVx;
    GraphicWindow* graphicVz;
    GraphicWindow* graphicdVx;
    GraphicWindow* graphicdVz;

    // отдельно по контуру марша
    SimpleContur* simpleContir;
    Perebor* perebor;

    //название файла для обработки
    QString fileName;
    int numPx = -1;
    int numPz = -1;
    int numVpx = -1;
    int numVpz = -1;
    Nabludatel8* nablXResult;
    Nabludatel8* nablZResult;
    GraphicWindow* graphicdVp;
};

#endif // MAINWIDGET_H
