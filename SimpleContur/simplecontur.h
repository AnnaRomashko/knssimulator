#ifndef SIMPLECONTUR_H
#define SIMPLECONTUR_H

#include <QObject>

#include "Nabludatel/nabludatelc0.h"
#include "Nabludatel/nabludatelc1.h"
#include "Nabludatel/nabludatelc2.h"
#include "Nabludatel/nabludatelc3.h"
#include "Graphics/graphicwindow.h"

struct SimpleConturStruct
{
    // расчет траектории

    // координаты заданной точки траектории
    double x_dist = 0;

    // координаты текущей точки траектории
    double x_now = 0;

    // координаты в обратной связиъ
    double x_sens = 0;

    // характеристики аппарата
    double m = 252; // масса
    double L11 = 10; // присоединенная масса по оси X

    double Cx1 = 43.0; // гидродинамический коэффициент вдоль оси X
    double Cx2 = 4.3; // гидродинамический коэффициент вдоль оси X

    double Kdv = 20; // коэффициент усиления движителя
    double Tdv = 0.15; // постоянная времени движителя
    double Umax = 10; // максимальное напряжение движителя

    // рассчитанные коэффициенты
    double Kdv_x = 40; // коэффициент усиления ДРК контура марша

    // текущие переменные расчета
    double Ux = 0; // задающее напряжение на ДРК контура марша

    double Vx = 0; // маршевая скорость
    double dVx = 0; // ускорение к контуре марша

    double Fdx = 0; // упор ДРК в контуре марша
    double dFdx = 0; // производная упора ДРК в контуре марша

    double dx = 0; // производная координаты X

    double K1 = 34.5; // коэффициен контура марша
    double K2 = 22; // коэффициент контура марша
};


class SimpleContur : public QObject
{
    Q_OBJECT
public:
    explicit SimpleContur(QObject *parent = nullptr);

private:

    SimpleConturStruct contur;
    NabludatelC3 nabl;

    // графики датчиков
    GraphicWindow* graphicX;
    GraphicWindow* graphicVx;
    GraphicWindow* graphicdX;
    GraphicWindow* graphicdVx;
    GraphicWindow* graphicdVx0;
    GraphicWindow* graphicdVx1;

public:

    double Ttimer = 0.01; // период всех расчетов
    double periodCalc = 0.01; // период расчета контура
    double endX = 100; // время расчета
    double periodSensX = 0.01; // период получения данных с датчика координат
    double periodSensVx = 0.01; // период получения данных с датчика скорости
    double periodNabl = 0.01; // период расчета наблюдаьеля
    double periodSensX2 = 20; // период получения данных с датчика координат во время коррекции
    double graphPeriod = 0.05; // период вывода графиков
    double time_change = 50; // период перехода в режим коррекции
    double NK1 = 2.04; // коэффициент наблюдателя
    double NK2 = 1.2; // коэффициент наблюдателя
    double NK3 = 0.31; // коэффициент наблюдателя

    double Vx_sens = 0; // скорость по датчикам
    double dVx0= 0.1; // постоянная ошибка скорости
    double dVx1 = 0.01; // линейная ошибка
    double dVx2 = 0.01; // квадратичная ошибка
    double m = 0.5; // масштабный коэффициент
    double dVxr = 0; // слуайная ошибка

    double x_sens = 0; // координата по датчикам
    double x_sens_1 = 0; // запаздывание на шаг
    double dx0 = 10; // постоянная ошибка
    double dxr = 0; // случайная ошибка

    double x_nabl = 0; // координата по наблюдателю
    double Vx_nabl = 0; // скорость по наблюдателю
    double dVx0_nabl = 0; // постоянная ошибка по наблюдателю
    double dVx1_nabl = 0; // линейная ошибка по наблюдателю

public slots:

    void Calc();

    double GetX();
    double GetVx();

private slots:

    void Runge(double step);
    void Func();
    void Rull();

signals:

    void newData(double idealP, double newP, double newVp);
    void endCalc();

};

#endif // SIMPLECONTUR_H
