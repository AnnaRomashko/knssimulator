#include "simplecontur.h"
#include <math.h>

SimpleContur::SimpleContur(QObject *parent) : QObject(parent)
{
    contur.x_dist = 300;
    //nabl.SetK(NK1,NK2,NK3);

    // графики для отображения датчиков
    graphicX = new GraphicWindow("X", "Время, с","",0);
    graphicdX = new GraphicWindow("dX", "Время, с","",0);
    graphicVx = new GraphicWindow("Vx", "Время, с","",0);
    graphicdVx = new GraphicWindow("dVx", "Время, с","",0);
    graphicdVx0 = new GraphicWindow("dVx0", "Время, с","",0);
    graphicdVx1 = new GraphicWindow("dVx1", "Время, с","",0);

    // названия файлов графиков
    graphicX->SetFileName("X");
    graphicdX->SetFileName("dX");
    graphicVx->SetFileName("Vx");
    graphicdVx->SetFileName("dVx");
    graphicdVx0->SetFileName("dVx0");
    graphicdVx1->SetFileName("dVx1");

    //ввод данных на график
    graphicX->addData("ideal", true);
    graphicX->addData("sens", true);
    graphicX->addData("nabl", true);
    graphicdX->addData("sens", true);
    graphicdX->addData("nabl", true);
    graphicVx->addData("ideal", true);
    graphicVx->addData("sens", true);
    graphicVx->addData("nabl", true);
    graphicdVx->addData("sens", true);
    /*graphicdVx->addData("nabl", true);
    graphicdVx->addData("filt", true);*/
    graphicdVx->addData("nablVx2", true);
    graphicdVx->addData("filtVx2", true);
    graphicdVx->addData("nabldVx2", true);
    graphicdVx->addData("filtdVx2", true);
    graphicdVx0->addData("sens", true);
    graphicdVx0->addData("nablVx0", true);
    graphicdVx0->addData("filtVx0", true);
    graphicdVx1->addData("nablVx1", true);
    graphicdVx1->addData("filtVx1", true);
    graphicdVx1->addData("nabldVx1", true);
    graphicdVx1->addData("filtdVx1", true);

    // отображение графиков
    graphicX->show();
    graphicdX->show();
    graphicVx->show();
    graphicdVx->show();
    graphicdVx0->show();
    graphicdVx1->show();

    Calc();
}

void SimpleContur::Calc()
{
    // шаг расчета траектории
    double T=0; // время моделирования
    bool not_end = true; // флаг окончания моделирования

    // определение счетчиков для периодов выдачи данных
    // СУ
    if (periodCalc<Ttimer)
        periodCalc = 1;
    else
    {
        periodCalc = periodCalc/Ttimer;
    }
    // датчик координаты X
    if (periodSensX<Ttimer)
        periodSensX = 1;
    else
        periodSensX = periodSensX/Ttimer;
    // датчик скоростии Vx
    if (periodSensVx<Ttimer)
        periodSensVx = 1;
    else
        periodSensVx = periodSensVx/Ttimer;
    // наблюдатель
    if (periodNabl<Ttimer)
        periodNabl = 1;
    else
        periodNabl = periodNabl/Ttimer;
    // отображение на графиках
    if (graphPeriod<Ttimer)
        graphPeriod = 1;
    else
        graphPeriod = graphPeriod/Ttimer;

    // счетчики для обновления данных
    int systCount = 0; // СУ
    int xCount = 0; // датчик координаты X
    int VxCount = 0; // датчик скорости Vx
    int NablCount = 0; // наблюдатель
    int graphCount = 0; // отображение графиков

    qDebug()<<"stop";

    // цикл расчетов
    while (not_end)
    {
        if (systCount == periodCalc) // период дискретизации системы
        {
            Rull();
            systCount = 0;
        }
        if (xCount == periodSensX) // период датчика координаты X
        {
            dxr = 1*sin(10*T);
            x_sens = x_sens_1;
            x_sens_1 = contur.x_now;// + dx0 + dxr;
            if (T>time_change)
            {
                nabl.Correct(x_sens);
                //qDebug()<<"correct "<<T;
            }
            xCount = 0;
        }
        if (VxCount == periodSensVx) // период датчиа скорости Vx
        {
            dVxr = 0.1*sin(10*T);
            Vx_sens = contur.Vx + dVx0+ dVx1*T;// + dVx2*T*T;// + m*contur.Vx + dVxr;
            emit newData(contur.x_now, x_sens, Vx_sens);
            VxCount = 0;
        }
        if (NablCount == periodNabl) // период наблюдателя
        {
            nabl.Calc(periodNabl*Ttimer,x_sens,Vx_sens);
            NablCount = 0;
        }
        if (fabs(T-time_change)<Ttimer*0.9) // переключение в режим коррекции
        {
            nabl.SetCorrect();
            periodSensX = periodSensX2/Ttimer;
            //qDebug()<<"new period x_sens "<<periodSensX<<"delta "<<fabs(T-time_change);
            xCount = 0;
        }
        Runge(Ttimer); // интегрирование производим на каждом шаге
        T+=Ttimer; //увеличиваем время
        if (graphCount == graphPeriod) // период добавления точек в отображение траектории
        {
            graphicX->addPoint("ideal", T, contur.x_now);
            graphicX->addPoint("sens", T, x_sens);
            graphicX->addPoint("nabl", T, nabl.GetP());
            graphicdX->addPoint("sens", T, contur.x_now-x_sens);
            graphicdX->addPoint("nabl", T, nabl.GetP()-x_sens);
            graphicVx->addPoint("ideal", T, contur.Vx);
            graphicVx->addPoint("sens", T, Vx_sens);
            graphicVx->addPoint("nabl", T, nabl.GetVp());
            graphicdVx->addPoint("sens", T, contur.Vx-Vx_sens);
            //graphicdVx->addPoint("nabl", T, nabl.GetVp0());
            //graphicdVx->addPoint("filt",T,nabl.GetVp0f());
            //graphicdVx1->addPoint("nabl", T, nabl.GetdVp0());
            //graphicdVx1->addPoint("filt",T,nabl.GetdVp0f());
            graphicdVx->addPoint("nablVx2", T, -nabl.GetVp2());
            graphicdVx->addPoint("filtVx2", T, -nabl.GetVp2f());
            graphicdVx->addPoint("nabldVx2", T, -nabl.GetdVp2());
            graphicdVx->addPoint("filtdVx2", T, -nabl.GetdVp2f());
            graphicdVx0->addPoint("nablVx0", T, nabl.GetVp0());
            graphicdVx0->addPoint("filtVx0", T, nabl.GetVp0f());
            graphicdVx1->addPoint("nablVx1", T, nabl.GetVp1());
            graphicdVx1->addPoint("filtVx1", T, nabl.GetVp1f());
            graphicdVx1->addPoint("nabldVx1", T, nabl.GetdVp1());
            graphicdVx1->addPoint("filtdVx1", T, nabl.GetdVp1f());
            graphCount = 0;

        }
        //qDebug()<<"T "<<T <<"time_change "<<time_change;//<<" L_dist "<<calcData->L_dist<<"x_dis "<<calcData->x_dist<<" x_sens "<<calcData->x_sens; //<<" x "<<"X Kalman2 "<<kalman->getKalman2_X()<<" X SSP "<<sspData->xBINSLag;
        if (T>endX)
        {
            not_end = false;
            emit endCalc();
        }

        // увеличиваем счетчики периодов
        systCount++;
        xCount++;
        VxCount++;
        graphCount++;
        NablCount++;

        // для обновления отображения на экране
        QApplication::processEvents();
    }
}

void SimpleContur::Func()
{
    // модель аппарата

    // контур марша
    contur.dVx = (contur.Fdx-contur.Cx1*contur.Vx*fabs(contur.Vx)-contur.Cx2*contur.Vx)/(contur.L11+contur.m);
    contur.dx = contur.Vx;

    // ДРК
    contur.dFdx = (contur.Kdv_x*contur.Ux-contur.Fdx)/contur.Tdv;
}

void SimpleContur::Rull()
{
    // контура управления
    contur.Ux = ((contur.x_dist-contur.x_now))*contur.K1-contur.Vx*contur.K2;
    // учитываем ограничение напряжения
    if (contur.Ux>10)
        contur.Ux = 10;
    else if (contur.Ux<-10)
        contur.Ux = -10;
}

void SimpleContur::Runge(double step)
{
    // создаем переменные для метода Рунге-Кутта 4-го порядка
    SimpleConturStruct* tempCalc = new SimpleConturStruct();

    // по лекциям
    Func();

    tempCalc->x_now = contur.x_now;
    tempCalc->Vx = contur.Vx;
    tempCalc->Fdx = contur.Fdx;

    tempCalc->dx = contur.dx;
    tempCalc->dVx = contur.dVx;
    tempCalc->dFdx = contur.dFdx;

    contur.x_now = tempCalc->x_now+0.5*step*contur.dx;
    contur.Vx = tempCalc->Vx+0.5*step*contur.dVx;
    contur.Fdx = tempCalc->Fdx+0.5*step*contur.dFdx;

    // второй подход
    Func();

    tempCalc->dx = tempCalc->dx + 2.0*contur.dx;
    tempCalc->dVx = tempCalc->dVx + 2.0*contur.dVx;
    tempCalc->dFdx = tempCalc->dFdx + 2.0*contur.dFdx;

    contur.x_now = tempCalc->x_now+0.5*step*contur.dx;
    contur.Vx = tempCalc->Vx+0.5*step*contur.dVx;
    contur.Fdx = tempCalc->Fdx+0.5*step*contur.dFdx;

    // третий подход
    Func();

    tempCalc->dx = tempCalc->dx + 2.0*contur.dx;
    tempCalc->dVx = tempCalc->dVx + 2.0*contur.dVx;
    tempCalc->dFdx = tempCalc->dFdx + 2.0*contur.dFdx;

    contur.x_now = tempCalc->x_now+step*contur.dx;
    contur.Vx = tempCalc->Vx+step*contur.dVx;
    contur.Fdx = tempCalc->Fdx+step*contur.dFdx;

    // четвертый подход
    Func();

    contur.x_now = tempCalc->x_now+(step/6.0)*(tempCalc->dx + contur.dx);
    contur.Vx = tempCalc->Vx+(step/6.0)*(tempCalc->dVx + contur.dVx);
    contur.Fdx = tempCalc->Fdx+(step/6.0)*(tempCalc->dFdx + contur.dFdx);
}

double SimpleContur::GetVx()
{
    return contur.dx;
}

double SimpleContur::GetX()
{
    return contur.x_now;
}
