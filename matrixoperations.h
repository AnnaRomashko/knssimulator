#ifndef MATRIXOPERATIONS_H
#define MATRIXOPERATIONS_H

#include <QObject>
#include <QDebug>

class MatrixOperations : public QObject
{
    Q_OBJECT
public:
    explicit MatrixOperations(QObject *parent = nullptr);

signals:

public slots:

    void matrix_sum(int strok, int stolb, double** matr1, double** matr2, double** matr_res); // сложение матриц
    void matrix_subt(int strok, int stolb, double** matr_from, double** matr_who, double** matr_res); // вычитание матриц
    void matrix_mult(int strok_res, int stolb_res, int stolb_1, double** matr1, double** matr2, double** matr_res); // умножение матриц
    void matrix_div(int strok_res, int stolb_res, double** matr_from, double** matr_who, double** matr_res); // деление матриц

    void matrix_trans(int strok_res, int stolb_res, double** matr_sour, double** matr_res); // транспонирование матрицы
    void matrix_back(int strok_res, int stolb_res, double** matr_sour, double** matr_res); // обратная матрица
    double matrix_minor(int strok, int stolb, double **matr); // минор
};

#endif // MATRIXOPERATIONS_H
