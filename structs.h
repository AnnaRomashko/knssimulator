#ifndef STRUCTS_H
#define STRUCTS_H

struct PointStruct
{
    // точки траеткори
    double x = 0;
    double y = 0;
    double r = 0;
};

struct SSPStruct
{
    // ССП по БИНС и ГДЛ
    double xBINSLag = 0;
    double zBINSLag = 0;
    double vxBINSLag = 0;
    double vzBINSLag = 0;

    double dxBINSLag = 0;
    double dzBINSLag = 0;
    double dVxBINSLag = 0;
    double dVzBINSLag = 0;

    double periodBINSLag = 0.01;

    // ССП по Магнитометру и ГДЛ
    double xMagnLag = 0;
    double zMagnLag = 0;
    double vxMagnLag = 0;
    double vzMagnLag = 0;

    double dxMagnLag = 0;
    double dzMagnLag = 0;
    double dVxMagnLag = 0;
    double dVzMagnLag = 0;

    double periodMagnLag = 0.01;

    // ССП по БИНС
    double xBINS = 0;
    double zBINS = 0;
    double vxBINS = 0;
    double vzBINS = 0;

    double dxBINS = 0;
    double dzBINS = 0;
    double dVxBINS = 0;
    double dVzBINS = 0;

    double periodBINS = 0.01;

    // ССП по ГАНС
    double xGANS = 0;
    double zGANS = 0;

    double dxGANS = 0;
    double dzGANS = 0;

    double periodGANS = 0.01;

    // ССП по СНС
    double xSNS = 0;
    double zSNS = 0;

    double dxSNS = 0;
    double dzSNS = 0;

    double periodSNS = 0.01;
};

struct CalcStruct
{
    // расчет траектории

    // координаты заданной точки траектории
    double x_dist = 0;
    double z_dist = 0;

    // координаты текущей точки траектории
    double x_now = 0;
    double z_now = 0;

    // координаты текущей точки траектории в связанных осях для ГАНС
    double x_gans = 0;
    double z_gans = 0;

    // координаты в обратной связиъ
    double x_sens = 0;
    double z_sens = 0;

    double x_kns = 0;
    double z_kns = 0;

    // скорости в обратной связи
    double dx_sens = 0;
    double dz_sens = 0;

    double dx_kns = 0;
    double dz_kns = 0;

    double Vx_sens = 0;
    double Vz_sens = 0;

    // заданный угол курса
    double psi_dist[2] = {0,0};

    // заданное расстояние
    double L_dist = 0;

    // текущий угол курса
    double psi_now = 0;

    // счетчик оборотов для абсолютного угла
    int count_psi = 0;

    // характеристики аппарата
    double m = 252; // масса
    double L11 = 10; // присоединенная масса по оси X
    double Iy = 258; // момент инерции вдоль оси Y
    double L55 = 260; // присоединенный момент инерции вдоль оси Y

    double Cx1 = 43.0; // гидродинамический коэффициент вдоль оси X
    double Cx2 = 4.3; // гидродинамический коэффициент вдоль оси X
    double Cwy1 = 185; // гидродинамический коэффициент вдоль оси Y
    double Cwy2 = 18.5; // гидродинамический коэффициент вдоль оси Y

    double Kdv = 20; // коэффициент усиления движителя
    double Tdv = 0.15; // постоянная времени движителя
    double Umax = 50; // максимальное напряжение движителя
    double a = 0.3; // расстояние между движителями

    // рассчитанные коэффициенты
    double Kdv_psi = 12; // коэффициент усиления ДРК контура курса
    double Kdv_x = 40; // коэффициент усиления ДРК контура марша

    // текущие переменные расчета
    double Upsi = 0; // задающее напряжение на ДРК контура курса
    double Ux = 0; // задающее напряжение на ДРК контура марша

    double wy = 0; // угловая скорость в контуре курса
    double Vx = 0; // маршевая скорость
    double dwy = 0; // угловое ускорение в контуре курса
    double dpsi_now = 0; // угловая скорость в контуре курса
    double dVx = 0; // ускорение к контуре марша
    double dpsi = 0; // производная угла курса для интегрирования

    double Fdx = 0; // упор ДРК в контуре марша
    double Mdy = 0; // упор ДРК в контуер курса
    double dFdx = 0; // производная упора ДРК в контуре марша
    double dMdy = 0; // производная упора ДРК в контуер курса

    double dx = 0; // производная координаты X
    double dz = 0; // производная координаты Z

    // ПИ-регуляторы
    double psi_pi = 0; // посел ПИ-регулятора
    double dpsi_pi = 0; // на входе ПИ-регулятора
    double Ktpsi = 0.01; // коэффициент ПИ-регулятора

    double xz_pi = 0; // после ПИ-регулятора
    double L_pi = 0; // не помню
    double dxz_pi = 0; // на входе ПИ-регулятора
    double Ktxz = 0.1; // коэффициент ПИ-регулятора
};

struct KonturStruct
{
    // параметры расчета
    double K1 = 3.9; // коэффициент контура курса
    double K2 = 2.5; // коэффициент контура курса
    double K3 = 34.5; // коэффициент контура марша
    double K4 = 22; // коэффициент контура марша

    double systPeriod = 0.01; // период расчета системы
    double xPeriod = 0.01; // период обновления данных датчика координаты X
    double zPeriod = 0.01; // период обновления данных датчика координаты Z
};

struct TraekStruct
{
    // характеристики траектории
    double Vx = 0; // течение вдоль оси X
    double Vz = 0; // течение вдоль оси Z
    double dX = 0; // постоянная ошибка датчика кооординаты X
    double dZ = 0; // постоянная ошибка датчика координаты Z
    double dVx = 0; // растущая со врмененем ошибка датчика кооординаты X
    double dVz = 0; // растущая со врмененем ошибка датчика координаты Z
    double xPrev = 0; // показания датчика координат на предыдущем такте
    double zPrev = 0; // показания датчика координат на предыдущем такте
    bool xzP = false; // наличие задержки на такт в показаниях датчиков координат
    double Ax = 0; // амплитуда случайной ошибки
    double Az = 0; // амплитуда случайной ошибки
    double wx = 0; // частота случайной ошибки
    double wz = 0; // частота случайной ошибки
    double R = 0; // радиус точек

    // для расчетов
    double Vx_t = 0; // результат интегрирования растущей со временем ошибки датчика координаты X
    double Vz_t = 0; // результат интегрирования растущей со временем ошибки датчика координаты Z
};

struct SensorsStruct
{
    // структрура датчиков

    // угол курса БИНС
    double psiBINS = 0; // результирующий угол курса БИНС
    double psi0BINS = 0; // постоянная ошибка угла курса
    double wyBINS = 0; // результирующая угловая скорость БИНС
    double DwyBINS = 0; // постоянная ошибка угловой скорости
    double DwyBINS_t = 0; // результат интегрирования угловой скорости БИНС
    double psiBINSPeriod = 0.01; // период выдачи данных
    double psiPrevBINS = 0; // показания БИНС на предыдущем такте выдачи данных
    bool psiPBINS = false; // наличие задержки на такт в БИНС

    // угол курса магинтометра
    double psiMagn = 0; // результирующий угол курса Магнитометра
    double psi0Magn = 0; // постоянняа ошибка угла курса Магнитометра от полукруговой девиации
    double psiAMagn = 0; // амплитуда шумовой составляющей ошибки Магнитометра
    double psiWMagn = 0; // частота шумовой составляющей ошибки Магнитометра
    double psiMagnPeriod = 0.01; // период выдачи данных с Магнитометра
    double psiPrevMagn = 0; // показания Магнитометра на предыдущем такте выдачи данных
    bool psiPMagn = false; // наличие задержки на такт в Магнитометре

    // северная скорость БИНС
    double VxBINS = 0; // результирующая северная скорость БИНС
    double Vx0BINS = 0; // постоянная ошибка северной скорости БИНС
    double AxBINS = 0; // линейно растущая со временем ошибка скорости БИНС
    double AxBINS_t = 0; // результат интегрирования линейно растущей со временем ошибки скорсти БИНС
    double AAxBINS = 0; // квадратично растущая со врменем ошибка скорости БИНС
    double AAxBINS_t = 0; // результат интегрирования квадратично растущей со врменем ошибки скорсти БИНС
    double AAxBINS_tt = 0; // результат интегрирования квадратично растущей со врменем ошибки скорсти БИНС
    double VxBINSPeriod = 0.01; // период выдачи данных с БИНС
    double VxPrevBINS = 0; // показания БИНС на предыдущем такте выдачи данных
    bool VxPBINS = false; // наличие задержки на такт в БИНС

    // восточная скорость БИНС
    double VzBINS = 0; // результирующая восточная скорость БИНС
    double Vz0BINS = 0; // постоянная ошибка восточной скорости БИНС
    double AzBINS = 0; // линейно растущая со временем ошибка скорости БИНС
    double AzBINS_t = 0; // результат интегрирования линейно растущей со временем ошибки скорсти БИНС
    double AAzBINS = 0; // квадратично растущая со врменем ошибка скорости БИНС
    double AAzBINS_t = 0; // результат интегрирования квадратично растущей со врменем ошибки скорсти БИНС
    double AAzBINS_tt = 0; // результат интегрирования квадратично растущей со врменем ошибки скорсти БИНС
    double VzBINSPeriod = 0.01; // период выдачи данных с БИНС
    double VzPrevBINS = 0; // показания БИНС на предыдущем такте выдачи данных
    bool VzPBINS = false; // наличие задержки на такт в БИНС

    // продольная скорость ГДЛ
    double VxLag = 0; // результирующая продольная скорость ГДЛ
    double mxLag = 0; // масштабный коэффициент ГДЛ
    double Vx0Lag = 0; // постоянная ошибка ГДЛ
    double VxALag = 0; // амплитуда случайной ошибки ГДЛ
    double VxWLag = 0; // частота случайной ошибки ГДЛ
    double VxLagPeriod = 0.01; // период выдачи данных ГДЛ
    double VxPrevLag = 0; // показания ГДЛ на предыдущем такте выдачи данных
    bool VxPLag = false; // наличие задержки на такт в БИНС

    // поперечная скорость ГДЛ
    double VzLag = 0; // результирующая поперечная скорость ГДЛ
    double mzLag = 0; // масштабный коэффициент ГДЛ
    double Vz0Lag = 0; // постоянная ошибка ГДЛ
    double VzALag = 0; // амплитуда случайной ошибки ГДЛ
    double VzWLag = 0; // частота случайной ошибки ГДЛ
    double VzLagPeriod = 0.01; // период выдачи данных ГДЛ
    double VzPrevLag = 0; // показания ГДЛ на предыдущем такте выдачи данных
    bool VzPLag = false; // наличие задержки на такт в БИНС


    // X СНС
    double xSNS = 0; // северная координата СНС
    double x0SNS = 0; // постоянная ошибка СНС
    double xASNS = 0; // амплитуда шумовой ошибки СНС
    double xWSNS = 0; // частота шумовой ошибки СНС
    double xSNSPeriod = 1; // период выдачи данных СНС
    double xPrevSNS = 0; // показания СНС на предыдущем такте выдачи данных
    bool xPSNS = false; // наличие задержки на такт в СНС

    // Z СНС
    double zSNS = 0; // восточная координата СНС
    double z0SNS = 0; // постоянная ошибка СНС
    double zASNS = 0; // амплитуда шумовой ошибки СНС
    double zWSNS = 0; // частота шумовой ошибки СНС
    double zSNSPeriod = 1; // период выдачи данных СНС
    double zPrevSNS = 0; // показания СНС на предыдущем такте выдачи данных
    bool zPSNS = false; // наличие задержки на такт в СНС

    // X ГАНС
    double xGANS = 0; // северная координата ГАНС
    double x0GANS = 0; // постоянная ошибка ГАНС
    double xAGANS = 0; // амплитуда шумовой ошибки ГАНС
    double xWGANS = 0; // частота шумовой ошибки ГАНС
    double xGANSPeriod = 5; // период выдачи данных ГАНС
    double xPrevGANS = 0; // показания ГАНС на предыдущем такте выдачи данных
    bool xPGANS = false; // наличие задержки на такт в ГАНС

    // Z ГАНС
    double zGANS = 0; // восточная координата ГАНС
    double z0GANS = 0; // постоянная ошибка ГАНС
    double zAGANS = 0; // амплитуда шумовой ошибки ГАНС
    double zWGANS = 0; // частота шумовой ошибки ГАНС
    double zGANSPeriod = 5; // период выдачи данных ГАНС
    double zPrevGANS = 0; // показания ГАНС на предыдущем такте выдачи данных
    bool zPGANS = false; // наличие задержки на такт в ГАНС
};

struct NablStruct
{
    // структура расчетов по наблюдателям

    // по СНС и БИНС
    double xSNSBINS = 0;
    double zSNSBINS = 0;
    double vxSNSBINS = 0;
    double vzSNSBINS = 0;

    double dxSNSBINS = 0;
    double dzSNSBINS = 0;
    double dVxSNSBINS = 0;
    double dVzSNSBINS = 0;

    double periodSNSBINS = 0.01;
    double timeSNSBINS = 30;
    double period2SNSBINS = 5;
    double correctSNSBINSCount = 0;

    // по ГАНС и БИНС с ГДЛ
    double xGANSBINSLag = 0;
    double zGANSBINSLag = 0;
    double vxGANSBINSLag = 0;
    double vzGANSBINSLag = 0;

    double dxGANSBINSLag = 0;
    double dzGANSBINSLag = 0;
    double dVxGANSBINSLag = 0;
    double dVzGANSBINSLag = 0;

    double periodGANSBINSLag = 0.01;
    double timeGANSBINSLag = 30;
    double period2GANSBINSLag = 5;
    double correctGANSBINSLagCount = 0;

    // по ГАНС и Магнитометру с ГДЛ
    double xGANSMagnLag = 0;
    double zGANSMagnLag = 0;
    double vxGANSMagnLag = 0;
    double vzGANSMagnLag = 0;

    double dxGANSMagnLag = 0;
    double dzGANSMagnLag = 0;
    double dVxGANSMagnLag = 0;
    double dVzGANSMagnLag = 0;

    double periodGANSMagnLag = 0.01;
    double timeGANSMagnLag = 30;
    double period2GANSMagnLag = 5;
    double correctGANSMagnLagCount = 0;

    // по ГАНС и БИНС
    double xGANSBINS = 0;
    double zGANSBINS = 0;
    double vxGANSBINS = 0;
    double vzGANSBINS = 0;

    double dxGANSBINS = 0;
    double dzGANSBINS = 0;
    double dVxGANSBINS = 0;
    double dVzGANSBINS = 0;

    double periodGANSBINS = 0.01;
    double timeGANSBINS = 30;
    double period2GANSBINS = 5;
    double correctGANSBINSCount = 0;
};

#endif // STRUCTS_H
