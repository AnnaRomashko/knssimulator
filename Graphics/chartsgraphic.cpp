#include "chartsgraphic.h"

ChartsGraphic::ChartsGraphic(QString title, QString Xname, QString Yname, unsigned int time, QFont font, QWidget *parent) : QChartView(parent)
{
    // для форматирования
    sampleFont = font;

    // для масштабирования и перемещения встроенными функциями кнопками и мышью
    setRubberBand(QChartView::RectangleRubberBand);

    // для постановки на форме
    this->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

    // Создание представления графика
    graphic = new QChart();
    graphic->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    graphic->setFont(sampleFont);
    this->setChart(graphic);

    // Настройка заголовка
    graphic->setTitle(title);
    graphic->setTitleFont(sampleFont);

    // Настройка легенды
    graphic->legend()->setVisible(true);
    graphic->legend()->setFont(sampleFont);

    // Настройка осей
    axX = new QValueAxis(this);
    axX->setTitleText(Xname);
    axX->setTitleFont(sampleFont);
    axX->setLabelsFont(sampleFont);
    //axX->setTickInterval(5);
    axX->setTickCount(10);
    axX->setMax(maxX);
    axX->setMin(minX);

    axY = new QValueAxis(this);
    axY->setTitleText(Yname);
    axY->setTitleFont(sampleFont);
    axY->setLabelsFont(sampleFont);
    //axY->setTickInterval(20);
    axY->setTickCount(7);
    axY->setMax(maxY);
    axY->setMin(minY);

    // Установка количества допустимых точек
    points = time;

    // для сохранения графика как картинки
    this->setRenderHint(QPainter::Antialiasing);
    graphic->setAnimationOptions(QChart::NoAnimation);
    //graphic->SaveImage(sfd.FileName, System.Windows.Forms.DataVisualization.Charting.ChartImageFormat.Png);
    graphic->setMargins(QMargins(0,0,0,0));
}

ChartsGraphic::ChartsGraphic(QWidget *parent) : QChartView(parent)
{
    // для масштабирования и перемещения встроенными функциями кнопками и мышью
    setRubberBand(QChartView::RectangleRubberBand);

    // для постановки на форме
    this->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

    // Создание представления графика
    graphic = new QChart();
    graphic->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    //graphic->setFont(sampleFont);
    this->setChart(graphic);

    // создание стиля
    //sampleFont.fromString("DejaVu Sans");
    sampleFont.setPointSizeF(18);
    qDebug()<<"font point size "<<sampleFont.pointSize();

    // Настройка заголовка
    graphic->setTitle("");
    graphic->setTitleFont(sampleFont);

    // Настройка легенды
    graphic->legend()->setVisible(true);
    graphic->legend()->setFont(sampleFont);

    // Настройка осей
    axX = new QValueAxis(this);
    axX->setTitleText("Время, с");
    axX->setTitleFont(sampleFont);
    axX->setLabelsFont(sampleFont);
    //axX->setTickInterval(5);
    //axX->setTickCount(10);
    //axX->setMax(maxX);
    //axX->setMin(minX);

    axY = new QValueAxis(this);
    axY->setTitleText("");
    axY->setTitleFont(sampleFont);
    axY->setLabelsFont(sampleFont);
    //axY->setTickInterval(20);
    //axY->setTickCount(7);
    //axY->setMax(maxY);
    //axY->setMin(minY);

    // Установка количества допустимых точек
    points = 0;
}

ChartsGraphic::~ChartsGraphic()
{

}

void ChartsGraphic::setGraphicName(QString name)
{
    graphic->setTitle(name);
}

void ChartsGraphic::setDataName(QString new_name, QString name)
{
    if (allLine.contains(name))
    {
        allLine[new_name] = allLine[name];
        allLine.remove(name);
        allLine[new_name]->setName(new_name);
    }
    else if (allScatter.contains(name))
    {
        allScatter[new_name] = allScatter[name];
        allScatter.remove(name);
        allScatter[new_name]->setName(new_name);
    }
}

void ChartsGraphic::clearGraphic()
{
    QList<QString> listGraphic = allLine.keys();
    for (auto i:listGraphic)
    {
        allLine[i]->clear();
    }
    listGraphic = allScatter.keys();
    for (auto i:listGraphic)
    {
        allScatter[i]->clear();
    }

    axX->setMax(10);
    axX->setMin(0);
    axY->setMax(10);
    axY->setMin(0);
}

void ChartsGraphic::deletePoint(QString name, double x, double y)
{
    if (allLine.contains(name))
    {
        allLine[name]->remove(x,y);
        maxX = allLine[name]->points().first().x();
        minX = allLine[name]->points().first().x();
        maxY = allLine[name]->points().first().y();
        minY = allLine[name]->points().first().y();
    }
    else if (allScatter.contains(name))
    {
        allScatter[name]->remove(x,y);
        maxX = allScatter[name]->points().first().x();
        minX = allScatter[name]->points().first().x();
        maxY = allScatter[name]->points().first().y();
        minY = allScatter[name]->points().first().y();
    }

    setMaxMin();
}

void ChartsGraphic::deleteData(QString name)
{
    if (allLine.contains(name))
    {
        graphic->removeSeries(allLine[name]);
        delete allLine[name];
        allLine.remove(name);
    }
    else if (allScatter.contains(name))
    {
        graphic->removeSeries(allScatter[name]);
        delete allScatter[name];
        allScatter.remove(name);
    }
}

void ChartsGraphic::setDataVisible(QString name)
{
    if (allLine.contains(name))
    {
        allLine[name]->setVisible(true);
    }
    else if (allScatter.contains(name))
    {
        allScatter[name]->setVisible(true);
    }
}

void ChartsGraphic::setDataInvisible(QString name)
{
    if (allLine.contains(name))
    {
        allLine[name]->setVisible(false);
    }
    else if (allScatter.contains(name))
    {
        allScatter[name]->setVisible(false);
    }
}

void ChartsGraphic::addPoint(QString name, double x, double y)
{
    if (allLine.contains(name))
    {
        allLine[name]->append(x,y);
        if (allLine[name]->count() > points & points != 0)
        {
            allLine[name]->remove(0);
        }
        maxX = allLine[name]->points().first().x();
        minX = allLine[name]->points().first().x();
        maxY = allLine[name]->points().first().y();
        minY = allLine[name]->points().first().y();
    }
    else if (allScatter.contains(name))
    {
        allScatter[name]->append(x,y);
        if (allScatter[name]->count() > points & points != 0)
        {
            allScatter[name]->remove(0);
        }
        maxX = allScatter[name]->points().first().x();
        minX = allScatter[name]->points().first().x();
        maxY = allScatter[name]->points().first().y();
        minY = allScatter[name]->points().first().y();
    }

    setMaxMin();
}

void ChartsGraphic::setMaxMin()
{
    for (auto j:allLine.keys())
    {
        if (allLine[j]->isVisible())
        {
            if (allLine[j]->points().size() != 0)
            {
                maxX = allLine[j]->points().first().x();
                minX = allLine[j]->points().first().x();
                maxY = allLine[j]->points().first().y();
                maxY = allLine[j]->points().first().y();
                break;
            }
        }
    }
    for (auto j:allLine.keys())
    {
        if (allLine[j]->isVisible())
        {
            for (auto i:allLine[j]->points())
            {
                if (i.x()>=maxX)
                    maxX=i.x()+10;
                else if (i.x()<=minX)
                    minX=i.x()-10;
                if (i.y()>=maxY)
                    maxY=i.y()+10;
                else if (i.y()<=minY)
                    minY=i.y()-10;
            }
        }
    }
    for (auto j:allScatter.keys())
    {
        if (allScatter[j]->isVisible())
        {
            for (auto i:allScatter[j]->points())
            {
                if (i.x()>maxX)
                    maxX=i.x();
                else if (i.x()<minX)
                    minX=i.x();
                if (i.y()>maxY)
                    maxY=i.y();
                else if (i.y()<minY)
                    minY=i.y();
            }
        }
    }

    //maxX=maxX+abs(maxX)*0.05;
    axX->setMax(maxX);
    //minX=minX-abs(minX)*0.05;
    axX->setMin(minX);
    //maxY = maxY+abs(maxY)*0.05;
    axY->setMax(maxY);
    //minY = minY-abs(minY)*0.05;
    axY->setMin(minY);
}

void ChartsGraphic::addData(QString name, bool line)
{
    if (line)
    {
        lineSeries = new QLineSeries(this);
        lineSeries->setName(name);
        lineSeries->setUseOpenGL(true);
        if (name == "заданная")
        {
            lineSeries->setPointLabelsVisible(true);
            lineSeries->setPointLabelsFormat("smth");
            lineSeries->setPointLabelsFont(sampleFont);
            qDebug()<<"set points title";
        }
        graphic->addSeries(lineSeries);
        allLine[name] = lineSeries;
        graphic->setAxisX(axX,lineSeries);
        graphic->setAxisY(axY,lineSeries);
    }
    else
    {
        scatterSeries = new QScatterSeries(this);
        scatterSeries->setName(name);
        scatterSeries->setUseOpenGL(true);
        allScatter[name] = scatterSeries;
        graphic->addSeries(scatterSeries);
        graphic->setAxisX(axX,scatterSeries);
        graphic->setAxisY(axY, scatterSeries);
    }
}

void ChartsGraphic::mousePressEvent(QMouseEvent *event)
{
    QChartView::mousePressEvent(event);
}

void ChartsGraphic::mouseMoveEvent(QMouseEvent *event)
{
    QChartView::mouseMoveEvent(event);
}

void ChartsGraphic::mouseReleaseEvent(QMouseEvent *event)
{
    QChartView::mouseReleaseEvent(event);
}

void ChartsGraphic::keyPressEvent(QKeyEvent *event)
{
    switch (event->key()) {
    case Qt::Key_Plus:
        chart()->zoomIn();
        break;
    case Qt::Key_Minus:
        chart()->zoomOut();
        break;
    case Qt::Key_Left:
        chart()->scroll(-10, 0);
        break;
    case Qt::Key_Right:
        chart()->scroll(10, 0);
        break;
    case Qt::Key_Up:
        chart()->scroll(0, 10);
        break;
    case Qt::Key_Down:
        chart()->scroll(0, -10);
        break;
    case Qt::Key_Z:
        chart()->zoomReset();
        break;
    default:
        QGraphicsView::keyPressEvent(event);
        break;
    }
}

void ChartsGraphic::saveToFile()
{
    // сохранить в файл
    QDir FolderForFile;
    if (!FolderForFile.exists("./Data/Files"))
        FolderForFile.mkpath("./Data/Files");
    QString num = QTime::currentTime().toString();
    num.remove(2,1);
    num.remove(4,1);
    QFile file("./Data/Files/"+fileName+num+".csv");
    file.open(QIODevice::WriteOnly);

    // формируем заголовки
    QByteArray dataLine;
    for (auto a:allLine.keys())
    {
        dataLine+=a+"_x"+";";
        dataLine+=a+"_z"+";";
    }
    /*for (auto a:allScatter.keys())
    {
        dataLine+=a+"_x"+";";
        dataLine+=a+"_z"+";";
    }*/
    dataLine+="\n";
    file.write(dataLine);
    file.flush();

    // пишем данные
    QString control_data;
    int max_size = 0;
    for (auto a:allLine.keys())
    {
        if (allLine[a]->pointsVector().size()>max_size)
        {
            max_size = allLine[a]->pointsVector().size();
        }
    }
    for (int i=0; i<max_size;i++)
    {
        // число точек во всех линиях разное, пишем для максимального, остальное заполняем -
        dataLine.clear();
        for (auto a:allLine.keys())
        {
            if (allLine[a]->pointsVector().size()>i)
            {
                // точка с таким номером есть
                dataLine+=QString::number(allLine[a]->pointsVector()[i].x(),'f',5)+";";
                dataLine+=QString::number(allLine[a]->pointsVector()[i].y(),'f',5)+";";
            }
            else
            {
                // точки с таким номером нет
                dataLine+=";;";
            }
        }
        for (auto a:allScatter.keys())
        {
            if (allScatter[a]->points().size()>i)
            {
                // точка с таким номером есть
                dataLine+=QString::number(allScatter[a]->pointsVector()[i].x(),'f',5)+";";
                dataLine+=QString::number(allScatter[a]->pointsVector()[i].y(),'f',5)+";";
            }
            else
            {
                // точки с таким номером нет
                dataLine+=";;";
            }
        }
        dataLine+="\n";
        file.write(dataLine);
        file.flush();
    }
    file.close();
}

void ChartsGraphic::setFileName(QString name)
{
    fileName = name;
}
