#ifndef CHARTSGRAPHIC_H
#define CHARTSGRAPHIC_H

#include <QObject>
#include <QtCharts>
#include <QVector>
#include <QSizePolicy>

class ChartsGraphic : public QChartView
{
    Q_OBJECT
public:
    ChartsGraphic(QString title, QString Xname, QString Yname, unsigned int time, QFont font, QWidget *parent = nullptr);
    ChartsGraphic(QWidget *parent = nullptr);
    ~ChartsGraphic();

public slots:

    // Названия
    void setGraphicName(QString name); // название графика
    void setDataName(QString new_name, QString name); // название данных

    // Очистить график
    void clearGraphic();

    //Удалить точку
    void deletePoint(QString name, double x, double y);

    // Удалить данные
    void deleteData(QString name);

    // Изменить видимость данных
    void setDataVisible(QString name);
    void setDataInvisible(QString name);

    // Добавить точку
    void addPoint(QString name, double x, double y);

    // Добавить график
    void addData(QString name, bool line);

    // Сохранить данные в файл
    void saveToFile();

    // ввод названия файла графика
    void setFileName(QString name);

private:

    // Оси
    QValueAxis* axX;
    QValueAxis* axY;

    // Данные
    QLineSeries* lineSeries;
    QMap<QString,QLineSeries*> allLine;

    QScatterSeries* scatterSeries;
    QMap<QString,QScatterSeries*> allScatter;

    // График
    QChart* graphic;

    // Максимальное и минимальное значение по осям
    double maxX = -10000000;
    double minX = 10000000;
    double maxY = -10000000;
    double minY = 10000000;

    // Число сохраняемых точек во времени
    unsigned int points = 0;

    // Форматирование
    QFont sampleFont{};

    // название файла графика
    QString fileName = "";

    // Диапазон осей
    void setMaxMin();

public:

    // Для масштабирование и перемещения
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void keyPressEvent(QKeyEvent *event);
};

#endif // CHARTSGRAPHIC_H
