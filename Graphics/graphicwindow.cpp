#include "graphicwindow.h"
#include "ui_graphicwindow.h"

GraphicWindow::GraphicWindow(QString title, QString Xname, QString Yname, unsigned int time, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::GraphicWindow)
{
    ui->setupUi(this);

    // добавление окна графика
    QFont thisFont = ui->pushButtonDoc->font();
    graphic = new ChartsGraphic(title,Xname,Yname,time,thisFont,this);
    ui->verticalLayout->insertWidget(0,graphic);
    name = title;

    // двумерный график
    isTwo = true;
}

GraphicWindow::GraphicWindow(QString title, QString Xname, QString Yname, QString Zname, unsigned int time, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::GraphicWindow)
{
    ui->setupUi(this);

    // добавление окна графика
    QFont thisFont = ui->pushButtonDoc->font();
    //graphic3D = new DataVisGraphic(title,Xname,Yname,Zname,time,thisFont,this);
    //ui->verticalLayout->insertWidget(0,graphic3D);
    name = title;

    // двумерный график
    isTwo = false;
}

GraphicWindow::GraphicWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::GraphicWindow)
{
    ui->setupUi(this);

    // добавление окна графика
    QFont thisFont = ui->pushButtonDoc->font();
    QString title;
    QString Xname = "\u03B6, м";
    QString Yname = "\u03BE, м";
    unsigned int time;
    graphic = new ChartsGraphic(title,Xname,Yname,time,thisFont,this);
    ui->verticalLayout->insertWidget(0,graphic);
    name = title;

    // двумерный график
    isTwo = true;
}

GraphicWindow::~GraphicWindow()
{
    delete ui;
}

void GraphicWindow::on_pushButtonPicture_clicked()
{
    // сохранить график
    /*QPixmap p( graphic->size() );
    graphic->render( &p );*/
    if (isTwo)
    {
        QPixmap p = graphic->grab();
        QOpenGLWidget *glWidget  = graphic->findChild<QOpenGLWidget*>();
        //qDebug()<<"is gl "<<glWidget;
        if(glWidget){
            QPainter painter(&p);
            QPoint d = glWidget->mapToGlobal(QPoint())-graphic->mapToGlobal(QPoint());
            painter.setCompositionMode(QPainter::CompositionMode_SourceAtop);
            painter.drawImage(d, glWidget->grabFramebuffer());
            painter.end();
        }
        QString num = QTime::currentTime().toString();
        num.remove(2,1);
        num.remove(4,1);
        QDir FolderForFile;
        if (!FolderForFile.exists("./Data/Graphics"))
            FolderForFile.mkpath("./Data/Graphics");
        p.save("./Data/Graphics/"+fileName+num, "JPEG");
        //qDebug()<<"try save ";
    }
    else
    {
        /*QPixmap pixmap = QPixmap::grabWidget(graphic3D);
        QString num = QTime::currentTime().toString();
        num.remove(2,1);
        num.remove(4,1);
        pixmap.save(name+num, "PNG");*/
    }
}


void GraphicWindow::on_pushButtonClear_clicked()
{
    // очистить график
    emit pressedClean();
    if (isTwo)
    {
        graphic->clearGraphic();
    }
    else
    {
        //graphic3D->clearGraphic();
    }
}

void GraphicWindow::setGraphicName(QString name)
{
    if (isTwo)
    {
        graphic->setGraphicName(name);
        this->name = name;
    }
    else
    {
        //graphic3D->setGraphicName(name);
        this->name = name;
    }
}

void GraphicWindow::setDataName(QString new_name, QString name)
{
    if (isTwo)
    {
        graphic->setDataName(new_name,name);
    }
    else
    {
        //graphic3D->setDataName(new_name,name);
    }
}

void GraphicWindow::deletePoint(QString name, double x, double y)
{
    graphic->deletePoint(name,x,y);
}


void GraphicWindow::deleteData(QString name)
{
    if (isTwo)
    {
        graphic->deleteData(name);
    }
    else
    {
        //graphic3D->deleteData(name);
    }
}

void GraphicWindow::setDataVisible(QString name)
{
    if (isTwo)
    {
        graphic->setDataVisible(name);
    }
    else
    {
        //graphic3D->setDataVisible(name);
    }
}

void GraphicWindow::setDataInvisible(QString name)
{
    if (isTwo)
    {
        graphic->setDataInvisible(name);
    }
    else
    {
        graphic->setDataInvisible(name);
    }
}

void GraphicWindow::addPoint(QString name, double x, double y)
{
    if (!isStop)
        graphic->addPoint(name,x,y);
}

void GraphicWindow::addPoint(QString name, double x, double y, double z)
{
    //if (!isStop)
        //graphic3D->addPoint(name,x,y,z);
}

void GraphicWindow::addData(QString name, bool line)
{
    if (isTwo)
    {
        graphic->addData(name, line);
    }
    else
    {
        //graphic3D->addData(name, line);
    }
}

void GraphicWindow::closeEvent(QCloseEvent *event)
{
    emit closed();
}

void GraphicWindow::keyPressEvent(QKeyEvent *event)
{
    if (isTwo)
    {
        graphic->keyPressEvent(event);
    }
    else
    {
        //graphic3D->keyPressEvent(event);
    }
}

void GraphicWindow::on_pushButtonDoc_clicked()
{
    // сохранить график как файл с данными
    graphic->saveToFile();
}

void GraphicWindow::SetFileName(QString name)
{
    fileName = name;
    graphic->setFileName(name);
}

void GraphicWindow::clearGraphic()
{
    graphic->clearGraphic();
}
