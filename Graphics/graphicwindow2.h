#ifndef GRAPHICWINDOW2_H
#define GRAPHICWINDOW2_H

#include <QWidget>
#include <QTime>
#include <QDir>

#include "Graphics/chartsgraphic.h"
//#include "Graphics/datavisgraphic.h"

namespace Ui {
class GraphicWindow2;
}

class GraphicWindow2 : public QWidget
{
    Q_OBJECT

public:

    // Конструкторы для разных типов графиков
    GraphicWindow2(QString title, QString Xname, QString Yname, unsigned int time, QWidget *parent = nullptr);
    GraphicWindow2(QString title, QString Xname, QString Yname, QString Zname, unsigned int time, QWidget *parent = nullptr);
    GraphicWindow2(QWidget *parent = nullptr);

    // Деструктор
    ~GraphicWindow2();

    // Названия
    void setGraphicName(QString name); // название графика
    void setDataName(QString new_name, QString name); // название данных

    // Очистить график
    void clearGraphic();

    // Удалить данные
    void deletePoint(QString name, double x, double y);
    void deleteData(QString name);

    // Изменить видимость данных
    void setDataVisible(QString name);
    void setDataInvisible(QString name);

    // Добавить точку
    void addPoint(QString name, double x, double y);
    void addPoint(QString name, double x, double y, double z);

    // Добавить график
    void addData(QString name, bool line);

    // Установить имя файла графика
    void SetFileName(QString name);

private slots:

     // сохранить график в виде картинки
     void on_pushButtonPicture_clicked();

     // очистить график
     void on_pushButtonClear_clicked();

     void on_pushButtonDoc_clicked();

protected slots:

     // окно было закрыто
     void closeEvent(QCloseEvent *event);

signals:

     // окно закрыто
     void closed();

     // нажатие кнопки запустить/остановить
     void pressedStop();
     void pressedStart();

     // нажата кнопка очистки
     void pressedClean();

private:

    Ui::GraphicWindow2 *ui;

    ChartsGraphic* graphic;
    //DataVisGraphic* graphic3D;

    bool isStop = false; //обновлять данные на графиках
    bool isTwo = true; // двухмерный или трехмерный график

    QString name = ""; // для удаления графика

    QString fileName = ""; // название файла графика

public slots:

    void keyPressEvent(QKeyEvent *event);
};


#endif // GRAPHICWINDOW2_H
