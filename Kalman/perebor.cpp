#include "perebor.h"

Perebor::Perebor(QObject *parent) : QObject(parent)
{
    Pmin.resize(2,2);
    Qmin.resize(2,2);
    Rmin.resize(1,1);
    Gmin.resize(2,2);

    Fill();
    //Calc();
}

void Perebor::Fill()
{
    contur = new SimpleContur(this);
    qDebug()<<"in calc";
    connect(contur, &SimpleContur::newData,[this](double newIdP, double newP, double newVp)
    {
        idealP.append(newIdP);
        realP.append(newP);
        realVp.append(newVp);
        qDebug()<<"new P "<<newP;
    });
    connect(contur, &SimpleContur::endCalc,[this]()
    {
        qDebug()<<"end calc";
        Calc();
    });
    contur->Calc();
}

void Perebor::Calc()
{
    double Ttimer = 0.01; // период расчета
    for (int P00 = 0.1; P00< 10; P00++)
    {
        for (int P01 = 0.1; P01< 10; P01++)
        {
            for (int P10 = 0.1; P10< 10; P10++)
            {
                for (int P11 = 0.1; P11< 10; P11++)
                {
                    for (int G00 = 0.1; G00< 10; G00++)
                    {
                        for (int G01 = 0.1; G01< 10; G01++)
                        {
                            for (int G10 = 0.1; G10< 10; G10++)
                            {
                                for (int G11 = 0.1; G11< 10; G11++)
                                {
                                    for (int Q00 = 0.1; Q00< 10; Q00++)
                                    {
                                        for (int Q01 = 0.1; Q01< 10; Q01++)
                                        {
                                            for (int Q10 = 0.1; Q10< 10; Q10++)
                                            {
                                                for (int Q11 = 0.1; Q11< 10; Q11++)
                                                {
                                                    for (int R00 = 0.1; R00< 10; R00++)
                                                    {
                                                        kalman = new Kalman2(this);
                                                        kalman->setP0(P00, P01,P10,P11);
                                                        kalman->setG(G00,G01,G10,G11);
                                                        kalman->setQ(Q00, Q01,Q10,Q11);
                                                        kalman->setR(R00);
                                                        int j = 0;
                                                        double minD_now = 10000;
                                                        for (int i=0;i<50;i+=Ttimer)
                                                        {
                                                            kalman->calcKalman(realP[j],realVp[j]);
                                                            if (fabs(idealP[j]-kalman->getP())<minD_now)
                                                            {
                                                                minD_now = fabs(idealP[j]-kalman->getP());
                                                            }
                                                            qDebug()<<"delta now "<<fabs(idealP[j]-kalman->getP())<<" R00 "<< R00;
                                                        }
                                                        if (minD_now<mindP)
                                                        {
                                                            mindP = minD_now;
                                                            Pmin(0,0) = P00;
                                                            Pmin(0,1) = P01;
                                                            Pmin(1,0) = P10;
                                                            Pmin(1,1) = P11;
                                                            Gmin(0,0) = G00;
                                                            Gmin(0,1) = G01;
                                                            Gmin(1,0) = G10;
                                                            Gmin(1,1) = G11;
                                                            Qmin(0,0) = Q00;
                                                            Qmin(0,1) = Q01;
                                                            Qmin(1,0) = Q10;
                                                            Qmin(1,1) = Q11;
                                                            Rmin(0,0) = R00;
                                                            std::cout<<"find new min "<<Pmin<<" "<<Gmin<<" "<<Qmin<<" "<<Rmin;
                                                        }
                                                        delete kalman;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
