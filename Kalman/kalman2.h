#ifndef KALMAN2_H
#define KALMAN2_H

#include <QObject>
#include <QDebug>

#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>

#include "inverse.h"

#define kalman1_n 2
#define kalman1_m 1
#define kalman1_l 2

using namespace boost::numeric::ublas;

struct Kalman1Matrix
{
    //перменнные матрицы для фильтра
    matrix<double> X_now; // матрица текущих оценок
    matrix<double> X_mid; // матрица на промежуточном шаге
    matrix<double> X_prev; // матрица предыдущих оценок
    matrix<double> P_now; // матрица Р на текущем шаге
    matrix<double> P_mid; // матрица Р на промежуточном шаге
    matrix<double> P_prev; // матрица Р на предыдущем шаге
    matrix<double> R_now; // матрица R
    matrix<double> F_now; // матрица F
    matrix<double> F_now_t; // матрица F транспонированная
    matrix<double> Z_now; // матрица Z
    matrix<double> Z_mid; // матрица Z
    matrix<double> Z_prev; // матрица Z
    matrix<double> PF_t;
    matrix<double> FPF_t;
    matrix<double> PH_t;
    matrix<double> HPH_t;
    matrix<double> HPHR_sum;
    matrix<double> HPHR_1;
    matrix<double> HR;
    matrix<double> K_now;
    matrix<double> KH;
    matrix<double> EKH;
    matrix<double> KZ;
    matrix<double> GQ;
    matrix<double> QG;

    // постоянные матрицы
    matrix<double> H_now; // матрица Н
    matrix<double> H_now_t; // транспонированная матрица H
    matrix<double> Q_now; // матрица Q
    matrix<double> E;
    matrix<double> G_now;
    matrix<double> G_now_t;
    matrix<double> B_now; // матрица B
    matrix<double> U_now; // матрица u
    matrix<double> BU_now;
};


class Kalman2 : public QObject
{
    Q_OBJECT
public:
    explicit Kalman2(QObject *parent = nullptr);


public slots:

    void calcKalman(double P_now, double Vp_now); // расчеты по фильтру Калмана

    void resetMetrix(); // обнуление всех матриц

    double getP();
    double getVp();

    void setP0(double P00, double P01, double P10, double P11);
    void setG(double G00, double G01, double G10, double G11);
    void setR(double R00);
    void setQ(double Q00, double Q01, double Q10, double Q11);

private:

    Kalman1Matrix kalman1;

private slots:

    void InitKalman1();
};

#endif // KALMAN2_H
