#include "kalman2.h"

Kalman2::Kalman2(QObject *parent) : QObject(parent)
{
    // зададим размеры матрицам
    InitKalman1();

    // зададим начальные значения
    resetMetrix();
}

void Kalman2::resetMetrix()
{
    // большинство матриц 2х2 нужно обнулить
    for (int i=0;i<kalman1_n;i++)
    {
        for (int j=0;j<1;j++)
        {
            kalman1.X_now(i,j) = 0;
            kalman1.X_prev(i,j) = 0;
            kalman1.X_mid(i,j) = 0;
            kalman1.KZ(i,j) = 0;
        }
    }
    for (int i=0;i<kalman1_n;i++)
    {
        for (int j=0;j<kalman1_n;j++)
        {
            kalman1.P_now(i,j) = 0;
            kalman1.P_mid(i,j) = 0;
            kalman1.P_prev(i,j) = 0;
            kalman1.F_now(i,j) = 0;
            kalman1.F_now_t(i,j) = 0;
            kalman1.PF_t(i,j) = 0;
            kalman1.FPF_t(i,j) = 0;
            kalman1.KH(i,j) = 0;
            kalman1.EKH(i,j) = 0;
            kalman1.E(i,j) = 0;
            kalman1.QG(i,j) = 0;
        }
    }
    for (int i=0;i<kalman1_m;i++)
    {
        for (int j=0;j<kalman1_m;j++)
        {
            kalman1.R_now(i,j) = 0;
            kalman1.HPH_t(i,j) = 0;
            kalman1.HPHR_sum(i,j) = 0;
            kalman1.HPHR_1(i,j) = 0;
        }
    }
    for (int i=0;i<kalman1_m;i++)
    {
        for (int j=0;j<1;j++)
        {
            kalman1.Z_now(i,j) = 0;
            kalman1.Z_mid(i,j) = 0;
            kalman1.Z_prev(i,j) = 0;
        }
    }
    for (int i=0;i<kalman1_n;i++)
    {
        for (int j=0;j<kalman1_m;j++)
        {
            kalman1.PH_t(i,j) = 0;
            kalman1.HR(i,j) = 0;
            kalman1.K_now(i,j) = 0;
            kalman1.H_now_t(i,j) = 0;

            kalman1.H_now(j,i) = 0;
        }
    }
    for (int i=0;i<kalman1_n;i++)
    {
        for (int j=0;j<kalman1_l;j++)
        {
            kalman1.G_now(i,j) = 0;
            kalman1.GQ(i,j) = 0;

            kalman1.G_now_t(j,i) = 0;
        }
    }
    for (int i=0;i<kalman1_l;i++)
    {
        for (int j=0;j<kalman1_l;j++)
        {
            kalman1.Q_now(i,j) = 0;
        }
    }
    kalman1.G_now(0,0) = 2;
    kalman1.G_now(0,1) = 1;
    kalman1.G_now(1,0) = 1;
    kalman1.G_now(1,1) = 2;
    kalman1.G_now_t = trans(kalman1.G_now);
    kalman1.H_now(0,0) = 1;
    kalman1.H_now(1,1) = 0;
    kalman1.H_now_t = trans(kalman1.H_now);
    kalman1.Q_now(0,0) = 1;
    kalman1.Q_now(1,1) = 1;
    kalman1.E(0,0) = 1;
    kalman1.E(1,1) = 1;
    kalman1.R_now(0,0) = 10;
    for (int i=0;i<kalman1_n;i++)
        for(int j=0;j<kalman1_n;j++)
        {
            kalman1.P_now(i,j) = 1;
        }
    kalman1.P_now(0,0) = 5;
    kalman1.P_now(1,1) = 5;
    //std::cout<<X_prev<<std::endl;
}

void Kalman2::setP0(double P00, double P01, double P10, double P11)
{
    kalman1.P_now(0,0) = P00;
    kalman1.P_now(0,1) = P01;
    kalman1.P_now(1,0) = P10;
    kalman1.P_now(1,1) = P11;
}

void Kalman2::setG(double G00, double G01, double G10, double G11)
{
    kalman1.G_now(0,0) = G00;
    kalman1.G_now(0,1) = G01;
    kalman1.G_now(1,0) = G10;
    kalman1.G_now(1,1) = G11;
    kalman1.G_now_t = trans(kalman1.G_now);
}

void Kalman2::setR(double R00)
{
    kalman1.R_now(0,0) = R00;
}

void Kalman2::setQ(double Q00, double Q01, double Q10, double Q11)
{
    kalman1.Q_now(0,0) = Q00;
    kalman1.Q_now(0,1) = Q01;
    kalman1.Q_now(1,0) = Q10;
    kalman1.Q_now(1,1) = Q11;
}

void Kalman2::InitKalman1()
{
    kalman1.X_now.resize(kalman1_n,1);
    kalman1.X_mid.resize(kalman1_n,1);
    kalman1.X_prev.resize(kalman1_n,1);
    kalman1.P_now.resize(kalman1_n,kalman1_n);
    kalman1.P_mid.resize(kalman1_n,kalman1_n);
    kalman1.P_prev.resize(kalman1_n,kalman1_n);
    kalman1.R_now.resize(kalman1_m,kalman1_m);
    kalman1.F_now.resize(kalman1_n,kalman1_n);
    kalman1.F_now_t.resize(kalman1_n,kalman1_n);
    kalman1.Z_now.resize(kalman1_m,1);
    kalman1.Z_mid.resize(kalman1_m,1);
    kalman1.Z_prev.resize(kalman1_m,1);
    kalman1.PF_t.resize(kalman1_n,kalman1_n);
    kalman1.FPF_t.resize(kalman1_n,kalman1_n);
    kalman1.PH_t.resize(kalman1_n,kalman1_m);
    kalman1.HPH_t.resize(kalman1_m,kalman1_m);
    kalman1.HPHR_sum.resize(kalman1_m,kalman1_m);
    kalman1.HPHR_1.resize(kalman1_m,kalman1_m);
    kalman1.HR.resize(kalman1_n,kalman1_m);
    kalman1.K_now.resize(kalman1_n,kalman1_m);
    kalman1.KH.resize(kalman1_n,kalman1_n);
    kalman1.EKH.resize(kalman1_n,kalman1_n);
    kalman1.KZ.resize(kalman1_n,1);
    kalman1.GQ.resize(kalman1_n,kalman1_l);
    kalman1.QG.resize(kalman1_n,kalman1_n);

    kalman1.H_now.resize(kalman1_m,kalman1_n);
    kalman1.H_now_t.resize(kalman1_n,kalman1_m);
    kalman1.Q_now.resize(kalman1_l,kalman1_l);
    kalman1.E.resize(kalman1_n,kalman1_n);
    kalman1.G_now.resize(kalman1_n,kalman1_l);
    kalman1.G_now_t.resize(kalman1_l,kalman1_n);
    kalman1.B_now.resize(kalman1_n,kalman1_m);
    kalman1.U_now.resize(kalman1_m,1);
    kalman1.BU_now.resize(kalman1_n,1);
}

void Kalman2::calcKalman(double P_now, double Vp_now)
{
    // первый шаг
    kalman1.X_prev = kalman1.X_now;
    kalman1.F_now(0,1) = -1;
    kalman1.Z_now(0,0) = P_now;
    kalman1.B_now(0,0) = 1;
    kalman1.B_now(0,1) = 0;
    kalman1.U_now(0,0) = Vp_now;
    /*kalman1.G_now(0,0) = cos(psi);
    kalman1.G_now(0,1) = -sin(psi);
    kalman1.G_now(1,0) = sin(psi);
    kalman1.G_now(1,1) = cos(psi);
    kalman1.G_now_t = trans(kalman1.G_now);*/
    kalman1.P_prev = kalman1.P_now;
    kalman1.X_mid = prod(kalman1.F_now,kalman1.X_prev);

    //qDebug()<<"first step";

    // второй шаг
    kalman1.Z_prev = prod(kalman1.H_now, kalman1.X_mid);

    //qDebug()<<"second step";

    // третий шаг
    for (int i=0;i<kalman1.Z_now.size1();i++)
        for (int j=0;j<kalman1.Z_now.size2();j++)
            kalman1.Z_mid(i,j) = kalman1.Z_now(i,j)-kalman1.Z_prev(i,j);
    //std::cout<<"Z_now "<<kalman1.Z_now<<std::endl;
    //std::cout<<"Z_prev "<<kalman1.Z_prev<<std::endl;

    //qDebug()<<"third step";

    // четвертый шаг
    kalman1.F_now_t = trans(kalman1.F_now);
    kalman1.PF_t = prod(kalman1.P_prev,kalman1.F_now_t);
    kalman1.FPF_t = prod(kalman1.F_now,kalman1.PF_t);
    kalman1.GQ = prod(kalman1.G_now,kalman1.Q_now);
    kalman1.QG = prod(kalman1.GQ,kalman1.G_now_t);
    for (int i=0;i<kalman1_n;i++)
        for (int j=0;j<kalman1_n;j++)
            kalman1.P_mid(i,j) = kalman1.FPF_t(i,j) + kalman1.QG(i,j);
    //kalman1.P_mid = kalman1.FPF_t;
    //std::cout<<"P_mid "<<kalman1.P_mid<<std::endl;

    // пятый шаг
    kalman1.PH_t = prod(kalman1.P_mid,kalman1.H_now_t);
    kalman1.HPH_t = prod(kalman1.H_now,kalman1.PH_t);
    for (int i=0;i<kalman1_m;i++)
        for (int j=0;j<kalman1_m;j++)
            kalman1.HPHR_sum(i,j) = kalman1.HPH_t(i,j)+kalman1.R_now(i,j);
    bool temp_bool = true;
    kalman1.HPHR_1 = gjinverse<double>(kalman1.HPHR_sum,temp_bool);
    kalman1.HR = prod(kalman1.H_now_t,kalman1.HPHR_1);
    kalman1.K_now = prod(kalman1.P_mid, kalman1.HR);
    //std::cout<<"HPHR_1 "<<kalman1.HPHR_1<<std::endl;
    //std::cout<<"HR "<<kalman1.HR<<std::endl;
    //std::cout<<"K_now  "<<kalman1.K_now <<std::endl;

    // шестой шаг
    kalman1.KH = prod(kalman1.K_now,kalman1.H_now);
    for (int i=0;i<kalman1_n;i++)
        for (int j=0;j<kalman1_n;j++)
            kalman1.EKH(i,j) = kalman1.E(i,j)-kalman1.KH(i,j);
    kalman1.P_now = prod(kalman1.EKH,kalman1.P_mid);
    //std::cout<<"KH "<<kalman1.KH <<std::endl;
    //std::cout<<"EKH "<<kalman1.EKH <<std::endl;
    //std::cout<<"P_now "<<kalman1.P_now <<std::endl;

    // седьмой шаг
    kalman1.KZ = prod(kalman1.K_now,kalman1.Z_mid);
    kalman1.BU_now = prod(kalman1.B_now,kalman1.U_now);
    for (int i=0;i<kalman1_n;i++)
        for (int j=0;j<1;j++)
            kalman1.X_now(i,j) = kalman1.KZ(i,j)+kalman1.X_mid(i,j)+kalman1.BU_now(i,j);
    //std::cout<<"KZ "<<kalman1.KZ<<std::endl;
    //std::cout<<"X_now "<<kalman1.X_now<<std::endl;
    //std::cout<<"X_mid "<<kalman1.X_mid<<std::endl;
}

double Kalman2::getP()
{
    return kalman1.X_now(0,0);
}

double Kalman2::getVp()
{
    return kalman1.X_now(1,0);
}
