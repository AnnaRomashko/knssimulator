#ifndef PEREBOR_H
#define PEREBOR_H

#include <QObject>
#include <SimpleContur/simplecontur.h>
#include <Kalman/kalman2.h>

class Perebor : public QObject
{
    Q_OBJECT
public:
    explicit Perebor(QObject *parent = nullptr);

private:
    SimpleContur* contur;
    Kalman2* kalman;

    double mindP = 100000;
    matrix<double> Pmin;
    matrix<double> Gmin;
    matrix<double> Rmin;
    matrix<double> Qmin;

    QVector<double> idealP;
    QVector<double> realP;
    QVector<double> realVp;

private slots:

    void Calc();
    void Fill();
};

#endif // PEREBOR_H
