#ifndef KALMAN_H
#define KALMAN_H

#include <QObject>
#include <QDebug>

#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>

#include "inverse.h"

#define kalman1_n 4
#define kalman1_m 2
#define kalman1_l 2

using namespace boost::numeric::ublas;

struct Kalman1Matrix
{
    //перменнные матрицы для фильтра
    matrix<double> X_now; // матрица текущих оценок
    matrix<double> X_mid; // матрица на промежуточном шаге
    matrix<double> X_prev; // матрица предыдущих оценок
    matrix<double> P_now; // матрица Р на текущем шаге
    matrix<double> P_mid; // матрица Р на промежуточном шаге
    matrix<double> P_prev; // матрица Р на предыдущем шаге
    matrix<double> R_now; // матрица R
    matrix<double> F_now; // матрица F
    matrix<double> F_now_t; // матрица F транспонированная
    matrix<double> Z_now; // матрица Z
    matrix<double> Z_mid; // матрица Z
    matrix<double> Z_prev; // матрица Z
    matrix<double> PF_t;
    matrix<double> FPF_t;
    matrix<double> PH_t;
    matrix<double> HPH_t;
    matrix<double> HPHR_sum;
    matrix<double> HPHR_1;
    matrix<double> HR;
    matrix<double> K_now;
    matrix<double> KH;
    matrix<double> EKH;
    matrix<double> KZ;
    matrix<double> GQ;
    matrix<double> QG;

    // постоянные матрицы
    matrix<double> H_now; // матрица Н
    matrix<double> H_now_t; // транспонированная матрица H
    matrix<double> Q_now; // матрица Q
    matrix<double> E;
    matrix<double> G_now;
    matrix<double> G_now_t;
};

struct Kalman2Matrix
{
    // теперь фильтр для оценки значений с теми же параметрами и непрерывный
    matrix<double> X_now;
    matrix<double> dX_now;
    matrix<double> X_now_t;
    matrix<double> Z_now;
    matrix<double> H_now;
    matrix<double> H_now_t;
    matrix<double> F_now;
    matrix<double> F_now_t;
    matrix<double> R_now;
    matrix<double> R_now_1;
    matrix<double> P_now;
    matrix<double> dP_now;
    matrix<double> B_now;
    matrix<double> U_now;
    matrix<double> K_now;

    matrix<double> HX;
    matrix<double> ZHX;
    matrix<double> FP;
    matrix<double> PF_t;
    matrix<double> PH_t;
    matrix<double> PHR_1;
    matrix<double> PHRH;
    matrix<double> PHRHP;
    matrix<double> PH_new_t;
    matrix<double> FX;
    matrix<double> BU;
    matrix<double> KZ;
};

class Kalman : public QObject
{
    Q_OBJECT
public:
    explicit Kalman(QObject *parent = nullptr);

signals:

public slots:

    void calcKalman(double dx, double dz, double psi); // расчеты по фильтру Калмана
    void calcKalman2(double x, double z, double psi, double vx, double vz, double step); // расчеты по фильтру Калмана

    void resetMetrix(); // обнуление всех матриц

    double getDX();
    double getDZ();
    double getDVx();
    double getDVz();

    double getKalman2_X();
    double getKalman2_Z();
    double getKalman2_dVx();
    double getKalman2_dVz();

private:

    Kalman1Matrix kalman1;
    Kalman2Matrix kalman2;

private slots:

    void InitKalman1();
    void InitKalman2();

    void RungeKalman2_P(double step);
    void RungeKalman2_X(double step);
    void CalcKalman2_P();
    void CalcKalman2_X();
};

#endif // KALMAN_H
