#include "matrixoperations.h"

MatrixOperations::MatrixOperations(QObject *parent) : QObject(parent)
{

}

void MatrixOperations::matrix_sum(int strok, int stolb, double **matr1, double **matr2, double **matr_res)
{
    for (int i=0;i<strok;i++)
    {
        for (int j=0;j<stolb;j++)
        {
            matr_res[i][j] = matr1[i][j]+matr2[i][j];
        }
    }
}

void MatrixOperations::matrix_subt(int strok, int stolb, double **matr_from, double **matr_who, double **matr_res)
{
    for (int i=0;i<strok;i++)
    {
        for (int j=0;j<stolb;j++)
        {
            matr_res[i][j] = matr_from[i][j]-matr_who[i][j];
        }
    }
}

void MatrixOperations::matrix_mult(int strok_res, int stolb_res, int stolb_1, double **matr1, double **matr2, double **matr_res)
{
    for (int i=0; i<strok_res;i++)
    {
        for (int j=0;j<stolb_res;j++)
        {
            double res = 0;
            for (int k=0;k<stolb_1;k++)
            {
                res+=matr1[i][k]*matr2[k][j];
            }
            matr_res[i][j] = res;
        }
    }
}

void MatrixOperations::matrix_div(int strok_res, int stolb_res, double **matr_from, double **matr_who, double **matr_res)
{
    //double** matr_whoT = new ;
    //this->matrix_back(matr_who,matr_whoT);
}

void MatrixOperations::matrix_trans(int strok_res, int stolb_res, double **matr_sour, double **matr_res)
{
    for (int i=0; i<strok_res;i++)
    {
        for (int j=0;j<stolb_res;j++)
        {
           matr_res[i][j] = matr_sour[j][i];
        }
    }
}

void MatrixOperations::matrix_back(int strok_res, int stolb_res, double **matr_sour, double **matr_res)
{
    double** matr_sourT = new double* [stolb_res];
    for (int i=0;i<stolb_res;i++)
        matr_sourT[i] = new double [strok_res];
    this->matrix_trans(stolb_res, strok_res, matr_sour,matr_sourT); // транспанированную матрицу нашли

    double** matrDiag = new double* [strok_res];
    for (int i=0;i<strok_res;i++)
    {
        matrDiag[i] = new double[stolb_res];
        for (int j=0;j<stolb_res;j++)
            matrDiag[i][j] = matr_sour[i][j];
    }
    // приводим матрицу к диагональному виду
    // поиск максимума по столбцу и прямой ход
    int count=0;
    for (int i=0;i<strok_res;i++)
    {
        double max=matrDiag[i][i];
        int maxnum=i;
        for (int j=i;j<stolb_res;j++) // поиск максимума в столбце
        {
            if ((matrDiag[j][i]>max && matrDiag[j][i] != 0) || (matrDiag[j][i] != 0 && max == 0))
            {
                max=matrDiag[j][i];
                maxnum=j;
            }
        }
        if (max == 0) // если на диагонале нулевой элемент
        {
            qDebug() << "нет решения\n";
            return;
        }
        if (maxnum != i) // перестановка максимума на диагональ
        {
            count++;
            double temp;
            for (int j=0;j<stolb_res;j++)
            {
                temp = matrDiag[i][j];
                matrDiag[i][j] = matrDiag[maxnum][j];
                matrDiag[maxnum][j] = temp;
            }
        }
        for (int j=i+1;j<strok_res;j++) // прямой ход
        {
            double m=matrDiag[j][i]/matrDiag[i][i]; // a21/a11
            for (int k=0;k<stolb_res;k++)
            {
                matrDiag[j][k]=matrDiag[j][k]-matrDiag[i][k]*m;
            }
        }
    }

    // расчет обратной матрицы
    double detA = 1; // определитель матрицы А
    for (int i=0;i<strok_res;i++)
    {
        detA=detA*matrDiag[i][i];
    }
    for (int j=0;j<stolb_res;j++)
    {
        for (int i=0;i<strok_res;i++)
        {
            double** mm = new double* [strok_res-1]; // для расчета миноров
            for (int k=0;k<strok_res-1;k++)
            {
                mm[k] = new double [strok_res-1];
                for (int l=0;l<strok_res-1;l++)
                {
                    if (k>=j && l>=i)
                        mm[k][l] = matr_sourT[l+1][k+1];
                    else if (k>=j && l<i)
                        mm[k][l] = matr_sourT[l][k+1];
                    else if (k<j && l>=i)
                        mm[k][l] = matr_sourT[l+1][k];
                    else if (k<j && l<i)
                        mm[k][l] = matr_sourT[l][k];
                    //cout<< mm[k][l]<< " ";
                }
                //cout<<endl;
            }
            matr_res[j][i] = (this->matrix_minor(strok_res-1,strok_res-1,mm)*pow(-1,i+j))/detA;
            //cout << i<< " "<< j<< " "<<pow(-1,i+j)<<"minor "<< minor(n-1,mm)<< endl;
        }
    }
}

double MatrixOperations::matrix_minor(int strok, int stolb, double **matr)
{
    int count=0;
    for (int i=0;i<strok;i++)
    {
        double max=matr[i][i];
        int maxnum=i;
        for (int j=i;j<strok;j++) // поиск максимума в столбце
        {
            if ((matr[j][i]>max && matr[j][i] != 0) || (matr[j][i] != 0 && max == 0))
            {
                max=matr[j][i];
                maxnum=j;
            }
        }
        if (max == 0) // если на диагонале нулевой элемент
        {
            //cout << "нет решения";
            return 0;
        }
        if (maxnum != i) // перестановка максимума на диагональ
        {
            count++;
            for (int j=0;j<strok;j++)
            {
                double temp = matr[i][j];
                matr[i][j] = matr[maxnum][j];
                matr[maxnum][j] = temp;
            }
        }
        for (int j=i+1;j<strok;j++) // прямой ход
        {
            double v=matr[j][i]/matr[i][i]; // m21/m11
            for (int k=0;k<strok;k++)
            {
                matr[j][k]=matr[j][k]-matr[i][k]*v;
            }
        }
    }
    double det=1;
    for (int i=0;i<strok;i++)
    {
        det=det*matr[i][i];
    }
    //cout << "count "<< count<<endl;
    return det*pow(-1,count);
}
